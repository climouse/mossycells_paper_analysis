%% this script generates a scatter plot of the firing properties as shown in the suppl figure.

%% set up in/out directories and files and figure style (for consistent figure cosmetics)
homedir='..'; %adjust, home directory for in/out
figdir=fullfile(homedir,'figures_juxta'); %ajdjust, folder where figures are saved
tablesdir=fullfile(homedir,'tables_juxta');
datadir=fullfile(homedir,'data_juxta'); %adjust, folder where the raw data are located.

ff=figureFormatter(figdir,1); %figure formatter 
ff.saveformats={'eps3'}; %save format ex: eps3, png, ...
ff.doplot=1; %set to 1 if don't want to save plots (faster), set to 2 to save the plots

%control of figure font size, linewidth, etc...
ff.dim.z=1.2*2;
ff.aflag=1;
ff.ratio=1;
ff.grey2black=1;
ff.p_axes.FontSize=12;
ff.p_axes.LineWidth=1;
ff.p_axes.TickDir='out';
ff.label_sz=12;

%color scheme
ccols=figureFormatter.getDefaultColors(7);
myorange= [0.8706  0.4902  0];
mygray=[0.7,0.7,0.7];
mygreen=[0,117,59]/255;
myblue=ccols{1};
myblue2=[0,173,238]/255;



fname=fullfile(tablesdir,'cells_analysis_input.xlsx');
opts = detectImportOptions(fname);
opts = setvartype(opts,{'phase'},{'char'});

  T=readtable(fname,opts);
  var2keep={'date','id','name','desc','epileptic','confirmed','side','lim','gp2plot','phase'};
  Trun=cat(2,array2table((1:sum(T.run==1))','VariableNames',{'cellID'}),T(T.run==1,var2keep));
  [T0,T1]=spikes.load_stats(datadir,Trun,'juxta','stats.mat');
  
  type2plot={'MC',[189,30,45]/255;'GC',ccols{3};'IN',ccols{1};'INAAC',ccols{1};'?',[0.2,0.2,0.2]};
sdata=120;
  showtxt=1;
  

  
  %%
  close all
  plot_layout={'delta_delay','delta_delay_right','ratesPM';'','ratesPMinsert','spikesN'};
doplot=2;
fname_comp='firing';
ff.dataname='juxta_summary';
ff.clear(2);
figsall=repmat({[]},size(plot_layout));
  % plot the change in rate left
  xs=100*T1.drate_mean_light-100;
  xes=100*T1.drate_std_light./sqrt(T1.drate_n_light);
  %xesH=T1.drate_std_light./sqrt(T1.drate_n_light);
  ys=T1.delay1_med_light;
  yes=T1.delay1_mad_light;
  %yesL=T1.delay1_CIlow_light;
  %yesH=T1.delay1_CIhigh_light;
  yesL=ys-yes;
  yesH=ys+yes;



  figure;
 
  plotebx=1;
   ploteby=1;
  for i=1:length(type2plot)
      ccol=type2plot{i,2};
      ccol2=coolcolors.rgb2hsl(ccol); %light out for unconfirmed
      ccol3=ccol2;
      ccol2(3)=(ccol2(3)+1)/2;

      ccol2=coolcolors.hsl2rgb(ccol2);

      ccol3(3)=ccol3(3)/2;
      ccol3=coolcolors.hsl2rgb(ccol3);


      i2plot=cellfun(@(x) strcmp(x,type2plot{i,1}),Trun.desc,'uni',1);
      i2plot_pipette=i2plot & Trun.side==1 & not(Trun.confirmed);
      i2plot_of=i2plot & Trun.side==2 & not(Trun.confirmed);
      i2plot_pipette_conf=i2plot & Trun.side==1 & (Trun.confirmed);
      i2plot_of_conf=i2plot & Trun.side==2 & (Trun.confirmed);
    scatter(xs(i2plot_pipette),ys(i2plot_pipette),'Marker','o','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    hold all
    scatter(xs(i2plot_of),ys(i2plot_of),'Marker','s','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    scatter(xs(i2plot_pipette_conf),ys(i2plot_pipette_conf),'Marker','o','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',3);
    scatter(xs(i2plot_of_conf),ys(i2plot_of_conf),'Marker','s','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',3);

    if plotebx | ploteby | showtxt
    i2plot_i=find(i2plot);
    for j=1:length(i2plot_i)
        jj=i2plot_i(j);
        if plotebx
        plot([xs(jj)-xes(jj),xs(jj)+xes(jj)],[ys(jj),ys(jj)],'LineWidth',1,'Color',ccol3);
        end
        if ploteby
        plot([xs(jj),xs(jj)],[yesL(jj),yesH(jj)],'LineWidth',1,'Color',ccol3);
        end
        if showtxt        
            labelname=Trun.name{jj};
            if Trun.epileptic(jj)
            labelname=[labelname ' *'];
            end
            mytxt=text(xs(jj)+xes(jj),yesH(jj),labelname,'Color',ccol3);
            
            set(mytxt,'HorizontalAlignment','left','VerticalAlignment','bottom');
        end
    end
    end
    
  end
  xlabel('Rate change [%]');
    ylabel('Delay [ms]');
    xlim(100*[0,4]-100);
    ylim([0,50]);
    plot(100*[1,1]-100,[0,50],'LineWidth',2,'LineStyle','-.','Color','k');
    ff.doplot=1;
    ff.ratio=2;
     ff.savefig(gcf,'deltaRateVSdelay',[1,2,2]);

     figsall{find(strcmp('delta_delay',plot_layout))}=gcf;
     
  % plot the change in rate right
  xs=T1.drate_mean_light*100-100;
  infpnts=xs>40000-100;
  xs(infpnts)=50*100-100;
  xes=T1.drate_std_light./sqrt(T1.drate_n_light);
  xes(infpnts)=0;
  %xesH=T1.drate_std_light./sqrt(T1.drate_n_light);
  ys=T1.delay1_med_light;
  yes=T1.delay1_mad_light;
  %yesL=T1.delay1_CIlow_light;
  %yesH=T1.delay1_CIhigh_light;
  yesL=ys-yes;
  yesH=ys+yes;


 
  figure;
 
  plotebx=1;
   ploteby=1;
  for i=1:length(type2plot)
      ccol=type2plot{i,2};
      ccol2=coolcolors.rgb2hsl(ccol); %light out for unconfirmed
      ccol3=ccol2;
      ccol2(3)=(ccol2(3)+1)/2;

      ccol2=coolcolors.hsl2rgb(ccol2);

      ccol3(3)=ccol3(3)/2;
      ccol3=coolcolors.hsl2rgb(ccol3);


      i2plot=cellfun(@(x) strcmp(x,type2plot{i,1}),Trun.desc,'uni',1);
      i2plot_pipette=i2plot & Trun.side==1 & not(Trun.confirmed);
      i2plot_of=i2plot & Trun.side==2 & not(Trun.confirmed);
      i2plot_pipette_conf=i2plot & Trun.side==1 & (Trun.confirmed);
      i2plot_of_conf=i2plot & Trun.side==2 & (Trun.confirmed);
    scatter(xs(i2plot_pipette),ys(i2plot_pipette),'Marker','o','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    hold all
    scatter(xs(i2plot_of),ys(i2plot_of),'Marker','s','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    scatter(xs(i2plot_pipette_conf),ys(i2plot_pipette_conf),'Marker','o','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',3);
    scatter(xs(i2plot_of_conf),ys(i2plot_of_conf),'Marker','s','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',3);

    if plotebx | ploteby | showtxt
    i2plot_i=find(i2plot);
    for j=1:length(i2plot_i)
        jj=i2plot_i(j);
        if plotebx
        plot([xs(jj)-xes(jj),xs(jj)+xes(jj)],[ys(jj),ys(jj)],'LineWidth',1,'Color',ccol3);
        end
        if ploteby
        plot([xs(jj),xs(jj)],[yesL(jj),yesH(jj)],'LineWidth',1,'Color',ccol3);
        end
        
        
        if showtxt       & xs(jj)>10*100-100
            labelname=Trun.name{jj};
            if Trun.epileptic(jj)
            labelname=[labelname ' *'];
            end
            mytxt=text(xs(jj)+xes(jj),yesH(jj),labelname,'Color',ccol3);
            
            set(mytxt,'HorizontalAlignment','left','VerticalAlignment','bottom');
        end
        
        
%         if showtxt
%             if xs(jj)>10 %rm marks from oters
%         text(xs(jj)+xes(jj),yesH(jj),num2str(Trun.cellID(jj)),'Color',ccol3);
%             end
%         end
    end
    end
    set(gca,'YColor','none');
    xlabel('Rate change [%]');
    ylabel('Delay [ms]');
    xlim([10,50]*100-100);
    ylim([0,50]);
    
  end
  ff.doplot=1;
  ff.ratio=2;
  ff.savefig(gcf,'deltaRateVSdelay_right',[1,2,2]);

     figsall{find(strcmp('delta_delay_right',plot_layout))}=gcf;

  % plot the rates

  ys=T1.rate_mean_light;
  yes=T1.rate_std_light./sqrt(T1.rate_n_light);

  xs=T0.rate_mean_mock;
  xes=T0.rate_std_mock./sqrt(T0.rate_n_mock);
  yesL=ys-yes;
  yesH=ys+yes;

  figure;
  plotebx=1;
   ploteby=1;
  for i=1:length(type2plot)
      ccol=type2plot{i,2};
      ccol2=coolcolors.rgb2hsl(ccol); %light out for unconfirmed
      ccol3=ccol2;
      ccol2(3)=(ccol2(3)+1)/2;

      ccol2=coolcolors.hsl2rgb(ccol2);

      ccol3(3)=ccol3(3)/2;
      ccol3=coolcolors.hsl2rgb(ccol3);


      i2plot=cellfun(@(x) strcmp(x,type2plot{i,1}),Trun.desc,'uni',1);
      i2plot_pipette=i2plot & Trun.side==1 & not(Trun.confirmed);
      i2plot_of=i2plot & Trun.side==2 & not(Trun.confirmed);
      i2plot_pipette_conf=i2plot & Trun.side==1 & (Trun.confirmed);
      i2plot_of_conf=i2plot & Trun.side==2 & (Trun.confirmed);
    scatter(xs(i2plot_pipette),ys(i2plot_pipette),'Marker','o','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    hold all
    scatter(xs(i2plot_of),ys(i2plot_of),'Marker','s','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    scatter(xs(i2plot_pipette_conf),ys(i2plot_pipette_conf),'Marker','o','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',2);
    scatter(xs(i2plot_of_conf),ys(i2plot_of_conf),'Marker','s','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',2);

    if plotebx | ploteby | showtxt
    i2plot_i=find(i2plot);
    for j=1:length(i2plot_i)
        jj=i2plot_i(j);
        if plotebx
        plot([xs(jj)-xes(jj),xs(jj)+xes(jj)],[ys(jj),ys(jj)],'LineWidth',1,'Color',ccol3);
        
        end
        if ploteby
        plot([xs(jj),xs(jj)],[yesL(jj),yesH(jj)],'LineWidth',1,'Color',ccol3);
        end
        if showtxt        
            labelname=Trun.name{jj};
            if Trun.epileptic(jj)
            labelname=[labelname ' *'];
            end
            mytxt=text(xs(jj)+xes(jj),yesH(jj),labelname,'Color',ccol3);
            
            set(mytxt,'HorizontalAlignment','left','VerticalAlignment','bottom');
        end
    end
  end



  end
 hold all
 plot(get(gca,'XLim'),get(gca,'XLim'),'Color','k','LineWidth',2,'LineStyle','--');

      xlabel('Firing rate without light [Hz]');
      ylabel('Firing rate with light [Hz]');

   
    ff.doplot=1;
    ff.ratio=2;
     ff.savefig(gcf,'ratesPM',[2,2,2]);

     figsall{find(strcmp('ratesPM',plot_layout))}=gcf;

     
     % rates INSERT ---------
     % plot the rates

  ys=T1.rate_mean_light;
  yes=T1.rate_std_light./sqrt(T1.rate_n_light);

  xs=T0.rate_mean_mock;
  xes=T0.rate_std_mock./sqrt(T0.rate_n_mock);
  
 

  figure;
  plotebx=1;
   ploteby=1;
  for i=1:length(type2plot)
      ccol=type2plot{i,2};
      ccol2=coolcolors.rgb2hsl(ccol); %light out for unconfirmed
      ccol3=ccol2;
      ccol2(3)=(ccol2(3)+1)/2;

      ccol2=coolcolors.hsl2rgb(ccol2);

      ccol3(3)=ccol3(3)/2;
      ccol3=coolcolors.hsl2rgb(ccol3);


      i2plot=cellfun(@(x) strcmp(x,type2plot{i,1}),Trun.desc,'uni',1)  & xs<2 & ys<4;
      i2plot_pipette=i2plot & Trun.side==1 & not(Trun.confirmed);
      i2plot_of=i2plot & Trun.side==2 & not(Trun.confirmed);
      i2plot_pipette_conf=i2plot & Trun.side==1 & (Trun.confirmed);
      i2plot_of_conf=i2plot & Trun.side==2 & (Trun.confirmed);
    scatter(xs(i2plot_pipette),ys(i2plot_pipette),'Marker','o','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    hold all
    scatter(xs(i2plot_of),ys(i2plot_of),'Marker','s','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    scatter(xs(i2plot_pipette_conf),ys(i2plot_pipette_conf),'Marker','o','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',2);
    scatter(xs(i2plot_of_conf),ys(i2plot_of_conf),'Marker','s','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',2);

    if plotebx | ploteby | showtxt
    i2plot_i=find(i2plot);
    for j=1:length(i2plot_i)
        jj=i2plot_i(j);
        if plotebx
        plot([xs(jj)-xes(jj),xs(jj)+xes(jj)],[ys(jj),ys(jj)],'LineWidth',1,'Color',ccol3);
        
        end
        if ploteby
        plot([xs(jj),xs(jj)],[yesL(jj),yesH(jj)],'LineWidth',1,'Color',ccol3);
        end
        if showtxt        
            labelname=Trun.name{jj};
            if Trun.epileptic(jj)
            labelname=[labelname ' *'];
            end
            mytxt=text(xs(jj)+xes(jj),yesH(jj),labelname,'Color',ccol3);
            
            set(mytxt,'HorizontalAlignment','left','VerticalAlignment','bottom');
        end
    end
  end



  end
 hold all
 xlim([0,2]);
 ylim([0,4]);
 plot(get(gca,'XLim'),get(gca,'XLim'),'Color','k','LineWidth',2,'LineStyle','--');

      xlabel('Firing rate without light [Hz]');
      ylabel('Firing rate with light [Hz]');

   
    ff.doplot=1;
    ff.ratio=2;
     ff.savefig(gcf,'ratesPMinsert',[0.7,0.7,2]);

     figsall{find(strcmp('ratesPMinsert',plot_layout))}=gcf;
     
     
     
     
     
 % plot the fraction of light with 1, the fraction with 2 or more
 % -------------
    xs=(T1.f1_light)*100;

 ys=(T1.f2_light+T1.fmore_light)*100;
  xes=0.01*ones(size(xs));
  yes=zeros(size(ys));
  yesH=yes+ys;
  plotebx=0;
  ploteby=0;

  figure;
  
  for i=1:length(type2plot)
      ccol=type2plot{i,2};
      ccol2=coolcolors.rgb2hsl(ccol); %light out for unconfirmed
      ccol3=ccol2;
      ccol2(3)=(ccol2(3)+1)/2;

      ccol2=coolcolors.hsl2rgb(ccol2);

      ccol3(3)=ccol3(3)/2;
      ccol3=coolcolors.hsl2rgb(ccol3);


      i2plot=cellfun(@(x) strcmp(x,type2plot{i,1}),Trun.desc,'uni',1);
      i2plot_pipette=i2plot & Trun.side==1 & not(Trun.confirmed);
      i2plot_of=i2plot & Trun.side==2 & not(Trun.confirmed);
      i2plot_pipette_conf=i2plot & Trun.side==1 & (Trun.confirmed);
      i2plot_of_conf=i2plot & Trun.side==2 & (Trun.confirmed);
    scatter(xs(i2plot_pipette),ys(i2plot_pipette),'Marker','o','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    hold all
    scatter(xs(i2plot_of),ys(i2plot_of),'Marker','s','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    scatter(xs(i2plot_pipette_conf),ys(i2plot_pipette_conf),'Marker','o','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',3);
    scatter(xs(i2plot_of_conf),ys(i2plot_of_conf),'Marker','s','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',3);

    if plotebx | ploteby | showtxt
    i2plot_i=find(i2plot);
    for j=1:length(i2plot_i)
        jj=i2plot_i(j);
        if plotebx
        plot([xs(jj)-xes(jj),xs(jj)+xes(jj)],[ys(jj),ys(jj)],'LineWidth',1,'Color',ccol3);
        end
        if ploteby
        plot([xs(jj),xs(jj)],[yesL(jj),yesH(jj)],'LineWidth',1,'Color',ccol3);
        end
        if showtxt        
            labelname=Trun.name{jj};
            if Trun.epileptic(jj)
            labelname=[labelname ' *'];
            end
            mytxt=text(xs(jj)+xes(jj),yesH(jj),labelname,'Color',ccol3);
            
            set(mytxt,'HorizontalAlignment','left','VerticalAlignment','bottom');
        end
    end
  end



  end
   hold all
 plot(get(gca,'XLim'),get(gca,'XLim'),'Color','k','LineWidth',2,'LineStyle','--');

     xlabel('Single spikes [% of pulses]');
      ylabel('Multiple spikes [% of pulses]');
   ff.doplot=1;
    ff.ratio=2;
     ff.savefig(gcf,'spikesN',[2,2,2]);

     figsall{find(strcmp('spikesN',plot_layout))}=gcf;
     
     %
     compfig=coolfig.compose(figsall,[-1,-1],1,'normalized',1,0.5*[5,5,5,5]);



ff.doplot=doplot;
%
ff.savefig(gcf,[fname_comp]);
ff.clear(0);


%%
fname_out_wf=fullfile(tablesdir,'waveforms.xlsx');

%% compute waveform properties

params=struct('baseline_tmM',[-1.5,-1],'raise_thresh',[0.7,0.3],'use_baseline',1);
Twf=table();
nboot=1000;
for i=1:height(Trun)
   
    fprintf(1,'Computing spike waveform properties for cell %g [%g]\n',i,height(Trun));
    
    out=spikes.load_data(datadir,Trun(i,:),'juxta','profiles.mat');
    u=spikes.load_data(datadir,Trun(i,:),'juxta','spikes.mat');
    props=out.f_profs(Trun.gp2plot(i)).get_properties(params);
    props_med=out.f_profs(Trun.gp2plot(i)).median.get_properties(params);
    props_boot=structfun(@(x) nanmedian(x,1),props,'uni',0);
    
    Tmed=struct2table(props_med);
    Tboot=struct2table(props_boot);
    
    Tboot.Properties.VariableNames=cellfun(@(x) [x '_boot'],Tboot.Properties.VariableNames,'uni',0);
    
    props_bootCIL=structfun(@(x) [1,0]*bootci(nboot,@(y) nanmedian(y,1),x),props,'uni',0);
     props_bootCIH=structfun(@(x) [0,1]*bootci(nboot,@(y) nanmedian(y,1),x),props,'uni',0);
    TbootCIL=struct2table(props_bootCIL);
    TbootCIL.Properties.VariableNames=cellfun(@(x) [x '_CIL'],TbootCIL.Properties.VariableNames,'uni',0);
    TbootCIH=struct2table(props_bootCIH);
    TbootCIH.Properties.VariableNames=cellfun(@(x) [x '_CIH'],TbootCIH.Properties.VariableNames,'uni',0);
    %spikegp=
    save(fullfile(u.folder,[u.dataname '_spikeswf.mat']),'props','props_med');
    Twf=cat(1,Twf,cat(2,array2table(i,'VariableNames',{'id'}),Tmed,Tboot,TbootCIL,TbootCIH));
    
    
end

%%
writetable(Twf,fname_out_wf);

%%
Twf=readtable(fname_out_wf);
   
    
    %%
    close all
  plot_layout={'widthVSraise','peakVSwidth'};
doplot=2;
fname_comp='spikeWF';
ff.dataname='juxta_summary';
 
  showtxt=1;
  

ff.clear(2);
figsall=repmat({[]},size(plot_layout));
    
    Trun2=Trun(1:18,:); %Trun([1:19,21:22],:);
    
    
     % plot width vs raise
  ys=Twf.t_dip_boot;
  yesL=Twf.t_dip_CIL;
  yesH=Twf.t_dip_CIH;
  %xesH=T1.drate_std_light./sqrt(T1.drate_n_light);
  xs=2*Twf.t_raise_boot;
  xesL=2*Twf.t_raise_CIL;
  xesH=2*Twf.t_raise_CIH;
  
  figure;
 
  plotebx=1;
   ploteby=1;
  for i=1:length(type2plot)
      ccol=type2plot{i,2};
      ccol2=coolcolors.rgb2hsl(ccol); %light out for unconfirmed
      ccol3=ccol2;
      ccol2(3)=(ccol2(3)+1)/2;

      ccol2=coolcolors.hsl2rgb(ccol2);

      ccol3(3)=ccol3(3)/2;
      ccol3=coolcolors.hsl2rgb(ccol3);


      i2plot=cellfun(@(x) strcmp(x,type2plot{i,1}),Trun2.desc,'uni',1);
      i2plot_pipette=i2plot & Trun2.side==1 & not(Trun2.confirmed);
      i2plot_of=i2plot & Trun2.side==2 & not(Trun2.confirmed);
      i2plot_pipette_conf=i2plot & Trun2.side==1 & (Trun2.confirmed);
      i2plot_of_conf=i2plot & Trun2.side==2 & (Trun2.confirmed);
    scatter(xs(i2plot_pipette),ys(i2plot_pipette),'Marker','o','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    hold all
    scatter(xs(i2plot_of),ys(i2plot_of),'Marker','s','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    scatter(xs(i2plot_pipette_conf),ys(i2plot_pipette_conf),'Marker','o','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',3);
    scatter(xs(i2plot_of_conf),ys(i2plot_of_conf),'Marker','s','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',3);

    if plotebx | ploteby | showtxt
    i2plot_i=find(i2plot);
    for j=1:length(i2plot_i)
        jj=i2plot_i(j);
        if plotebx
        plot([xesL(jj),xesH(jj)],[ys(jj),ys(jj)],'LineWidth',2,'Color',ccol3);
        end
        if ploteby
        plot([xs(jj),xs(jj)],[yesL(jj),yesH(jj)],'LineWidth',1,'Color',ccol3);
        end
        if showtxt
        text(xesH(jj)+0.02,yesH(jj)+0.02,num2str(Trun2.cellID(jj)),'Color',ccol3);

        end
    end
    end
    
  end
  ylabel('Peak-to-trough [ms]');
 xlabel('Rise time [ms]');
%     xlim([0,4]);
%     ylim([0,50]);
%     plot([1,1],[0,50],'LineWidth',2,'LineStyle','-.','Color','k');
    ff.doplot=1;
    ff.ratio=2;
     ff.savefig(gcf,'widthVSraise',[2,2,2]);
% 
%      figsall{find(strcmp('delta_delay',plot_layout))}=gcf;

     figsall{find(strcmp('widthVSraise',plot_layout))}=gcf;
     

     % plot peakVSwidth vs width
  xs=Twf.t_dip_boot;
  xesL=Twf.t_dip_CIL;
  xesH=Twf.t_dip_CIH;
  %xesH=T1.drate_std_light./sqrt(T1.drate_n_light);
  ys=-Twf.A_dip_boot;
  yesL=-Twf.A_dip_CIH;
  yesH=-Twf.A_dip_CIL;
  
  figure;
 
  plotebx=1;
   ploteby=1;
  for i=1:length(type2plot)
      ccol=type2plot{i,2};
      ccol2=coolcolors.rgb2hsl(ccol); %light out for unconfirmed
      ccol3=ccol2;
      ccol2(3)=(ccol2(3)+1)/2;

      ccol2=coolcolors.hsl2rgb(ccol2);

      ccol3(3)=ccol3(3)/2;
      ccol3=coolcolors.hsl2rgb(ccol3);


      i2plot=cellfun(@(x) strcmp(x,type2plot{i,1}),Trun2.desc,'uni',1);
      i2plot_pipette=i2plot & Trun2.side==1 & not(Trun2.confirmed);
      i2plot_of=i2plot & Trun2.side==2 & not(Trun2.confirmed);
      i2plot_pipette_conf=i2plot & Trun2.side==1 & (Trun2.confirmed);
      i2plot_of_conf=i2plot & Trun2.side==2 & (Trun2.confirmed);
    scatter(xs(i2plot_pipette),ys(i2plot_pipette),'Marker','o','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    hold all
    scatter(xs(i2plot_of),ys(i2plot_of),'Marker','s','SizeData',sdata,'MarkerEdgeColor',ccol2,'MarkerFaceColor',ccol2);
    scatter(xs(i2plot_pipette_conf),ys(i2plot_pipette_conf),'Marker','o','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',3);
    scatter(xs(i2plot_of_conf),ys(i2plot_of_conf),'Marker','s','SizeData',sdata,'MarkerEdgeColor','none','MarkerFaceColor',ccol,'LineWidth',3);

    if plotebx | ploteby | showtxt
    i2plot_i=find(i2plot);
    for j=1:length(i2plot_i)
        jj=i2plot_i(j);
        if plotebx
        plot([xesL(jj),xesH(jj)],[ys(jj),ys(jj)],'LineWidth',2,'Color',ccol3);
        end
        if ploteby
        plot([xs(jj),xs(jj)],[yesL(jj),yesH(jj)],'LineWidth',1,'Color',ccol3);
        end
        if showtxt
        text(xesH(jj)+0.02,yesH(jj)+0.02,num2str(Trun2.cellID(jj)),'Color',ccol3);

        end
    end
    end
    
  end
  xlabel('Peak-to-trough [ms]');
 ylabel('Trough over peak [%]');
%     xlim([0,4]);
%     ylim([0,50]);
%     plot([1,1],[0,50],'LineWidth',2,'LineStyle','-.','Color','k');
    ff.doplot=1;
    ff.ratio=2;
     ff.savefig(gcf,'peakVSwidth',[2,2,2]);
% 
%      figsall{find(strcmp('delta_delay',plot_layout))}=gcf;

     figsall{find(strcmp('peakVSwidth',plot_layout))}=gcf;

%
ff.doplot=doplot;
compfig=coolfig.compose(figsall,[-1,-1],1,'normalized',1,0.5*[5,5,5,5]);
%%

%
ff.savefig(gcf,[fname_comp]);
ff.clear(0);
