%% this script plots all the juxtacellular data for all the recordings, as shown in the suppl figure.

%% set up in/out directories and files and figure style (for consistent figure cosmetics)
homedir='..'; %adjust, home directory for in/out
figdir=fullfile(homedir,'figures_juxta'); %ajdjust, folder where figures are saved
tablesdir=fullfile(homedir,'tables_juxta');
datadir=fullfile(homedir,'data_juxta'); %adjust, folder where the raw data are located.

ff=figureFormatter(figdir,1); %figure formatter 
ff.saveformats={'eps3'}; %save format ex: eps3, png, ...
ff.doplot=1; %set to 1 if don't want to save plots (faster), set to 2 to save the plots

%control of figure font size, linewidth, etc...
ff.dim.z=1.2*2;
ff.aflag=1;
ff.ratio=1;
ff.grey2black=1;
ff.p_axes.FontSize=12;
ff.p_axes.LineWidth=1;
ff.p_axes.TickDir='out';
ff.label_sz=12;

%color scheme
ccols=figureFormatter.getDefaultColors(7);
myorange= [0.8706  0.4902  0];
mygray=[0.7,0.7,0.7];
mygreen=[0,117,59]/255;
myblue=ccols{1};
myblue2=[0,173,238]/255;



fname=fullfile(tablesdir,'cells_analysis_input.xlsx');
opts = detectImportOptions(fname);
opts = setvartype(opts,{'phase'},{'char'});

%%
 type2plot={'MC',ccols{2};'GC',ccols{3};'IN',ccols{1};'INAAC',ccols{1};'?',[0.2,0.2,0.2]};


%% extract spikes and show diagnosis plots. 
i2load=35; %data # to process
runall=1;
reextract=struct('spikes',1,'clusters',1,'profiles',1,'stats',0);
T=readtable(fname,opts);
if runall
    i2load=find(T.run);
end
spikes.analyze_batch(T(i2load,:),datadir,reextract)


%% plot the spiking properties
n2plot=50; %number of spike profiles to plot
plot_only_gp2plot=1;  %set to 1 if don't care about other clusters (faster for savign!)
coarsing_factor=10; 
plot_flag=struct('shape',1,'rates',1,'delays',1,'dist',1);
plot_layout={'cell','rates','';'shape','delays','dist'};
plot_flag_nolight_dist=1;
nphases2plot=8;
runall=1;
i2load=33:38;
closeall=1;
doplot=2;
T=readtable(fname,opts);
if runall
    i2load=find(T.run);
end

figsall=repmat({[]},size(plot_layout));

ff.clear(); %clear the figure dump stack
for i=1:length(i2load)
    gp2plot=T.gp2plot(i2load(i));
phases0=T.phase0(i2load(i));
ylims_rate=eval(T.ylimRate{i2load(i)});
    fprintf(1,'Plotting data %g [%g/%g]\n',i2load(i),i,length(i2load));

out=spikes.load_data(datadir,T(i2load(i),:),'juxta','spikes.mat');
ff.dataname=out.dataname;
u=spikes.load_data(datadir,T(i2load(i),:),'juxta','profiles.mat');

mygps=unique(u.gps);

mygps=mygps(mygps>0);
ngps=length(mygps);

nphases=size(out.phases,1);
    phase0=phases0;
    
%     cgp2plot=gp2plot(i);
%     cgp2plot=
    if out.phases(phase0,3)==1
        phase0=phase0+1;
    end
    phase_max=min(nphases,phase0+(nphases2plot-1));


if plot_flag.shape
% get spike profiles

u.f_profs(gp2plot).subrnd(n2plot).plot_linear([],1,{[0,0,0],ccols(1:7)});
%f_profs.plot_linear(gcf,2);
xlim([-1.5,1.5]);
ylim([-1,1]);
set(gca,'XTick',(-1:1:1),'YTick',-1:0.5:1);
xlabel('Time [ms]');
ylabel('Amplitude');
%ff.ratio=4; %increase tick size
ff.doplot=1;
ff.savefig(gcf,'spike_singles',[0.75,0.6,2]);
ff.ratio=1;
figsall{find(strcmp('shape',plot_layout))}=gcf;
end

% chromogram interspike as scatter
if plot_only_gp2plot
    mygps=gp2plot;
end
if plot_flag.delays
for j=1:length(mygps)
    
    
    spikes_clean=out.spikes(u.gps==mygps(j) & out.spikes.id>0,:);
    length(spikes_clean.t)
spikes.plot_chronogram_scatter_horiz(spikes_clean.delay/(out.Fs/1000),spikes_clean,out,coarsing_factor,phase0:phase_max,1,[50,3]);
%ylim([-50/25,50]);
ylabel('Delay [ms]');
%title(['Group: ' num2str(mygps(j))])
set(gca,'YTick',0:10:50);
myrects=findobj(gca,'type','rectangle');
delete(myrects);
ylim([0,50]);
%xlabel('Time [s]');rectangle
ff.doplot=1;
ff.savefig(gcf,['delay_scatter' num2str(mygps(j))],[1.4,0.8,2]);
figsall{find(strcmp('delays',plot_layout))}=gcf;
end
end
if plot_flag.rates
%chronogram spiking rate
if not(isempty(ylims_rate))
    cy=ylims_rate(3:4);
else
    cy=[];
end
spikes.plot_chronogram_rate_horiz(spikes_clean.delay/(out.Fs/1000),spikes_clean,out,coarsing_factor,phase0:phase_max,1,cy);
if not(isempty(ylims_rate))
ylim([ylims_rate(1),max(ylims_rate(2),ylims_rate(3)+ylims_rate(4))]);
end
xlabel('');
set(gca,'XTickLabel',[]);
if not(isempty(ylims_rate))
set(gca,'YTick',round(linspace(ylims_rate(1),ylims_rate(2),4)*100)/100);
else
    cylim=get(gca,'YLim');
    set(gca,'YTick',round(linspace(cylim(1),cylim(2),4)*100)/100);
end
    
%title(['Group: ' num2str(mygps(j))])
ff.doplot=1;
ff.savefig(gcf,['rate_scatter'  num2str(mygps(j))],[1.4,0.5,2]);
figsall{find(strcmp('rates',plot_layout))}=gcf;
end

if plot_flag.dist
    disp('Plotting distribution of delays');
% delay distribution
dt=0.25;
edges=0:dt:50;
delays_hist=histcounts(spikes_clean.delay(spikes_clean.light==1)/out.Fs*1000,edges);
delays_hist2=histcounts(spikes_clean.delay(spikes_clean.light==0)/out.Fs*1000,edges);
figure;
if plot_flag_nolight_dist
    plot(delays_hist2/sum(delays_hist2)/dt,edges(1:end-1)+dt/2,'Color',[0,0,0],'LineWidth',1.5);
hold all
end
plot(delays_hist/sum(delays_hist)/dt,edges(1:end-1)+dt/2,'Color',myblue2,'LineWidth',3);

%ylabel('Delay [ms]');
xlabel('Spike fraction');
ylim([0,50]);
xmax=max(delays_hist/sum(delays_hist)/dt);
xlim([0,ceil(xmax/0.05)*0.05]);
%title(['Group: ' num2str(mygps(j))])
set(gca,'YTick',0:10:50);
set(gca,'XTick',linspace(0,ceil(xmax/0.05)*0.05,3));
%ff.ratio=4;
ff.doplot=1;
ff.savefig(gcf,['delay_dist'  num2str(mygps(j))],[0.5,0.8,2]);
ff.ratio=1;
figsall{find(strcmp('dist',plot_layout))}=gcf;
end

figtext=figure;
text(0.5,0.7,['Cell #' T.name{i2load(i)}],'FontSize',16,'HorizontalAlignment','center','VerticalAlignment','middle');
celltype=[T.desc{i2load(i)}];
if T.epileptic(i2load(i))
    celltype=[celltype ' *'];
end

celltypeid=find(strcmp(T.desc{i2load(i)},type2plot(:,1)));
txtcol=[0,0,0];
if not(isempty(celltypeid))
    txtcol=type2plot{celltypeid(1),2};
end

mytext=text(0.5,0.5,celltype,'FontSize',16,'HorizontalAlignment','center','VerticalAlignment','middle','Color',txtcol);
if T.confirmed(i2load(i))
    set(mytext,'EdgeColor','k');
end
ff.doplot=1;
ff.savefig(gcf,'cell',[0.6,0.6,2]);
set(gca,'Visible','off');
figsall{find(strcmp('cell',plot_layout))}=figtext;


compfig=coolfig.compose(figsall,[-1,-1],1,'normalized',1,0.5*[5,5,5,5]);



ff.doplot=doplot;
ff.savefig(gcf,['allplots'  num2str(mygps(j))]);

if closeall
close all
end
end
%
%now dump the figure stack awesome!
scalefactor=0.5;
ff.dataname='juxta_summary_noOpsin';
ff.stack_2latex(scalefactor,'firing_all_short.tex',0,1,1);
ff.clear(1);
 
%% compute all the statistics
i2load=1; %data # to process
runall=1;
rerun=1; %if 0, do not rerun the spike finding (faster), just plot the diagnosis plots. Needs to be 1 if change xls file
nboot=1000;

T=readtable(fname,opts);
if runall
    i2load=find(T.run);
end
  disp('Computing stats');

  for i=1:length(i2load)
      fprintf(1,'Computing stats for data %g [%g/%g]\n',i2load(i),i,length(i2load));
      out=spikes.load_data(datadir,T(i2load(i),:),'juxta','spikes.mat');
      u=spikes.load_data(datadir,T(i2load(i),:),'juxta','profiles.mat');

      [stats0,stats1]=spikes.summarize_statistics(out,u.gps,nboot);
      save(fullfile(out.folder,[out.dataname '_stats.mat']),'stats0','stats1');
  end

  
 
   %% summarize all statistics
  fname_out=fullfile(tablesdir,'cells_analysis_out.xlsx');
  T=readtable(fname,opts);
  var2keep={'date','id','name','desc','epileptic','confirmed','side','lim','gp2plot','phase'};
  Trun=cat(2,array2table((1:sum(T.run==1))','VariableNames',{'cellID'}),T(T.run==1,var2keep));
  [T0,T1]=spikes.load_stats(datadir,Trun,'juxta','stats.mat');
  Tout=cat(2,Trun,T0,T1);
  writetable(Tout,fname_out);
 

