%% this script plots examples of recordings

%% set up in/out directories and files and figure style (for consistent figure cosmetics)
homedir='..'; %adjust, home directory for in/out
figdir=fullfile(homedir,'figures_juxta'); %ajdjust, folder where figures are saved
tablesdir=fullfile(homedir,'tables_juxta');
datadir=fullfile(homedir,'data_juxta'); %adjust, folder where the raw data are located.

ff=figureFormatter(figdir,1); %figure formatter 
ff.saveformats={'eps3'}; %save format ex: eps3, png, ...
ff.doplot=1; %set to 1 if don't want to save plots (faster), set to 2 to save the plots

%control of figure font size, linewidth, etc...
ff.dim.z=1.2*2;
ff.aflag=1;
ff.ratio=1;
ff.grey2black=1;
ff.p_axes.FontSize=12;
ff.p_axes.LineWidth=1;
ff.p_axes.TickDir='out';
ff.label_sz=12;

%color scheme
ccols=figureFormatter.getDefaultColors(7);
myorange= [0.8706  0.4902  0];
mygray=[0.7,0.7,0.7];
mygreen=[0,117,59]/255;
myblue=ccols{1};
myblue2=[0,173,238]/255;



fname=fullfile(tablesdir,'cells_analysis_input.xlsx');
opts = detectImportOptions(fname);
opts = setvartype(opts,{'phase'},{'char'});
%%
  type2plot={'MC',[189,30,45]/255;'GC',ccols{3};'IN',ccols{1};'INAAC',ccols{1};'?',[0.2,0.2,0.2]};
sdata=120;


%% Plot all the traces examples
ff.dataname='traces_examples_main';
doplot=2;

T=readtable(fname,opts);
Trun=T(T.run==1,:);
ff.clear(0);


%% ______ interneuron example 5v single spike ____________
fign='IN-5v-single';
ix2load=find(strcmp(Trun.name,'5v'))
thiscol=type2plot{find(strcmp(type2plot(:,1),'IN')),2};

out=spikes.load_data(data_root,Trun(ix2load,:),'juxta','spikes.mat');
%u=spikes.load_data(data_root,Trun(ix2load,:),'juxta','profiles.mat');


%%
Fs=out.Fs;
Fc_low=500;
Fc_high=9999;
[b,a] = butter(4, [Fc_low/(Fs/2) Fc_high/(Fs/2)]);
data2=filtfilt(b,a,double(out.data(:,1)));
%%
ff.clear();
allfigs=repmat({[]},2,2);
phaseix=13;
lms=[0,0.1];
[ixs,tt,yy]=spikes.get_data(data2,out.phases(phaseix,1)/out.Fs,lms,out.Fs,1);
figure, plot(tt,yy,'Color','k');
hold all
myspks=out.spikes.t>=ixs(1) & out.spikes.t<ixs(2);
% scatter((out.spikes.t(myspks)-out.phases(phaseix,1))/out.Fs,out.data(out.spikes.t(myspks),1),...
%     'Marker','o','MarkerEdgeColor','r');


h_l=spikes.plot_laser(gca,out.laser.t,out.phases(phaseix,1)/out.Fs,lms,out.Fs,1,0.01,[2.3,0.2]);
h_m=spikes.plot_laser(gca,out.mock.t,out.phases(phaseix,1)/out.Fs,lms,out.Fs,1,0.01,[2.3,0.2]);
set(h_l,'FaceColor',myblue2);
set(h_m,'FaceColor',mygray);
ylim([-0.5,2.5])
xlim([0,0.07]);
plot([0,0,0.01],[0,-1+0.5,-1+0.5],'Color','k','LineWidth',1);
set(gca,'Visible','off');
ff.savefig(gcf,'tmp',[1,0.4,2],1);
allfigs{1}=gcf;

%
phaseix=13;
lms=[15,30];

[ixs,tt,yy]=spikes.get_data(data2,out.phases(phaseix,1)/out.Fs,lms,out.Fs,1);
figure, plot(tt,yy,'Color','k');
hold all
myspks=out.spikes.t>=ixs(1) & out.spikes.t<ixs(2);
% scatter((out.spikes.t(myspks)-out.phases(phaseix,1))/out.Fs,out.data(out.spikes.t(myspks),1),...
%     'Marker','o','MarkerEdgeColor','r');

%plot_phases(ax,phases,tmiddle,d,Fs,center,vals)
h_l=spikes.plot_phases(gca,out.phases,out.phases(phaseix,1)/out.Fs,lms,out.Fs,1,[2,0.2]);
%h_m=spikes.plot_laser(gca,out.mock.t,out.phases(phaseix,1)/out.Fs,lms,out.Fs,1,0.01,[2.3,0.2]);
set(h_l,'FaceColor',myblue2);
%set(h_m,'FaceColor',mygray);
ylim([-1,2.2])
plot([-20,-20,-10],[0,-1,-1],'Color','k','LineWidth',1);
set(gca,'Visible','off');
ff.savefig(gcf,'tmp',[2,0.4,2],1);
allfigs{2}=gcf;

 
%_
phaseix=14;
lms=[1,1];

[ixs,tt,yy]=spikes.get_data(data2,out.phases(phaseix,1)/out.Fs,lms,out.Fs,1);
figure, plot(tt,yy,'Color','k');
hold all
myspks=out.spikes.t>=ixs(1) & out.spikes.t<ixs(2);
% scatter((out.spikes.t(myspks)-out.phases(phaseix,1))/out.Fs,out.data(out.spikes.t(myspks),1),...
%     'Marker','o','MarkerEdgeColor','r');

%h_l=plot_phases(ax,phases,tmiddle,d,Fs,center,vals);
h_l=spikes.plot_laser(gca,out.laser.t,out.phases(phaseix,1)/out.Fs,lms,out.Fs,1,0.01,[2.3,0.2]);

%%h_l=spikes.plot_phases(gca,out.phases,out.phases(phaseix,1)/out.Fs,lms,out.Fs,1,[2,0.2]);
h_m=spikes.plot_laser(gca,out.mock.t,out.phases(phaseix,1)/out.Fs,lms,out.Fs,1,0.01,[2.3,0.2]);
set(h_l,'FaceColor',myblue2);
set(h_m,'FaceColor',mygray);
ylim([-1,2.5])
xlim([-1,1]);
plot([-1,-1,-0.8],[0,-1,-1],'Color','k','LineWidth',1);
set(gca,'Visible','off');
ff.savefig(gcf,'tmp',[2,0.4,2],1);
allfigs{3}=gcf;


compfig=coolfig.compose(allfigs,[0,-1],1,'normalized',1,0.5*[5,5,5,5]);
ff.savefig(compfig,'all',[],2);

%


scalefactor=0.5;
ff.stack_2latex(scalefactor,'all.tex',0,1,1);
ff.clear(1);


%%
lims2plot={[out.phases(9,1)/out.Fs,0.05,0.1],[0.4,0.4,2],[0.02,2]};
center=1;
ff.doplot=1;

rng=[-2,2.5]
figsall=repmat({[]},size(lims2plot,1),1);

lims=lims2plot{1};
[hax,hs,hs_l,hs_s]=spikes.plot([],out.data(:,1),out.spikes.t,out.mock.t,lims,out.Fs,center,10e-3,[],[0.2,0.1,0.3]);
set(hs,'LineWidth',0.6);
set(hs_s,'Visible','off'); %'Color','k','LineWidth',1);
set(hs_l,'FaceColor',myblue2,'LineWidth',1);
%xlim([-lims2plot{ii,1}(2),lims2plot{ii,1}(3)]);
%ylim([-4.5,3.5])
%plotdim=lims2plot{ii,2};
%ff.savefig(gcf,lims2plot{ii,2},plotdim);


%%
% ______ interneuron example 5v ____________
fign='IN-5v';
ix2load=find(strcmp(Trun.name,'5v'))
thiscol=type2plot{find(strcmp(type2plot(:,1),'IN')),2};

out=spikes.load_data(data_root,Trun(ix2load,:),'juxta','spikes.mat');
%u=spikes.load_data(data_root,Trun(ix2load,:),'juxta','profiles.mat');

lims2plot={[out.phases(9,1)/out.Fs,0.6,0.6],[0.9,0.4,2],[0.2,2];...
    [out.phases(9,1)/out.Fs,0.02,0.08],[0.4,0.4,2],[0.02,2]};
center=1;
ff.doplot=1;

rng=[-2,2.5]
figsall=repmat({[]},size(lims2plot,1),1);
for ii=1:size(lims2plot,1)
lims=lims2plot{ii,1};
[hax,hs,hs_l,hs_s]=spikes.plot([],out.data(:,1),out.spikes.t,out.laser.t,lims,out.Fs,center,10e-3,rng,[0.2,0.1,0.3]);
set(hs,'LineWidth',0.6);
set(hs_s,'Color','k','LineWidth',1);
set(hs_l,'FaceColor',myblue2,'LineWidth',1);
xlim([-lims2plot{ii,1}(2),lims2plot{ii,1}(3)]);
ylim([-4.5,3.5])
plotdim=lims2plot{ii,2};
ff.savefig(gcf,lims2plot{ii,2},plotdim);

hold all
plot([-lims2plot{ii,1}(2),lims2plot{ii,3}(1)-lims2plot{ii,1}(2)],...
    [-4.5,-4.5],'Color','k','LineWidth',2);
plot([-lims2plot{ii,1}(2),-lims2plot{ii,1}(2)],[-4.5,lims2plot{ii,3}(2)-4.5],...
    'Color','k','LineWidth',2);
set(gca,'Visible','off')
if ii==1
text(-lims2plot{ii,1}(2)+0.1,-4.5+0.2,fign,'HorizontalAlignment','left','VerticalAlignment','bottom');
end
%set(gca,'Visible','off');
figsall{ii}=gcf;
end

 compfig=coolfig.compose(figsall',[0,-1],1,'normalized',1,0.5*[5,5,5,5]);
 ff.doplot=doplot;
ff.savefig(compfig,fign);


 % ___________ GC example 9v _____________________
 fign='GC-9v';
ix2load=find(strcmp(Trun.name,'9v'));
thiscol=type2plot{find(strcmp(type2plot(:,1),'GC')),2};


out=spikes.load_data(data_root,Trun(ix2load,:),'juxta','spikes.mat');
%u=spikes.load_data(data_root,Trun(ix2load,:),'juxta','profiles.mat');

tnum=3;
lims2plot={[out.phases(tnum,1)/out.Fs,0.6,0.6],[0.9,0.4,2],[0.2,2];...
    [out.phases(tnum,1)/out.Fs+0.05,0.02,0.08],[0.4,0.4,2],[0.02,2]};

% lims2plot={[out.phases(tnum,1)/out.Fs,1,1],[2.5,0.5,2];...
%     [out.phases(tnum,1)/out.Fs,0.02,0.08],[1.0,0.5,2]};
center=1;
ff.doplot=1;
rng=[-1,2]
figsall=repmat({[]},size(lims2plot,1),1);
for ii=1:size(lims2plot,1)
lims=lims2plot{ii,1};
[hax,hs,hs_l,hs_s]=spikes.plot([],out.data(:,1),out.spikes.t,out.laser.t,lims,out.Fs,center,10e-3,rng,[0.2,0.1,0.3]);

set(hs,'LineWidth',0.6);
set(hs_s,'Color','k','LineWidth',1);
set(hs_l,'FaceColor',myblue2,'LineWidth',1);

xlim([-lims2plot{ii}(2),lims2plot{ii}(3)]);
ylim([-3,2])
plotdim=lims2plot{ii,2};
ff.savefig(gcf,lims2plot{ii,2},plotdim);
hold all
plot([-lims2plot{ii,1}(2),lims2plot{ii,3}(1)-lims2plot{ii,1}(2)],...
    [-3,-3],'Color','k','LineWidth',2);
plot([-lims2plot{ii,1}(2),-lims2plot{ii,1}(2)],[-3,lims2plot{ii,3}(2)-3],...
    'Color','k','LineWidth',2);
set(gca,'Visible','off');
if ii==1
text(-lims2plot{ii,1}(2)+0.1,-3+0.2,fign,'HorizontalAlignment','left','VerticalAlignment','bottom');
end
figsall{ii}=gcf;
end
compfig=coolfig.compose(figsall',[0,-1],1,'normalized',1,0.5*[5,5,5,5]);
ff.doplot=doplot;
ff.savefig(compfig,fign);

% _______ MC example 7v __________________
 fign='MC-7v';
ix2load=find(strcmp(Trun.name,'7v'));
thiscol=type2plot{find(strcmp(type2plot(:,1),'MC')),2};
out=spikes.load_data(data_root,Trun(ix2load,:),'juxta','spikes.mat');
%u=spikes.load_data(data_root,Trun(ix2load,:),'juxta','profiles.mat');

tnum=2;
lims2plot={[out.phases(tnum,1)/out.Fs,2.5,2.5],[0.9,0.4,2],[0.2,2];...
    [out.phases(tnum,1)/out.Fs+1.55,0,0.1],[0.4,0.4,2],[0.02,2]};
% lims2plot={[out.phases(tnum,1)/out.Fs,3,3],[2.5,0.5,2];...
%     [out.phases(tnum,1)/out.Fs+1.55,0,0.1],[1.0,0.5,2]};
center=1;
ff.doplot=1;

rng=[-1.5,1]
figsall=repmat({[]},size(lims2plot,1),1);
for ii=1:size(lims2plot,1)
lims=lims2plot{ii,1};
[hax,hs,hs_l,hs_s]=spikes.plot([],out.data(:,1),out.spikes.t,out.laser.t,lims,out.Fs,center,10e-3,rng,[0.2,0.1,0.3]);

set(hs,'LineWidth',0.6);
set(hs_s,'Color','k','LineWidth',1);
set(hs_l,'FaceColor',myblue2,'LineWidth',1);
xlim([-lims2plot{ii}(2),lims2plot{ii}(3)]);
ylim([-3,1])
plotdim=lims2plot{ii,2};

ff.savefig(gcf,lims2plot{ii,2},plotdim);
hold all
plot([-lims2plot{ii,1}(2),lims2plot{ii,3}(1)-lims2plot{ii,1}(2)],...
    [-3,-3],'Color','k','LineWidth',2);
plot([-lims2plot{ii,1}(2),-lims2plot{ii,1}(2)],[-3,lims2plot{ii,3}(2)-3],...
    'Color','k','LineWidth',2);
if ii==1
text(-lims2plot{ii,1}(2)+0.1,-3+0.2,fign,'HorizontalAlignment','left','VerticalAlignment','bottom');
end
set(gca,'Visible','off')
figsall{ii}=gcf;
figsall{ii}=gcf;
end
compfig=coolfig.compose(figsall',[0,-1],1,'normalized',1,0.5*[5,5,5,5]);

ff.doplot=doplot;
ff.savefig(compfig,fign);



% ___________ MC example 8v _____________________
 fign='MC-8v';
ix2load=find(strcmp(Trun.name,'8v'));
thiscol=type2plot{find(strcmp(type2plot(:,1),'GC')),2};


out=spikes.load_data(data_root,Trun(ix2load,:),'juxta','spikes.mat');
%u=spikes.load_data(data_root,Trun(ix2load,:),'juxta','profiles.mat');

tnum=3;
lims2plot={[out.phases(tnum,1)/out.Fs,0.6,0.6],[0.9,0.4,2],[0.2,2];...
    [out.phases(tnum,1)/out.Fs+0.05,0.02,0.08],[0.4,0.4,2],[0.02,2]};

% lims2plot={[out.phases(tnum,1)/out.Fs,1,1],[2.5,0.5,2];...
%     [out.phases(tnum,1)/out.Fs,0.02,0.08],[1.0,0.5,2]};
center=1;
ff.doplot=1;
rng=[-0.5,0.8]
figsall=repmat({[]},size(lims2plot,1),1);
for ii=1:size(lims2plot,1)
lims=lims2plot{ii,1};
[hax,hs,hs_l,hs_s]=spikes.plot([],out.data(:,1),out.spikes.t,out.laser.t,lims,out.Fs,center,10e-3,rng,[0.2,0.1,0.3]);

set(hs,'LineWidth',0.6);
set(hs_s,'Color','k','LineWidth',1);
set(hs_l,'FaceColor',myblue2,'LineWidth',1);

xlim([-lims2plot{ii}(2),lims2plot{ii}(3)]);
ylim([-1.4,0.8])
plotdim=lims2plot{ii,2};
ff.savefig(gcf,lims2plot{ii,2},plotdim);
hold all
plot([-lims2plot{ii,1}(2),lims2plot{ii,3}(1)-lims2plot{ii,1}(2)],...
    [-1.4,-1.4],'Color','k','LineWidth',2);
plot([-lims2plot{ii,1}(2),-lims2plot{ii,1}(2)],[-1.4,lims2plot{ii,3}(2)-1.4],...
    'Color','k','LineWidth',2);
set(gca,'Visible','off');
if ii==1
text(-lims2plot{ii,1}(2)+0.1,-3+0.2,fign,'HorizontalAlignment','left','VerticalAlignment','bottom');
end
figsall{ii}=gcf;
end
compfig=coolfig.compose(figsall',[0,-1],1,'normalized',1,0.5*[5,5,5,5]);
ff.doplot=doplot;
ff.savefig(compfig,fign);
%

scalefactor=0.5;
ff.stack_2latex(scalefactor,'all.tex',0,1,1);
ff.clear(1);
