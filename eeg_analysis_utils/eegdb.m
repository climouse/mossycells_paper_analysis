classdef eegdb < handle
    properties
        data=table(); % the file data
        %         name={};
        %         pathstr={};
        %         t0=[];
        %         t1=[];
        %         dt=[];
        name=''; % description of the database
        partialload=0;
        simplefiles=0;
        
    end
    
    methods
        function obj=eegdb()
        end
        
        function makedb(obj,folders,pattern,maxdepth) %create a database with all the filenames and t0 tend timestamps
            if nargin<3
                pattern='*';
            end
            fnames={};
            for i=1:length(folders)
                fprintf(1,'Parsing folder : %s \n',folders{i});
                alls=['"' pattern '.mat"'];
                findcmd=['find "' folders{i} '" -maxdepth ' num2str(maxdepth) ' -type f \( -name ' alls ' \)']
                [~,list]=system(findcmd);
                
                if not(isempty(list))
                    list=strsplit(list,'\n')';
                    xxxx=list(1:end-1)';
                    fnames=cat(2,fnames,list(1:end-1)');
                    
                end
            end
            fnames=fnames(cellfun(@(x) isempty(x),strfind(fnames,'cursors'),'uni',1));
            nfiles=length(fnames);
            pathstr=repmat({''},nfiles,1);
            name=repmat({''},nfiles,1);
            t0=zeros(nfiles,1);
            t1=zeros(nfiles,1);
            dt=zeros(nfiles,1);
            
            for i=1:nfiles
                fprintf(1,'Parsing file %s (%g/%g) \n',fnames{i},i,nfiles);
                [cpathstr,cname,cext]=fileparts(fnames{i});
                cname=[cname cext];
                pathstr{i}=cpathstr;
                name{i}=cname;
                u=load(fnames{i},'trdata','fs');
                t0(i)=u.trdata(1).timestamp(1);
                t1(i)=u.trdata(1).timestamp(end);
                dt(i)=1000/u.fs;
            end
            
            %now sort the database
            [~,srtidxs]=sort(t0);
            t0=t0(srtidxs);
            t1=t1(srtidxs);
            dt=dt(srtidxs);
            name=name(srtidxs);
            pathstr=pathstr(srtidxs);
            obj.data=table(pathstr,name,t0,t1,dt,'VariableNames',...
                {'pathstr','name','t0','t1','dt'});
        end
        
        
        function idx=query(obj,t) %find the appropriate file
            
            i1=find(obj.data.t0>t,1);
            idx=[0,0];
            if isempty(i1)
                i1=length(obj.data.t0)+1;
            else
                i1=i1(1);
            end
            if i1>1
                if obj.data.t1(i1-1)>t & obj.data.t0(i1-1)<=t
                    idx(1)=i1-1;
                    idx(2)=(t-obj.data.t0(i1-1))*24*60;
                end
            end
            
        end
        
        function coord=coordinates(obj,t,T)
            coord=zeros(length(t),3); %file, ix0, ix1
            nT=length(T);
            for i=1:length(t)
                coord(i,1:2)=obj.query(t(i));
                if coord(i,1)>0
                    cdt=obj.data.dt(coord(i,1));
                    coord(i,2)=max(1,floor(coord(i,2)*60*1000/cdt));
                    coord(i,3)=coord(i,2)+ceil(T(min(i,nT))*1000/cdt);
                end
            end
        end
        
        function ee=loadeeg(obj,t,T,chans,padding,trigsdb) %trigs bd can be either a triggerdb or a sz db
            coord=obj.coordinates(t,T);
            n=size(coord,1);
            ee(n,1)=eeg();
           
            if nargin<5
                trigsdb=[];
            end
            if nargin<4
                padding=[0,0];
            end
            ci=0;
            if obj.simplefiles & chans>1
                display('Simple file mode, using assigned channel instead');
                chans=1;
            end
            
            mytrigsdb=[];
            myszdb=[];
            if not(isempty(trigsdb))
                if isa(trigsdb,'triggersdb');
                    mytrigsdb=trigsdb;
                else
                    mytrigsdb=trigsdb.triggersdb;
                    myszdb=trigsdb;
                end
            end
            
            
            for i=1:n
                if coord(i,1)>0
                    
                    fprintf(1,'Loading eeg %g/%g \n',i,n);
                    ee(i).t0=t(i);
                    dt=obj.data.dt(coord(i,1));
                    ee(i).ts=dt;
                    cfile=fullfile(obj.data.pathstr{coord(i,1)},obj.data.name{coord(i,1)});
                    ee(i).file=cfile;
                    %whos cfile
                    if not(coord(i,1)==ci)
                        if obj.partialload
                            u=matfile(cfile);
                        else
                            u=load(cfile,'sbuf','trdata');
                        end
                        ci=coord(i,1);
                    end
                    [ix0padded,flag0padded]=max([coord(i,2)-ceil(padding(1)*1000/dt),1]);
                    [ix1padded,flag1padded]=min([coord(i,3)+ceil(padding(2)*1000/dt),size(u.sbuf,1)]);
                    [ix0,flag0]=max([coord(i,2),1]);
                    [ix1,flag1]=min([coord(i,3),size(u.sbuf,1)]);
                    ee(i).ix=[ix0padded,ix1padded,ix0,ix1];
                    ee(i).flag=[flag0padded,flag1padded,flag0,flag1]-1;
                    t0file=obj.data.t0(coord(i,1));
                    
                    ee(i).y=u.sbuf(ix0padded:ix1padded,chans(min(i,length(chans))));
                    ee(i).t=(ix0padded:ix1padded)'*dt/1000-ix0*dt/1000; %(ix0padded-ix0)*dt/1000:(dt/1000):(ix1padded-ix0)
                    ee(i).tmM=[t0file+(ix0padded-1)*dt/(1000*60*60*24),t0file+(ix1padded-1)*dt/(1000*60*60*24)];
                    if not(isempty(mytrigsdb))
                        mytrigs=mytrigsdb.query_interval(ee(i).tmM(1),(ee(i).tmM(2)-ee(i).tmM(1))*24*60*60,chans(min(i,length(chans))));
                        %mytrigs=find(trigs.t0>=ee(i).tmM(1) & trigs.t0<=ee(i).tmM(2),)
                        
                        %mytrigs=[trigid,fileid,t[mn],led,t0]
                        %mytrigs
                        if not(isempty(mytrigs));
                            
                            %ee(i).trigs=[(trigs.t0(mytrigs)-ee(i).tmM(1))*24*60*60*1/dt+ee(i).t(1),trigs.led(mytrigs),trigs.bhflag(mytrigs)];
                            ee(i).trigs=[(mytrigs(:,5)-ee(i).tmM(1))*24*60*60*1/1+ee(i).t(1),mytrigs(:,4),zeros(size(mytrigs,1),1)]; %rigs.bhflag(mytrigs)];
                            %ee(i).trigs=[time_local(s),led,bhflag];
                            
                            if not(isempty(myszdb))
                                sztrigs=myszdb.triggers; %[sznumer, t0, flag, light, trignumberInTrigDB]
                                [insz,locb]=ismember(sztrigs.trigid,mytrigs(:,1));
                                ee(i).trigs(locb(locb>0),3)=sztrigs.bhflag(insz);
                                szix=find(myszdb.data.t0>=ee(i).tmM(1) & myszdb.data.t0<=ee(i).tmM(2));
                                
                                if not(isempty(szix))
                                    t0s=myszdb.data.t0(szix);
                                    thesesz=[(t0s-ee(i).tmM(1))*24*60*60*1/1+ee(i).t(1),myszdb.data.T_focal(szix),myszdb.data.T_total(szix)];
                                    thesesz=[thesesz(:,1),thesesz(:,1)+thesesz(:,2),thesesz(:,1)+thesesz(:,3)];
                                ee(i).szs=thesesz;
                                end
                            end
                        end
                    end
                    
                    
                    
                end
            end
        end
    end
    
end
