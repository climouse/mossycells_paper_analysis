classdef eeg_io < handle
    
    
    properties
        root='.';
        format={'mm/dd/yy','HH_MM_SS'};
        format_t0='HH:MM:SS';
        format_file={'yyyy_mm_dd___HH_MM_SS','yyyy_mmm_dd___HH_MM_SS'};
        db=[]; % the database of files
    end
    
    methods
        function obj=eeg_io(root,f)
            if nargin>0
                obj.root=root;
            end
            if nargin>1
                obj.f=f;
            end
        end
        
        function load_db(fname)
            
        end
        
        function idx=query(t) %find the appropriate file
            
        end
        
        %function 
        
        
        
        function [name,pathstr,dnum,dnumDay]=get_filename(obj,d,t)
            
            dnum=datenum([d ' ' t],[obj.format{1} ' ' obj.format{2}]);
            dnumDay=datenum(d,[obj.format{1}]);
            s=cellfun(@(x) [' "*_',datestr(dnum,x) '.mat" -o -name'],obj.format_file,'uni',0);
            %s{1}
            alls=strcat(s{:});
            alls=alls(1:end-8);
            %s2find=['"*_{' alls '}.mat"']
            pathstr='';
            name='';
            
            if isunix
                findcmd=['find "' obj.root '" -type f \( -name' alls ' \)'];
                [~,list]=system(findcmd);
                
            end
            if not(isempty(list))
                list=strsplit(list,'\n')';
                
                if length(list)>2
                    
                    warning('Multiple occurence of file');
                    disp(list(1:end-1))
                    
                end
                list=list{1};
                
                [pathstr,name,ext]=fileparts(list);
                name=[name ext];
            end
        end
        
        function [fnames,dnums,dnumDays]=get_filenames(obj,ds,ts)
            if not(iscell(ds))
                [name,pathstr,dnum,dnumDays]=obj.get_filename(ds,ts);
                fnames={fullfile(pathstr,name)};
                dnums=dnum;
            else
                dnums=zeros(length(ds),1);
                dnumDays=dnums;
                for i=length(ds):-1:1
                    [name,pathstr,dnum,dnumDay]=obj.get_filename(ds{i},ts{i});
                    fnames{i,1}=fullfile(pathstr,name);
                    dnums(i,1)=dnum;
                    dnumDays(i,1)=dnumDay;
                end
            end
        end
        
        function [timeabs,dt,i]=timeRel2Abs(obj,dnum,dnumsDay,s_time,ts)
            s_dnumsDay=datestr(dnumsDay,obj.format{1});
            %s_time
            %xx=[s_dnumsDay ' ' s_time];
            timeabs=datenum([s_dnumsDay ' ' s_time],[obj.format{1} ' ' obj.format_t0]);
            if timeabs<dnum
                s_dnumsDay=datestr(dnumsDay+1,obj.format{1});
                timeabs=datenum([s_dnumsDay ' ' s_time],[obj.format{1} ' ' obj.format_t0]);
            end
            %datetime(datevec(timeabs))
            dt=seconds(datetime(datevec(timeabs))-datetime(datevec(dnum)));
            i=ceil(dt/ts);
        end
        
        function myeeg=load_data(obj,d,t,t0,t1,chans,padding)
            [fnames,dnums,dnumDays]=obj.get_filenames(d,t);
            n=length(dnums);
            cdnum=0;
            u=[];
            myeeg(n,1)=eeg();
            
            if nargin<7
                padding=[0,0];
            end
            padding=repmat(padding,1,int32(2/length(padding)));
            
            if not(isempty(t0))
                if not(iscell(t0))
                    t0={t0};
                end
            end
%             if not(isempty(t1))
%                 if not(iscell(t1))
%                     t1={t1};
%                 end
%             end
            
            for i=1:n
                fprintf(1,'Loading data %g[/%g] : %s\n',i,n,datestr(dnums(i)));
                if not(dnums(i)==cdnum)
                    cdnum=dnums(i);
                    u=load(fnames{i});
                    %fprintf(1,'New file\n');
                end
                ts= 1/u.fs; %sampling period
                myeeg(i).ts=ts;
                myeeg(i).file=fnames{i};
                myeeg(i).t0=repmat(dnums(i),1,5);
                i0=1;
                imax=size(u.sbuf,1);
                i1=imax;
                i1_nopad=imax;
                i0_nopad=1;
                
                if not(isempty(t0)) & iscell(t0)
                    %cdnum
                    %cdumDay=dnumDays(i);
                    %ct0=t0{i};
                   % tmp=cdnum-dnumDays(i)
                    [~,ix]=obj.timeRel2Abs(cdnum,dnumDays(i),t0{i},ts);
                    i0_nopad=max(1,(floor(ix/ts)));
                    i0=max(1,(floor((ix-padding(1))/ts)));
                    
                  
               
                end
%                 
%                 if not(isempty(t1)) & iscell(t1)
%                     [~,ix]=obj.timeRel2Abs(cdnum,dnumDays(i),t1{i},ts);
%                     ix=min(int32(round(ix+padding(2)/ts));
%                     i1=min(ix,imax);
%                 else
                    if not(isempty(t1))
                        i1_nopad=min(i0_nopad+(ceil(t1(i)/ts)),imax);
                        i1=min(i0_nopad+(ceil((t1(i)+padding(2))/ts)),imax);
                        
                    end
                    
                %end
               
                myeeg(i).t0(2:5)=ts*[i0-1,i1-1,i0_nopad-1,i1_nopad-1];
                
                myeeg(i).y=u.sbuf(i0:i1,chans);
                %ct=0:ts:(i1-i0);
                myeeg(i).t=-padding(1)+(1:(i1-i0+1))'*myeeg(i).ts;
                
                
            end
            
        end
        
        
        function myeeg=load_data2(obj,d,t,t0,t1,chans,padding)
            [fnames,dnums,dnumDays]=obj.get_filenames(d,t);
            n=length(dnums);
            cdnum=0;
            u=[];
            myeeg(n,1)=eeg();
            
            if nargin<7
                padding=[0,0];
            end
            padding=repmat(padding,1,int32(2/length(padding)));
            
            if not(isempty(t0))
                if not(iscell(t0))
                    t0={t0};
                end
            end
%             if not(isempty(t1))
%                 if not(iscell(t1))
%                     t1={t1};
%                 end
%             end
            
            for i=1:n
                fprintf(1,'Loading data %g[/%g] : %s\n',i,n,datestr(dnums(i)));
                if not(dnums(i)==cdnum)
                    cdnum=dnums(i);
                    u=load(fnames{i});
                    %fprintf(1,'New file\n');
                end
                ts= 1/u.fs; %sampling period
                myeeg(i).ts=ts;
                myeeg(i).file=fnames{i};
                myeeg(i).t0=repmat(dnums(i),1,5);
                i0=1;
                imax=size(u.sbuf,1);
                i1=imax;
                i1_nopad=imax;
                i0_nopad=1;
                
                if not(isempty(t0)) & iscell(t0)
                    %cdnum
                    %cdumDay=dnumDays(i);
                    %ct0=t0{i};
                   % tmp=cdnum-dnumDays(i)
                    %[~,ix]=obj.timeRel2Abs(cdnum,dnumDays(i),t0{i},ts);
                    ix=(datenum(t0{i},'dd/mm/yy HH:MM:SS')-cdnum)*24*60*60;
                    i0_nopad=max(1,(floor(ix/ts)));
                    i0=max(1,(floor((ix-padding(1))/ts)));
                    
                  
               
                end
%                 
%                 if not(isempty(t1)) & iscell(t1)
%                     [~,ix]=obj.timeRel2Abs(cdnum,dnumDays(i),t1{i},ts);
%                     ix=min(int32(round(ix+padding(2)/ts));
%                     i1=min(ix,imax);
%                 else
                    if not(isempty(t1))
                        i1_nopad=min(i0_nopad+(ceil(t1(i)/ts)),imax);
                        i1=min(i0_nopad+(ceil((t1(i)+padding(2))/ts)),imax);
                        
                    end
                    
                %end
               
                myeeg(i).t0(2:5)=ts*[i0-1,i1-1,i0_nopad-1,i1_nopad-1];
                
                myeeg(i).y=u.sbuf(i0:i1,chans);
                %ct=0:ts:(i1-i0);
                myeeg(i).t=-padding(1)+(1:(i1-i0+1))'*myeeg(i).ts;
                
                
            end
            
        end
        
        function myeeg=load_data_table(obj,T,padding)
            if nargin<3
                padding=[0,0];
            end
            myeeg=obj.load_data2(T.date,T.hour,T.t_szstart,T.T_total,T.ch,padding)
        end
        

        
    end
    methods(Static)
        function db=makedb(folders,pattern) %create a database with all the filenames and t0 tend timestamps
            if nargin<2
                pattern='*';
            end
            %
            
     
            fnames={};
            for i=1:length(folders)
                fprintf(1,'Parsing folder : %s \n',folders{i});
                
                
            
            
             alls=['"' pattern '.mat"'];
            %s{1}
            
                findcmd=['find "' folders{i} '" -maxdepth 1 -type f \( -name ' alls ' \)'];
                [~,list]=system(findcmd);
            
%             
             if not(isempty(list))
                list=strsplit(list,'\n')';
                fnames=cat(1,list(1:end-1)');
             end
            end
            nfiles=length(fnames)
            db=struct('pathstr',{repmat({''},nfiles,1)},'name',{repmat({''},nfiles,1)},...
                'dt',zeros(nfiles,1),'t0',zeros(nfiles,1),'t1',zeros(nfiles,1));
            
            for i=1:nfiles
                fprintf(1,'Parsing file %s (%g/%g) \n',fnames{i},i,nfiles);
               [pathstr,name,ext]=fileparts(fnames{i});             
               name=[name ext];
                db.pathstr{i}=pathstr;
              db.name{i}=name;
              u=load(fnames{i},'trdata','fs');
              db.t0(i)=u.trdata(1).timestamp(1);
              db.t1(i)=u.trdata(1).timestamp(2);
              db.dt(i)=1000/u.fs;
            end
              
              
              
            end
            
        end
        
    end
