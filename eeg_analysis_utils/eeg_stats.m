classdef eeg_stats
    methods (Static)
        %take a list of trigger and sz init and group them
        
        function [szdata,trigdata,srtidxs,gp]=trig2sz_aux(t_trig,t1,t2,t3,thrs,mids,sortflag) %t1=szstart_2_
            t_sz=t_trig*24*60*60-t1;
             n=length(t_sz);
            T_focal=t1+t2;
            T_total=T_focal+t3;
            
            srtidxs=[];
            if isempty(mids)
                mids=ones(n,1);
            end
            if sortflag
                [~,srtidxs]=sortrows([t_trig,mids],[2,1]);
                %[t_sz,srtidxs]=sort(t_sz);
                t_trig=t_trig(srtidxs);
                mids=mids(srtidxs);
                t_sz=t_sz(srtidxs);
                T_focal=T_focal(srtidxs);
                T_total=T_total(srtidxs);
                %t_trig=t_trig(srtidxs);
                
            end
           %t_sz(1:2)
            %attribute group to sz
            gp=ones(n,1);
            dt0=diff(t_sz);
            dmids=diff(mids);
            %dt0(1)
            same_gp_idxs=dt0<thrs(2) & dt0>thrs(1) & dmids==0;
            %tmp=sum(same_gp_idxs)
            gp(2:n)=1-(same_gp_idxs);   
            gp=cumsum(gp);
            
            [~,first_trig_idx]=unique(gp);
            nsz=length(first_trig_idx);
            last_trig_idx=first_trig_idx;
            last_trig_idx(1:(nsz-1))=first_trig_idx(2:nsz)-1;
            last_trig_idx(nsz)=n;
            szdata=cat(2,t_sz(first_trig_idx),T_focal(first_trig_idx),...
                T_total(first_trig_idx),first_trig_idx,last_trig_idx);
            %the last 2 column provide a map to trigdata
            trigdata=[t_trig*24*60*60-szdata(gp,1),gp]; %relative time of trigger wrt sz start
            
        end
        
        function [trigflag,sztype,bhtype]=getSzType(szdata,trigdata,lon)
            % a sz is type 1 only if light occurs at least one time before
            nsz=size(szdata,1);
            trigflag=zeros(length(lon),1);
            sztype=zeros(nsz,4); %num sz with ligh, tfirst_light, numsz no light, tfirst_nolight
            bhtype=zeros(nsz,4);
            
            for i=1:nsz
                trigs_i=trigdata(szdata(i,4):szdata(i,5),1);
                lon_i=logical(lon(szdata(i,4):szdata(i,5)));
                trigflag(szdata(i,4):szdata(i,5))=trigs_i>szdata(i,2);
                
                sztype(i,1)=sum(trigs_i<szdata(i,2) & lon_i);
                if sztype(i,1)
                    x=trigs_i(trigs_i<szdata(i,2) & lon_i);
                sztype(i,2)=x(1);
                end
                sztype(i,3)=sum(trigs_i<szdata(i,2) & not(lon_i));
                if sztype(i,3)
                    x=trigs_i(trigs_i<szdata(i,2) & not(lon_i));
                sztype(i,4)=x(1);
                end
                
                 bhtype(i,1)=sum(trigs_i>=szdata(i,2) & lon_i);
                if bhtype(i,1)
                    x=trigs_i(trigs_i>=szdata(i,2) & lon_i);
                bhtype(i,2)=x(1);
                end
                bhtype(i,3)=sum(trigs_i>=szdata(i,2) & not(lon_i));
                if bhtype(i,3)
                    x=trigs_i(trigs_i>=szdata(i,2) & not(lon_i));
                bhtype(i,4)=x(1);
                end
                
            end
            
            
            
        end
        
        function slist=make_sz_list(sdata)
            slist(length(sdata),1)=struct('sz',table(),'trig',table());
            for i=1:length(sdata)
                cT=array2table(repmat({sdata(i).id},size(sdata(i).sz,1),1),'VariableName',{'group'});
                cT=cat(2,cT,sdata(i).raw(sdata(i).sz(:,4),1:4),...
                    cell2table(cellfun(@(x) datestr(x+sdata(i).t0,'dd/mm/yy HH:MM:SS'), ...
                 num2cell(sdata(i).sz(:,1)/(24*60*60)),'uni',0), 'VariableName',{'t_szstart'}),...
                    array2table(sdata(i).sz(:,2:3),'VariableNames',{'T_focal','T_total'}),...
                    array2table(sdata(i).focaltriggers(:,[1,3]),'VariableNames',{'Nfocal_on','Nfocal_off'}),...
                    array2table(sdata(i).bhtriggers(:,[1,3]),'VariableNames',{'Nbh_on','Nbh_off'}));
                slist(i).sz=cT;
                slist(i).trig=array2table([sdata(i).trig(:,2),sdata(i).trig(:,1),sdata(i).lon,sdata(i).trigflag],...
                    'VariableNames',{'sznum','t','lon','bh'});
                    
            end
          
                
                
               
        end
        
%         function fl=getflags(sdata)
%             fl(length(sdata),1)=struct('flags',[]);
%             for i=1:length(sdata)
%                 cflags=zeros(length(sdata.focaltriggers),4);
%                 %1=n focal only, 2=n bh only, 3=nfocal, 4=nbh
%                 cflags(sdata.bhtriggers(:,1)==0,1)=sdata(i).focaltriggers(sdata.bhtriggers(:,1)==0,1);
%                 
%                 fl(i).
% 
%         flags(:,1)=nbh_on==0 & (nfocal_on+nfocal_off)>0;
%             flags(:,2)=nfocal_on==0 & (nbh_on+nbh_off)>0;
%             flags(:,3)=(nfocal_off+nfocal_on)>0;
%             flags(:,4)=(nbh_off+nbh_on)>0;
%             flags(:,5)=(nfocal_on+nfocal_off)>0 & (nbh_on+nbh_off)>0;
          
        function sdata=trig2sz(T,thrs,mygroups)
            
            
            ids=unique(T.id);
            sortflag=1;
            if nargin<3
                mygroups=cat(2,num2cell(ids),num2cell(ids))
            end
            
            nids=size(mygroups,1);
            sdata(nids,1)=struct('sz',[],'trig',[],'lon',[],'id',[],'t0','','raw',[]);
            for i=1:nids
                Tsub=T(ismember(T.id,mygroups{i,1}),:);
                %[szdata,trigdata,srtidxs]=eeg_stats.trig2sz_aux(Tsub.timeabs-min(Tsub.timeabs),Tsub.t1,Tsub.t2,Tsub.t3,thrs,Tsub.id,sortflag);
                [szdata,trigdata,srtidxs]=eeg_stats.trig2sz_aux(Tsub.timeabs,Tsub.t1,Tsub.t2,Tsub.t3,thrs,Tsub.id,sortflag);
                sdata(i).sz=szdata;
                sdata(i).trig=trigdata;
                sdata(i).id=num2str(mygroups{i,2});
                %sdata(i).t0=min(Tsub.timeabs); %datestr(min(Tsub.timeabs),'dd/mm/yy HH:MM:SS');
                sdata(i).t0=0;
                [trigflag,sztype,bhtype]=eeg_stats.getSzType(szdata,trigdata,Tsub.lon(srtidxs));
                
                sdata(i).focaltriggers=sztype;
                sdata(i).bhtriggers=bhtype;
                sdata(i).trigflag=trigflag;
                sdata(i).raw=Tsub(srtidxs,:);
                if not(isempty(srtidxs))
                    sdata(i).lon=Tsub.lon(srtidxs);
                else
                    sdata(i).lon=T.lon; %sortflag()
                end
            end 
        end
        
%         function group_sdata(sdata,)
        
        
       function f=get_fraction_lon(nfocal_on,nbh_on,nfocal_off,nbh_off) %sz_type 1 is light on , 
            %nsz=size(nfocal_on,1);
            f=zeros(1,5);
            f(1)=sum(nfocal_on>0 & nbh_on==0)/sum(nbh_on==0 & (nfocal_on+nfocal_off)>0); %focal only : at least one trig w/ light during focal, no trigh w/ light during bh
            f(2)=sum(nbh_on>0 & nfocal_on==0)/sum(nfocal_on==0 & (nbh_on+nbh_off)>0); %bh only : at least one trig w/ light during bh, no trigh w/ light during focal
            nsz_focaltrig=sum((nfocal_off+nfocal_on)>0); 
            nsz_bhtrig=sum((nbh_off+nbh_on)>0); 
            f(3)=sum(nfocal_on>0)/nsz_focaltrig; %focal : at least one trig w/ light during focal (there can be light on during bh)
            f(4)=sum(nbh_on>0)/nsz_bhtrig; %bh : at least one trig w/ light during bh (there can be light on during focal)
            f(5)=sum(nfocal_on>0 & nbh_on>0)/sum((nfocal_on+nfocal_off)>0 & (nbh_on+nbh_off)>0); %both : at least one trig w/ light during bh and one trig w/ light during focal
            % normalize with total, so baseline not gonna be 0.5!
       end
        
       
       
        
        
        function [f,flags]=get_fraction_lon_cumul(nfocal_on,nbh_on,nfocal_off,nbh_off) %sz_type 1 is light on , 
            nsz=size(nfocal_on,1);
            f=zeros(nsz,5);
   
            flags=zeros(nsz,5);
            f(:,1)=cumsum(nfocal_on>0 & nbh_on==0)./cumsum(nbh_on==0 & (nfocal_on+nfocal_off)>0); %focal only : at least one trig w/ light during focal, no trigh w/ light during bh
            f(:,2)=cumsum(nbh_on>0 & nfocal_on==0)./cumsum(nfocal_on==0 & (nbh_on+nbh_off)>0); %bh only : at least one trig w/ light during bh, no trigh w/ light during focal
            nsz_focaltrig=cumsum((nfocal_off+nfocal_on)>0); 
            nsz_bhtrig=cumsum((nbh_off+nbh_on)>0); 
            f(:,3)=cumsum(nfocal_on>0)./nsz_focaltrig; %focal : at least one trig w/ light during focal (there can be light on during bh)
            f(:,4)=cumsum(nbh_on>0)./nsz_bhtrig; %bh : at least one trig w/ light during bh (there can be light on during focal)
            f(:,5)=cumsum(nfocal_on>0 & nbh_on>0)./cumsum((nfocal_on+nfocal_off)>0 & (nbh_on+nbh_off)>0); %both : at least one trig w/ light during bh and one trig w/ light during focal
            % normalize with total, so baseline not gonna be 0.5!
          
            flags(:,1)=nbh_on==0 & (nfocal_on+nfocal_off)>0;
            flags(:,2)=nfocal_on==0 & (nbh_on+nbh_off)>0;
            flags(:,3)=(nfocal_off+nfocal_on)>0;
            flags(:,4)=(nbh_off+nbh_on)>0;
            flags(:,5)=(nfocal_on+nfocal_off)>0 & (nbh_on+nbh_off)>0;
        end
        
        function [sflagsP,sflagsM]=getFlags(sdata)
sflagsP(length(sdata),1)=struct('focal',[],'focalOnly',[],'bh',[],'bhOnly',[]);
sflagsM(length(sdata),1)=struct('focal',[],'focalOnly',[],'bh',[],'bhOnly',[]);
for i=1:length(sdata)
    szP=sdata(i).focaltriggers(:,1)>0 %seizure with at least one plus light
    szPfocalOnly=sdata(i).focaltriggers(:,1)>0 & sdata(i).bhtriggers(:,1)==0;
    szM=sdata(i).focaltriggers(:,1)==0 & sdata(i).focaltriggers(:,3)>0% seizure with at least one plus light
    szMfocalOnly=sdata(i).focaltriggers(:,1)==0 & sdata(i).focaltriggers(:,3)>0 & ...
        sdata(i).bhtriggers(:,1)==0;
    
    bhP=sdata(i).bhtriggers(:,1)>0 %seizure with at least one plus light
    bhPbhOnly=sdata(i).bhtriggers(:,1)>0 & sdata(i).focaltriggers(:,1)==0;
    bhM=sdata(i).bhtriggers(:,1)==0 & sdata(i).bhtriggers(:,3)>0% seizure with at least one plus light
    bhMbhOnly=sdata(i).bhtriggers(:,1)==0 & sdata(i).bhtriggers(:,3)>0 & ...
        sdata(i).focaltriggers(:,1)==0;
    
    
    sflagsP(i).focal=szP;
    sflagsM(i).focal=szM;
    sflagsP(i).focalOnly=szPfocalOnly;
    sflagsM(i).focalOnly=szMfocalOnly;
    sflagsP(i).bh=bhP;
    sflagsM(i).bh=bhM;
    sflagsP(i).bhOnly=bhPbhOnly;
    sflagsM(i).bhOnly=bhMbhOnly;
end
        end
        
        function t=sz2table(sdata)
            n=length(sdata);
            t(n,1)=struct('sz',table(),'id','');
            for i=1:n
                t(i).id=sdata(i).id;
                
                 
                
                t(i).sz=cat(2,...
                    cell2table(cellfun(@(x) datestr(x+sdata(i).t0,'dd/mm/yy HH:MM:SS'), ...
                 num2cell(sdata(i).sz(:,1)/(24*60*60)),'uni',0), 'VariableName',{'t0'}),...
                    array2table(sdata(i).sz(:,2:3),'VariableNames',{'T_focal','T_total'}),...
                    array2table(sdata(i).focaltriggers(:,[1,3]),'VariableNames',{'Nfocal_on','Nfocal_off'}),...
                    array2table(sdata(i).bhtriggers(:,[1,3]),'VariableNames',{'Nbh_on','Nbh_off'}));
          
                fprintf(1,'Group:%s\n',t(i).id);
                disp(t(i).sz);
                
            end
        end
            
        function T=fraction_lon(sdata)
            i=1;
            %ntrig=size(sdata(i).focaltriggers,1);
            f=eeg_stats.get_fraction_lon(sdata(i).focaltriggers(:,1),sdata(i).bhtriggers(:,1),...
               sdata(i).focaltriggers(:,3),sdata(i).bhtriggers(:,3));
            T=cat(2,cell2table({sdata(i).id},'VariableNames',{'group'}),array2table(f,...
                'VariableNames',{'focalOnly','bhOnly','focal','bh','both'}));
            for i=2:length(sdata)
                f=eeg_stats.get_fraction_lon(sdata(i).focaltriggers(:,1),sdata(i).bhtriggers(:,1),...
                    sdata(i).focaltriggers(:,3),sdata(i).bhtriggers(:,3));
               T=cat(1,T,cat(2,cell2table({sdata(i).id},'VariableNames',{'group'}),array2table(f,...
                'VariableNames',{'focalOnly','bhOnly','focal','bh','both'})));
            end
           disp(T);
        end
        
        function T_cumul=fraction_lon_cumul(sdata)
           
            T_cumul(length(sdata),1)=struct('group',[],'T',table(),'Tflag',table());
            %ntrig=size(sdata(i).focaltriggers,1);
            
            for i=1:length(sdata)
              [f,flags]=eeg_stats.get_fraction_lon_cumul(sdata(i).focaltriggers(:,1),sdata(i).bhtriggers(:,1),...
               sdata(i).focaltriggers(:,3),sdata(i).bhtriggers(:,3));
            T_cumul(i).group=sdata(i).id;
            T_cumul(i).T=array2table(f,...
                'VariableNames',{'focalOnly','bhOnly','focal','bh','both'});
            T_cumul(i).Tflag=array2table(flags,...
                'VariableNames',{'focalOnly','bhOnly','focal','bh','both'});
            
            end
           %disp(T);
        end
        
        
        function f=fraction_lon_montecarlo_aux(szdata,trigdata,n)
            
            
            ntrig=size(trigdata,1);
           
            f=zeros(n,5); %[focal, bh, focalloose, both]
            for i=1:n
                lon = zeros(ntrig,1);
                x = rand(ntrig,1);
                lon(x<0.5) = 1;
                [~,sztype,bhtype]=eeg_stats.getSzType(szdata,trigdata,lon);
               f(i,:)=eeg_stats.get_fraction_lon(sztype(:,1),bhtype(:,1),sztype(:,3),bhtype(:,3));
            end
           
        end
        
        
        function [f,flags]=fraction_lon_montecarlo_cumul_aux(szdata,trigdata,n)
            
            
            ntrig=size(trigdata,1);
            nsz=size(szdata,1);
            f=zeros(nsz,5,n); %[focal, bh, focalloose, both]
            flags=zeros(nsz,5,n);
            for i=1:n
                lon = zeros(ntrig,1); 
                x = rand(ntrig,1);
                lon(x<0.5) = 1;
                [~,sztype,bhtype]=eeg_stats.getSzType(szdata,trigdata,lon);
               [cf,cflags]=eeg_stats.get_fraction_lon_cumul(sztype(:,1),bhtype(:,1),sztype(:,3),bhtype(:,3));
               f(:,:,i)=cf;
               flags(:,:,i)=cflags;
            end
           
        end
        
        
        function T=fraction_lon_montecarlo(sdata,n,th)
            if nargin<3
                th=5;
            end
            nid=length(sdata);
            i=1;
            fracon=eeg_stats.fraction_lon_montecarlo_aux(sdata(i).sz,sdata(i).trig,n);
            m=mean(fracon);
            cil=prctile(fracon,th);
            cih=prctile(fracon,100-th);
            T=cat(2,cell2table({sdata(i).id},'VariableNames',{'group'}),...
                array2table([m(1),cil(1),cih(1),m(2),cil(2),cih(2),...
                m(3),cil(3),cih(3),m(4),cil(4),cih(4),m(5),cil(5),cih(5)],...
                'VariableNames',{'f0_focalOnly','cil_focalOnly','cih_focalOnly',...
                'f0_bhOnly','cil_bhOnly','cih_bhOnly',...
                'f0_focal','cil_focal','cih_focal',...
                'f0_bh','cil_bh','cih_bh',...
                'f0_both','cil_both','cih_both'}));
            for i=2:nid
                  fracon=eeg_stats.fraction_lon_montecarlo_aux(sdata(i).sz,sdata(i).trig,n);
            m=mean(fracon);
            cil=prctile(fracon,th);
            cih=prctile(fracon,100-th);
            T=cat(1,T,cat(2,cell2table({sdata(i).id},'VariableNames',{'group'}),...
                array2table([m(1),cil(1),cih(1),m(2),cil(2),cih(2),...
                m(3),cil(3),cih(3),m(4),cil(4),cih(4),m(5),cil(5),cih(5)],...
                'VariableNames',{'f0_focalOnly','cil_focalOnly','cih_focalOnly',...
                'f0_bhOnly','cil_bhOnly','cih_bhOnly',...
                'f0_focal','cil_focal','cih_focal',...
                'f0_bh','cil_bh','cih_bh',...
                'f0_both','cil_both','cih_both'})));
            end
                
        end
        
         function T_cumul=fraction_lon_montecarlo_cumul(sdata,n,th)
            if nargin<3
                th=5;
            end
            nid=length(sdata);
           
            T_cumul(nid,1)=struct('group',[],'T',table());
          
            for i=1:nid
                 [fracon,~]=eeg_stats.fraction_lon_montecarlo_cumul_aux(sdata(i).sz,sdata(i).trig,n);
            m=mean(fracon,3);
            cil=prctile(fracon,th,3);
            cih=prctile(fracon,100-th,3);
            T_cumul(i).group=sdata(i).id;
            T_cumul(i).T=...
                array2table([m(:,1),cil(:,1),cih(:,1),m(:,2),cil(:,2),cih(:,2),...
                m(:,3),cil(:,3),cih(:,3),m(:,4),cil(:,4),cih(:,4),m(:,5),cil(:,5),cih(:,5)],...
                'VariableNames',{'f0_focalOnly','cil_focalOnly','cih_focalOnly',...
                'f0_bhOnly','cil_bhOnly','cih_bhOnly',...
                'f0_focal','cil_focal','cih_focal',...
                'f0_bh','cil_bh','cih_bh',...
                'f0_both','cil_both','cih_both'});
            %T_cumul(i).
            end
                
        end
        
        %% how do I show that the sz dynamics is different
      %  
    end
end

