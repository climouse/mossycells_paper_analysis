classdef szdb < handle
    properties
        db=eegdb();
        data=table();
        %t0, tfocal, ttotal, channel;
        map=[]; % coordinates onto eeg db
        name='';
        triggersdb=[];
        triggers=[]; %long vector of triggers n*4, is row is a trigger, [sznumer, t0, flag, light, trignumberInTrigDB]
        triggersmap=[]; %nsz*2 [first idx, last idx]
        
    end
    
    methods
        
        function obj=szdb(fname,mode)
            
            if nargin>0
                if nargin<2
                    mode='ready';
                end
                switch mode
                    case 'ready'
                        obj.data=readtable(fname);
                    case 'raw'
                        obj.data=szdata.import_raw(fname)
                end
            end
        end
        
        
        function n=length(obj)
            n=size(obj.data,1);
        end
        
        function link_eegdb(obj,db)
            n=obj.length();
            obj.map=zeros(n,2); %id, time2beg in minutes
            obj.db=db;
            for i=1:n
                x=obj.db.query(obj.data.t0(i));
                obj.map(i,:)=x;
            end
        end
        
        function link_triggesrdb(obj,triggersdb,recalibrate)
            % recalibrate flag allows to find the more exact sz time by
            % using the available triggers which are recocorded w/ ms
            % precision
            obj.triggersdb=triggersdb;
            %find all the triggers that fit within
            nsz=obj.length();
            obj.triggersmap=zeros(nsz,2);
            triggers=[];
            cn=1;
            if recalibrate
                display('Recalibrating')
                for i=1:nsz
                    cchan=obj.data.ch(i);
                    if obj.db.simplefiles
                        cchan=1;
                    end
                    
                    % find the closest trigger to recalibrate
                    vals=obj.triggersdb.query_interval(obj.data.t_trig(i)-recalibrate/(60*60*24),2*recalibrate,cchan);
                    
                    %else
                    %vals=obj.triggersdb.query_interval(obj.data.t0(i)-recalibrate/(60*60*24),obj.data.T_total(i)+2*recalibrate,cchan);
                    
                    n=size(vals,1);
                    if n>0 %find a close trigger,  a readjust time
                        t1day=obj.data.t_trig(i)-obj.data.t0(i);
                        
                        [~,ix]=min(abs(obj.data.t_trig(i)-vals(:,5)));
                        t_trig_new=vals(ix(1),5);
                        t0_new=t_trig_new-t1day; %new trigger time
                        obj.data.t_trig(i)=t_trig_new;
                        t0_old=obj.data.t0(i);
                        obj.data.t0(i)=t0_new;
                        
                        fprintf(1,'Adjusting start of sz %g from %s to %s\n',i,datestr(t0_old,'mm/dd/yy_HH:MM:SS:FFF'),...
                            datestr(t0_new,'mm/dd/yy_HH:MM:SS:FFF'));
                        %now find the triggers in the interval
                        %db_id=vals(ix(1),2);
                        vals=obj.triggersdb.query_interval(obj.data.t0(i),obj.data.T_total(i),cchan); %,db_id);
                        n=size(vals,1);
                        obj.triggersmap(i,:)=[cn,n];
                        if n>0
                            
                            cn=cn+n;
                            bhflag=vals(:,5)>=(obj.data.t0(i)+obj.data.T_focal(i)/(60*60*24));
                            %vals=%[trigidx,fileid,t[mn],led,t0]
                            triggers=cat(1,triggers,cat(2,i*ones(n,1),vals(:,1),vals(:,2),vals(:,5),vals(:,4),bhflag)) %[szid, fileid, t0, led, bhflag]
                        else
                            display('Oops, something wrong');
                        end
                    else % case there is no close trigger
                        obj.triggersmap(i,:)=[cn,n];
                        fprintf(1,'No trigger close to trigger of sz %g\n',i);
                    end
                    
                    
                end
            else % no recalibrate
                for i=1:nsz
                    cchan=obj.data.ch(i);
                    if obj.db.simplefiles
                        cchan=1;
                    end
                    vals=obj.triggersdb.query_interval(obj.data.t0(i),obj.data.T_total(i),cchan);
                    n=size(vals,1);
                    obj.triggersmap(i,:)=[cn,n];
                    if n>0
                        
                        cn=cn+n;
                        bhflag=vals(:,5)>=(obj.data.t0(i)+obj.data.T_focal(i)/(60*60*24));
                        %vals=%[trigidx,fileid,t[mn],led,t0]
                        triggers=cat(1,triggers,cat(2,i*ones(n,1),vals(:,1),vals(:,2),vals(:,5),vals(:,4),bhflag)); %[szid, fileid, t0, led, bhflag]
                        
                    end
                end
            end
            obj.triggers=array2table(triggers,'VariableNames',{'szid','trigid','fileid','t0','led','bhflag'});
        end
        
        function T=all_triggers(obj,chan) %
            if obj.db.simplefiles
                chan=1;
            end
            T=obj.triggersdb.toTable(chan);
            n=size(T,1);
            T=cat(2,T,array2table(zeros(n,2),'VariableNames',{'szid','bhflag'}));
            if not(isempty(obj.triggers))
                T.szid(obj.triggers.trigid)=obj.triggers.szid;
                T.bhflag(obj.triggers.trigid)=obj.triggers.bhflag;
            end
            
        end
        
        
        
        
        
    end
    
    methods(Static)
        function T=import_raw(filename,datefrmt)
            Traw=readtable(filename);
            t_trigs= cellfun(@(x,y) datenum([x '_' y],datefrmt),Traw.date,Traw.t0,'uni',1);
            T=table(Traw.id,t_trigs-Traw.t1/(60*60*24),Traw.t2+Traw.t1,Traw.t2+Traw.t1,Traw.ch,t_trigs,...
                'VariableNames',{'id','t0','T_focal','T_total','ch','t_trig'});
        end
    end
end