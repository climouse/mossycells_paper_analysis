classdef spikes < handle
    methods (Static)
        
        %___ LOW LEVEL FUNCTIONS TO EXTRACT SPIKES FROM EEG SIGNAL ___
        
        function t=find_local_max(y,ymin,dtmin)
            % find the location (index) of the local maxima in the vector y. Local maxima below ymin are discarded. dtmin enforces a minimum distance between two local maxima. If two local maxima are found closer than dtmin, the highest of the two is kept the other one is discared. This operation is repeated until all the local maxima in t are at least dtmin apart.
            if size(y,2)==1
                y=y';
            end %row vector
            t=find(y(2:end-1)>=y(1:end-2) & y(2:end-1)>=y(3:end) & y>=ymin)+1;
            
            %delete local maxima closer than dtmin
            keepon=1;
            while keepon
                closepeaks=find(diff(t)<tmin);
                keepon=not(isempty(closepeaks));
                if keepon
                    [~,ix2del]=min([y(closepeaks);y(closepeaks+1)],2); %wich of the 2 local maxima 2 remove
                    t2del=closepeaks+ix2del-1;
                    t(t2del)=[];
                end
            end
        end
        
        function y_snle = snle(y, windowSize,snlePeriod)
            % Comput the smoothed non-linear energy of the signal y
            % See
            % Mukhopadyay and Ray, "A New Interpretation of Nonlinear
            % Energy Operator and Its Efficacy is Spike Detection",
            % IEEE Trans Biomed Eng, 1998
            % This function is adapted from
            % https://github.com/LeventhalLab/EphysToolbox/blob/master/MatlabSpikeworks/snle.m
            
            numSamps = length(y);
            phaseShift = ceil(windowSize / 2);
            w = hann(windowSize);
            y_snle = y;
            L = length(y)-(2*snlePeriod);
            y_snle(1+snlePeriod:end-snlePeriod) = y(1+snlePeriod:end-snlePeriod).^2 - y(1:L) .* y((end-L+1):end);
            temp = conv(y_snle, w);
            y_snle = temp(phaseShift : phaseShift + numSamps - 1);
        end
        
        function data_localMax = find_nearest_peak(data,approx_localMax, halfWindow)
            % if we have the approximate location (approx_localMax) of the local maxima in the data (data), this function find the point of highest . This is useful when we find the peaks in the filtered signal (say the data smoothened by the snle function), but want the local maxima in the non filtered data instead.
            if nargin<3
                halfWindow = 20;
            end
            data_localMax = approx_localMax;
            n=length(data);
            for ii=1:length(approx_localMax)
                snippet_ix=[max(1,approx_localMax(ii)-halfWindow),min(approx_localMax(ii)+halfWindow,n)];
                snippet=data(snippet_ix(1):snippet_ix(2));
                snippetLoc=spikes.find_local_max(abs(snippet),2*halfWindow);
                if not(isempty(snippetLoc)) %otherwise use the approximate location
                    data_localMax(ii) = snippetLoc + snippet_ix(1)- 1;
                end
            end
        end
        
        function [s,y_snle,params] = get_spikes_locations(data,p)
            % extracts the spikes from the ephys data (data)
            % s: the location of the spikes (index)
            % y_snle: the data smoothed by snle
            % p: structure with parameters for snle and spike selection (should be obvious what they are)
            % This function is adapted from
            % https://github.com/LeventhalLab/EphysToolbox/blob/master/SpikeySpike/getSpikeLocations.m
            if nargin<2
                p=struct('dir',1,'dtmin',10,'thresh',15,'snle_windowSize',10,'snle_period',3)
            end
            
            
            disp('Calculating SNLE ...')
            
            y_snle = spikes.snle(data,p.windowSize,p.snlePeriod);
            
            params.mad=mad(y_snle,1); %this estimates the noise in the data using the median absolute deviation (MAD) which should not be too sensitive to the presence of sparse action potentials
            params.ymin=p.thresh*params.mad; %how many MAD above the median do we want to accept peaks
            y_snle = y_snle-median(y_snle); %zero median
            
            disp(['Extracting peaks from the data after smoothing using the SNLE...']);
            y_snle_peaks = spikes.find_local_max(y_snle,params.ymin,p.dtmin)  ;
            fprintf(1,'Found %g peaks \n',length(y_snle_peaks));
            disp(['Adjusting position on pre-smoothened data...']);
            s = spikes.find_nearest_peak(data,y_snle_peaks);
            switch p.dir
                case 1 %only keep positive spikes
                    
                    spikes2keep=data(s)>0; %positive spikes
                    nn=sum(spikes2keep);
                    fprintf(1,'Found %g spikes that are positive (%g percent)\n',nn,round(nn/length(s)*100));
                    s=s(spikes2keep);
                case -1 %only keep negative spikes
                    spikes2keep=data(s)<0;
                    nn=sum(spikes2keep);
                    fprintf(1,'Found %g spikes that are negative (%g percent)\n',nn,round(nn/length(s)*100));
                    s=s(spikes2keep);
                case 0 %keep all spikes
                    nn=length(s);
                    fprintf(1,'Found %g spikes \n',nn);
            end
            
            
            s = unique(sort(s)); %just to be safe
            if p.dtmin>0 %elimates close by spikes. there can be close by spikes after the find_nearest_peak operation, even though all the spikes were far apart in the y_snle_peaks vector.
                y = zeros(1,size(s,1));
                y(s) = 1;
                s = spikes.find_local_max(y,p.dtmin,0);
            end
        end
        
        
        function [I,t]=intensity(t_spikes,dt,tmM)
            %compute spiking rate from spike arrival times t_spikes, using an averaging window of size dt, and only keeping spiles within tmM interval
            if nargin<3
                tmM=[t_spikes(1),t_spikes(end)];
                
            end
            edges = tmM(1):dt:tmM(2);
            out = histc(t_spikes, edges);
            I=out(1:end-1)/dt;
            t=dt*ones(length(I),1);
            t=tmM(1)+cumsum(t)-dt;
        end
        
        %___ LOW LEVEL FUNCTIONS TO FIND LASER ON/OFF PHASES, INDIVIDUAL LASER PULSES DURING LASER ON PHASES, AND REGISTER SPIKES WITH RESPECT TO THESE PHASES AND PULSES ___
        
        function [t_p,t_m]=get_transitions(x,thresh)
            % finds transition times where the phase switches from laser on (ie. pulsing) to laser off t_m, and vice e versa t_p
            if nargin<2
                thresh=2.5; %up dowm
            end
            ons=int8(x>thresh(1));
            d=diff(ons);
            t_p=find((d==1));
            t_m=find(d==-1);
            if not(isempty(t_p))
                t_p=t_p+1; %gives the first moment when it is on
            end
        end
        
        function p=get_phase(x,phases)
            p=zeros(length(x),2);
            phase0=1;
            y=1;
            for i=1:length(x)
                
                while x(i)>phases(phase0,2) & phase0<size(phases,1)
                    
                    phase0=phase0+1;
                    y=1;
                end
                if x(i)>=phases(phase0,1) & x(i)<=phases(phase0,2)
                    p(i,1)=phase0;
                    p(i,2)=y;
                    y=y+1;
                    
                end
                
            end
        end
        
        function [y,yp,t_mock]=register_signals(x,tp,phases,T) % x are photons, tp are pulses
            y=zeros(size(x,1),4); %phase num, pulse num, delay, light
            yp=spikes.get_phase(tp,phases);
            n=size(phases,1);
            t_mock=[];
            for i0=1:n
                cixs=x>=phases(i0,1) & x<phases(i0,2);
                nn=sum(cixs);
                if nn
                    y(cixs,1)=i0;
                    y(cixs,4)=phases(i0,3);
                    
                    
                    if phases(i0,3)==1
                        
                        [yp_ixs_left,~,~,~]=spikes.get_nearest_left(tp,x(cixs));
                        
                        iok=find(yp_ixs_left);
                        spikenum=zeros(nn,1);
                        spikenum(iok)=yp(yp_ixs_left(iok),2);
                        delay=nan(nn,1);
                        cx=x(cixs);
                        delay(iok)=cx(iok)-tp(yp_ixs_left(iok));
                        y(cixs,2)=spikenum;
                        y(cixs,3)=delay; % normally all should have a left because we are inside a phase 1
                        
                    else
                        y(cixs,3)=mod(x(cixs)-phases(i0,1),T);
                        y(cixs,2)=floor((x(cixs)-phases(i0,1))/T)+1;
                        t_mock_i0=(phases(i0,1):T:phases(i0,2))';
                        t_mock=cat(1,t_mock,[t_mock_i0,i0*ones(length(t_mock_i0),1),(1:length(t_mock_i0))']);
                    end
                end
                
                
            end
        end
        
        
        
        
        function [y,tref,ix_query,delay]=get_nearest_left(ref,query)
            y=zeros(size(query));
            %ix_ref=zeros(size(query));
            ii=1;
            i=1;
            while i<=length(query) & ii<=length(ref)
                if query(i)>ref(ii)
                    ii=ii+1;
                else
                    y(i)=ii-1;
                    i=i+1;
                end
            end
            yok=y>0;
            tref=ref(y(yok));
            
            ix_query=find(yok);
            delay=query(yok)-tref;
            
        end
        
        
        
        function phases=get_light_phases(x,thresh,dur_thresh,T) %x is column vector
            if nargin<4
                T=0;
            end
            if nargin<2
                thresh=2.5;
            end
            if nargin<3
                dur_thresh=20000;
            end
            %% get moments of light on
            
            light_on=int8(x>thresh);
            transitions=not(diff(light_on)==0);
            nn=length(x);
            ix_transitions=[1;find(transitions);length(x)];
            deltas=diff(ix_transitions);
            transitions_long=ix_transitions(deltas>dur_thresh);
            light_off_intervals=cat(2,transitions_long,transitions_long+deltas(deltas>dur_thresh));
            %light_on_state=ones(length(x),1);
            light_on_intervals=zeros(size(light_off_intervals,1)-1,2);
            for i=1:size(light_off_intervals,1)-1
                light_on_intervals(i,:)=[light_off_intervals(i,2),light_off_intervals(i+1,1)];
            end
            
            
            if not(light_off_intervals(1,1)==1)
                light_on_intervals=cat(1,light_on_intervals,[1,light_off_intervals(1,1)]);
                
            end
            if not(light_off_intervals(end,2)==length(x))
                light_on_intervals=cat(1,light_on_intervals,[light_off_intervals(end,2),length(x)]);
            end
            if T>0
                light_on_intervals(:,2)=min(light_on_intervals(:,2)+T,nn);
                light_off_intervals(:,1)=max(light_off_intervals(:,1)+T,1);
            end
            
            phases=cat(1,cat(2,light_on_intervals,ones(size(light_on_intervals,1),1)),cat(2,light_off_intervals,zeros(size(light_off_intervals,1),1)));
            phases=sortrows(phases,1);
            phases(1,1)=1;
            
        end
        
        function [phases,ix_transitions]=get_light_phases2(x,thresh) %x is column vector
            
            %% get moments of light on
            
            light_on=int8(x>thresh);
            transitions=not(diff(light_on)==0);
            nn=length(x);
            ix_transitions=unique([1;find(transitions);length(x)]);
            phases=zeros(length(ix_transitions)-1,3);
            for i=1:length(ix_transitions)-1
                phases(i,1)=ix_transitions(i);
                phases(i,2)=ix_transitions(i+1);
                phases(i,3)=light_on(ix_transitions(i+1));
            end
        end
        
        
        function  N=count_spikes(i,s)
            % i is a list of intervals
            n=size(i,1);
            N=zeros(n,2);
            for i0=1:n
                N(i0,1)=sum(s>=i(i0,1) & s<i(i0,2));
                N(i0,2)=i(i0,2)-i(i0,1);
            end
        end
        
        function g=get_spike_group(i,s)
            n=size(i,1);
            g=zeros(length(s),2);
            for i0=1:n
                cixs=s>=i(i0,1) & s<i(i0,2);
                g(cixs,1)=i0;
                g(cixs,2)=i(i0,3);
                
                
            end
        end
        
        function sp=get_spikes_properties(s,signal,t_p,phases,fs)
            g=spikes.get_spike_group(phases,s);
            %ampl, delay,t2p,t2n
            vnames={'t','phase','light','ampl','delay','t2p','t2n'};
            sp=array2table([s,g,nan(size(s,1),4)],'VariableNames',vnames);
            sp.ampl=signal(sp.t,1);
            
            [~,~,ix_query,delays]=spikes.get_nearest_left(t_p,sp.t);
            sp.delay(ix_query)=delays/fs; %in ms
            deltas=diff(sp.t)/fs;
            sp.t2p(2:end)=deltas;
            sp.t2n(1:end-1)=deltas;
        end
        
        %spike train, pulse train,
        function dr=getRateChange(epochs,s,base_status,sym)
            %epochs is [tstart, tstop, ligh status]
            ne=size(epochs,1);
            N=spikes.count_spikes(epochs,s);
            
            side_rates_l=zeros(ne,2);
            side_rates_r=zeros(ne,2);
            r=zeros(ne,3);
            if ne>1
                side_rates_l(2:end,:)=N(1:end-1,:);
                side_rates_r(1:end-1,:)=N(2:end,:);
            end
            r0=N(:,1)./N(:,2);
            r(:,1)=r0./(side_rates_l(:,1)./side_rates_l(:,2));
            r(:,2)=r0./(side_rates_r(:,1)./side_rates_r(:,2));
            r(:,3)=r0./((side_rates_r(:,1)+side_rates_l(:,1))./(side_rates_r(:,2)+side_rates_l(:,2)));
            if nargin<3
                dr=r;
            else
                switch sym
                    case -1
                        dr=r(epochs(:,3)==base_status & not(isnan(r(:,1))),1);
                    case 1
                        dr=r(epochs(:,3)==base_status & not(isnan(r(:,2))),2);
                    case 0
                        dr=r(epochs(:,3)==base_status & not(isnan(r(:,2)) | isnan(r(:,1))),3);
                end
            end
            
            
        end
        
        
        function [epochs_list,ixs_list]=split_epochs(epochs,t_epochs,fs)
            if nargin<3
                fs=1;
            end
            epochs_list=repmat({[]},size(t_epochs,1),1);
            ixs_list=repmat({[]},size(t_epochs,1),1);
            for i=1:size(t_epochs,1)
                ixs=find(epochs(:,1)>=floor(t_epochs(i,1)*fs) & epochs(:,2)<=ceil(t_epochs(i,2)*fs));
                
                epochs_list{i}=epochs(ixs,:);
                ixs_list{i}=ixs;
            end
        end
        
        function train_vec=train2vec(train,tmM,downsmpl)
            if nargin<3
                downsmpl=1;
            end
            
            train_resample=int32(round(train-tmM(1))/downsmpl)+1;
            nel=int32(round((tmM(2)-tmM(1))/downsmpl))+1;
            train_vec=zeros(nel,1);
            
            train_vec(train_resample(train_resample<=nel & train_resample>0))=1;
            
        end
        
        %___ LOW LEVEL FUNCTIONS FOR SPIKE SORTING  ___
        
        function profs=get_spikes_profiles(signal,locs,w)
            n=length(locs);
            nn=length(signal);
            profs=NaN(n,2*w+1);
            for i=1:n
                t0=max(1,locs(i)-w);
                t1=min(locs(i)+w,nn);
                offL=-locs(i)+t0+w+1; %if negative, then offset to the right
                
                profs(i,offL:(t1-t0+offL))=signal(t0:t1);
            end
        end
        
        function [gps,d,C]=classify_spikes(profiles,mask,k,mad_reject,iter,refs)
            if nargin<6
                refs=[]; %forces a relabelling of groups
            end
            if nargin<5
                iter=0;
            end
            if nargin<4
                mad_reject=0;
            end
            if nargin<3
                k=2;
            end
            if nargin<2
                mask=ones(1,size(profiles,2));
            end
            gps=kmeans(profiles(:,mask),k,'Distance','cityblock');
            d=zeros(size(profiles,1),1);
            C=zeros(k,size(profiles,2));
            for h=1:k
                %% find outliers
                gps_h=find(gps==h);
                
                profiles_h=profiles(gps_h,:);
                C_h=median(profiles_h,1);
                C(h,:)=C_h;
                d2clust=sum(abs(bsxfun(@minus,profiles_h,C_h)),2);
                
                m=median(d2clust);
                ma=mad(d2clust);
                
                d(gps_h)=(d2clust-m)/ma;
                if mad_reject>0
                    ix2reject=(d2clust-m)>mad_reject*ma;
                    gps(gps_h(ix2reject))=-h;
                    if iter>0
                        C(h,:)=median(profiles_h(not(ix2reject),:),1);
                        d2clust=sum(abs(bsxfun(@minus,profiles_h,C(h,:))),2);
                        m=median(d2clust);
                        ma=mad(d2clust);
                        d(gps_h)=(d2clust-m)/ma;
                    end
                end
            end
            if not(isempty(refs))
                [g_relabel,~]=spikes.profile_distance(C,refs,mask);
                gps2=gps;
                for ii=1:size(C,1)
                    gps2(gps==i)=g_relabel(ii);
                    gps2(gps==-i)=-g_relabel(ii);
                end
            end
        end
        
        function [g,d]=profile_distance(profs,refs,mask)
            nref=size(refs,1);
            n=size(profs,1);
            d=zero(n,nref);
            g=zeros(n,1);
            for j=1:nref
                d(:,j)=sum(abs(bsxfun(@minus,profs(:,mask),refs(j,mask))),2);
                
                [~,g(:,j)]=min(d(:,j),2);
            end
            
        end
        
        function f=profiles2fcs(profs,t_profile)
            f=fcs();
            f.g2=profs;
            %f.I=ones(size(profs,1),2);
            f.tau=t_profile;
            f.n=size(profs,1);
            
        end
        
        
        
        %___ PLOTTING FUNCTIONS ___
        
        function z=plot_chronogram_scatter_horiz(x,x_props,out,coarsing_factor,phases_ix,zeroing,xl) %out should contain laser and mock and phase
            if nargin<5 | isempty(phases_ix)
                phases_ix=2:size(out.phases,1)-1;
            end
            if nargin<6
                zeroing=0;
            end
            t0=0;
            if zeroing
                t0=out.phases(phases_ix(1),1);
            end
            if nargin<7
                xl=[50,2];
            end
            
            
            aa1=max(x_props.phase);
            bb1=max(ceil(x_props.id/coarsing_factor));
            aa2=max(cat(1,out.laser.phase,out.mock.phase));
            bb2=max(ceil(cat(1,out.laser.id,out.mock.id)/coarsing_factor));
            y=accumarray([x_props.phase,ceil(x_props.id/coarsing_factor)],x,[max(aa1,aa2),max(bb1,bb2)],@(x) {x});
            %[aa,bb]=size(y)
            t_ref=accumarray([cat(1,out.laser.phase,out.mock.phase),ceil(cat(1,out.laser.id,out.mock.id)/coarsing_factor)],cat(1,out.laser.t,out.mock.t),[max(aa1,aa2),max(bb1,bb2)],@(x) nanmean(x));
            yN=cellfun(@(x) numel(x),y);
            
            t=cellfun(@(x,y) x*ones(y,1),num2cell(t_ref),num2cell(yN),'uni',0); %for vecotrization
            
            phases2keep=zeros(size(out.phases,1),1);
            phases2keep(phases_ix)=1;
            
            i0=find(out.phases(:,3)==0 & phases2keep);
            i1=find(out.phases(:,3)==1 & phases2keep);
            %nn0=find(i0)
            %nn1=find(i1)
            yvec_1=vertcat(y{i1,:});
            tvec_1=vertcat(t{i1,:})-t0;
            yvec_0=vertcat(y{i0,:});
            tvec_0=vertcat(t{i0,:})-t0;
            %[n1,n2]=size(y);
            z={[yvec_0,tvec_0/out.Fs],[yvec_1,tvec_1/out.Fs]};
            figure;
            scatter(tvec_0/out.Fs,yvec_0,'Marker','.','MarkerEdgeColor','k');
            hold all
            scatter(tvec_1/out.Fs,yvec_1,'Marker','.','MarkerEdgeColor',[0,173,238]/255);
            ylim([0,xl(1)+xl(2)]);
            for i=1:length(phases_ix)
                if out.phases(phases_ix(i),3)
                    crect=rectangle('Position',[(out.phases(phases_ix(i),1)-t0)/out.Fs,xl(1),...
                        (out.phases(phases_ix(i),2)-out.phases(phases_ix(i),1))/out.Fs,xl(2)],'FaceColor',[0,173,238]/255,...
                        'EdgeColor','none');
                end
                
            end
            for i=1:length(phases_ix)
                plot((out.phases(phases_ix(i),1)-t0)*ones(1,2)/out.Fs,get(gca,'YLim'),'Color','k','LineStyle','-.');
            end
            % for i=1:size(out.phases,1)
            %     if out.phases(i,3)
            %  crect=rectangle('Position',[-2,out.phases(i,1)/out.Fs,2,(out.phases(i,2)-out.phases(i,1))/out.Fs],'FaceColor',[0    0.4470    0.7410],...
            %         'EdgeColor','none');
            %     end
            %
            % end
            % for i=1:size(out.phases,1)
            %     plot(get(gca,'XLim'),out.phases(i,1)*ones(1,2)/out.Fs,'Color','k','LineStyle','-.');
            % end
            %plot([edges(1),edges(end)],out.phases(i,1)*ones(1,2),'Color','k','LineStyle','-.');
            
            %xlabel('Delay [ms]');
            
            %set(gca,'XAxisLocation','origin');
            % cpos=get(gca,'Position');
            set(gca,'XTick',round(([out.phases(phases_ix(:),1);out.phases(phases_ix(end),2)]-t0)/out.Fs));
            xlabel('Time [s]');
            xlim(round([(out.phases(phases_ix(1),1)-t0)/out.Fs,(out.phases(phases_ix(end),2)-t0)/out.Fs]));
            
        end
        
        
        
        
        
        function plot_chronogram_rate_horiz(x,x_props,out,coarsing_factor,phases_ix,zeroing,yl)
            if nargin<5 | isempty(phases_ix)
                phases_ix=2:size(out.phases,1)-1;
            end
            if nargin<6
                zeroing=0;
            end
            
            t0=0;
            if zeroing
                t0=out.phases(phases_ix(1),1);
            end
            
            yN=accumarray([x_props.phase,ceil(x_props.id/coarsing_factor)],x,[],@(x) numel(x),0);
            t_ref=accumarray([cat(1,out.laser.phase,out.mock.phase),ceil(cat(1,out.laser.id,out.mock.id)/coarsing_factor)],cat(1,out.laser.t,out.mock.t),[],@(x) nanmean(x));
            
            t_ref2=((out.phases(phases_ix,2)+out.phases(phases_ix,1))/2-t0)/out.Fs;
            
            perioddur=50e-3*coarsing_factor;
            
            
            yN2=mean(yN(phases_ix,:),2)/perioddur;
            %yN(phases_ix,:)
            yN2std=std(yN(phases_ix,:),1,2)/sqrt(size(yN,2))/perioddur;
            
            
            phases2keep=zeros(size(out.phases,1),1);
            phases2keep(phases_ix)=1;
            
            i0=find(out.phases(:,3)==0 & phases2keep);
            i1=find(out.phases(:,3)==1 & phases2keep);
            
            
            t_ref20=((out.phases(i0,2)+out.phases(i0,1))/2-t0)/out.Fs;
            yN20=mean(yN(i0,:),2)./perioddur;
            yN20std=std(yN(i0,:),1,2)/sqrt(size(yN,2));
            t_ref21=((out.phases(i1,2)+out.phases(i1,1))/2-t0)/out.Fs;
            yN21=mean(yN(i1,:),2)./perioddur;
            yN21std=std(yN(i1,:),1,2)/sqrt(size(yN,2));
            
            
            yvec0=reshape(yN(i0,:)',numel(yN(i0,:)),1);
            tvec0=reshape(t_ref(i0,:)',numel(t_ref(i0,:)),1)-t0;
            yvec1=reshape(yN(i1,:)',numel(yN(i1,:)),1);
            tvec1=reshape(t_ref(i1,:)',numel(t_ref(i1,:)),1)-t0;
            
            
            figure;
            plot(t_ref2,yN2,'Color','k','LineWidth',2);
            hold all
            scatter(t_ref20,yN20,'Marker','s','MarkerEdgeColor','k','MarkerFaceColor','k');
            scatter(t_ref21,yN21,'Marker','s','MarkerEdgeColor',[0,173,238]/255,'MarkerFaceColor',[0,173,238]/255)
            hold all
            for ii=1:length(yN2)
                errorbar(t_ref2(ii),yN2(ii),yN2std(ii),'Color','k','LineWidth',1);
                hold all
            end
            % plot([t_ref2(ii),t_ref2(ii)],[yN2(ii)-yN2std(ii), yN2(ii)+yN2std(ii)],'Color','k','LineWidth',1);
            if nargin<7 | isempty(yl)
                cylim=get(gca,'YLim');
                yl=[cylim(2),0.05/0.7*diff(cylim)];
                %            else
                %                cylim=yl;
            end
            %ylim([xl(1),xl(2)]);
            for i=1:length(phases_ix)
                if out.phases(phases_ix(i),3)
                    crect=rectangle('Position',[(out.phases(phases_ix(i),1)-t0)/out.Fs,yl(1),...
                        (out.phases(phases_ix(i),2)-out.phases(phases_ix(i),1))/out.Fs,yl(2)],'FaceColor',[0,173,238]/255,...
                        'EdgeColor','none');
                end
                
            end
            %ylim([cylim(1),cylim(2)*(1+1/15)]);
            for i=1:length(phases_ix)
                plot((out.phases(phases_ix(i),1)-t0)*ones(1,2)/out.Fs,get(gca,'YLim'),'Color','k','LineStyle','-.');
            end
            %plot([edges(1),edges(end)],out.phases(i,1)*ones(1,2),'Color','k','LineStyle','-.');
            
            %xlabel('Delay [ms]');
            
            % set(gca,'YAxisLocation','origin','YDir','reverse');
            % cpos=get(gca,'Position');
            xlabel('Time [s]');
            ylabel('Spike rate [s^{-1}]');
            
            set(gca,'XTick',round(([out.phases(phases_ix(:),1);out.phases(phases_ix(end),2)]-t0)/out.Fs));
            xlabel('Time [s]');
            xlim(round([(out.phases(phases_ix(1),1)-t0)/out.Fs,(out.phases(phases_ix(end),2)-t0)/out.Fs]));
            
        end
        
        
        
        %___ HIGH LEVEL FUNCTIONS TO ANALYZE THE DATA AND PLOT ___
        
        function out=extract_spikes(datafile,tmM,params,ploton)
            % extract spikes from raw data file, only keeping spikes occuring during specified time interval tmM. If ploton, also plots diagnosis graphs to make sure the spike extraction is good
            
            ccols={ [0    0.4470    0.7410],...
                [0.8500    0.3250    0.0980],...
                [0.9290    0.6940    0.1250],...
                [0.4940    0.1840    0.5560],...
                [0.4660    0.6740    0.1880],...
                [0.3010    0.7450    0.9330],...
                [0.6350    0.0780    0.1840]};
            %% load data only keep spikes during tmM interval
            if nargin<2
                tmM=[];
            end
            
            if nargin<3 | isempty(params)
                params={'dtmin',10,'thresh',30,'snle_windowSize',10,'snle_period',3,'dir',1,'Fc_high',9999,'Fc_low',300,...
                    'Fc_high2',9999,'Fc_low2',100,'T',50,'Tpulse',10};
            end
            if nargin<4
                ploton=0;
            end
            params_dict=cell2struct(params(2:2:end),params(1:2:end),2);
            
            fprintf(1,'Loading data %s\n',datafile);
            u=load(datafile);
            [pathstr,name,ext] = fileparts(datafile) ;
            if isempty(pathstr)
                pathstr=pwd;
            end
            out.folder=pathstr;
            disp('Extracting spikes');
            Fs=u.fs;
            t0=1;
            t1=size(u.s,1);
            if not(isempty(tmM))
                t0=max(1,round(tmM(1)*Fs));
                t1=min(t1,round(tmM(2)*Fs));
            end
            ts=(t0:t1)'/Fs;
            % extract peaks
            [b,a] = butter(4, [params_dict.Fc_low/(Fs/2) params_dict.Fc_high/(Fs/2)]);
            filteredData = filtfilt(b,a,double(u.s(t0:t1,1)));
            [allLocs,~,~] = spikes.get_spikes_locations(filteredData,params_dict);
            spike_locs=allLocs';
            % prepare filtered data for spike profiling
            
            [b,a] = butter(4, [params_dict.Fc_low2/(Fs/2) params_dict.Fc_high2/(Fs/2)]);
            filteredData2 = filtfilt(b,a,double(u.s(t0:t1,1)));
            
            % get light phases, count spikes and get spikes properties
            disp('Counting spikes');
            phases=spikes.get_light_phases(u.s(t0:t1,3),2.5,2*params_dict.T*Fs/1000,(params_dict.T-params_dict.Tpulse)*Fs/1000);
            [t_p,t_m]=spikes.get_transitions(u.s(t0:t1,3),2.5);
            
            Nspikes=spikes.count_spikes(phases(:,1:2),spike_locs);
            %sp=spikes.get_spikes_properties(allLocs',filteredData2,t_p,phases,u.fs/1000);
            disp('Registering signals');
            [y,yp,t_mock]=spikes.register_signals(spike_locs,t_p,phases,params_dict.T*Fs/1000);
            
            
            %t_p_phases=spikes.get_phase(t_p,phases);
            out.laser=array2table([t_p,yp],'VariableNames',{'t','phase','id'});
            out.mock=array2table(t_mock,'VariableNames',{'t','phase','id'});
            out.spikes=array2table([spike_locs,y,filteredData2(spike_locs)],'VariableNames',{'t','phase','id','delay','light','ampl'});
            deltas=diff(spike_locs)/Fs*1000;
            t2p=zeros(length(spike_locs),2);
            t2p(2:end,1)=deltas;
            t2p(1:end-1,2)=deltas;
            t2p(1,1)=nan;
            t2p(end,2)=nan;
            out.spikes=cat(2,out.spikes,array2table(t2p,'VariableNames',{'t2p','t2n'}));
            
            out.data=u.s(t0:t1,:);
            out.filtered_data=filteredData2;
            out.Nspikes=Nspikes;
            %out.spikes=sp;
            out.phases=phases;
            out.Fs=Fs;
            out.lim=[t0,t1];
            
            
            % CRUDE: plot all firing rates
            if ploton
                disp('Plotting raw data');
                figure, plot(ts,[filteredData,u.s(t0:t1,3)/5]);
                hold all, scatter((allLocs+t0-1)/u.fs,filteredData(allLocs,1),'Marker','o','SizeData',20,'MarkerEdgeColor','k');
                hold all, scatter((out.laser.t+t0-1)/u.fs,out.data(out.laser.t,3)/5,'Marker','s','SizeData',20,'MarkerEdgeColor','k','MarkerFaceColor','r');
                hold all, scatter((out.mock.t+t0-1)/u.fs,ones(length(out.mock.t),1),'Marker','s','SizeData',20,'MarkerEdgeColor','k','MarkerFaceColor','g');
                plot((mean(phases(:,1:2),2)+t0-1)/u.fs,Nspikes(:,1)./Nspikes(:,2)*u.fs,'Color',ccols{3},'MarkerFaceColor',ccols{3},'Marker','o')
            end
            
        end
        
        
        function out=analyze_raw(data_desc,data_num,tmM,data_out,params)
            ccols={ [0    0.4470    0.7410],...
                [0.8500    0.3250    0.0980],...
                [0.9290    0.6940    0.1250],...
                [0.4940    0.1840    0.5560],...
                [0.4660    0.6740    0.1880],...
                [0.3010    0.7450    0.9330],...
                [0.6350    0.0780    0.1840]};
            %% load data
            if nargin<3
                tmM=[];
            end
            if nargin<4
                data_out='';
            end
            if nargin<5 | isempty(params)
                params={'dtmin',10,'thresh',30,'dir',1,'Fc_high',9999,'Fc_low',300,...
                    'Fc_high2',9999,'Fc_low2',100,'T',50,'Tpulse',10};
            end
            params_dict=cell2struct(params(2:2:end),params(1:2:end),2);
            x=dir(data_desc);
            i0=data_num;
            
            
            iid=find(cellfun(@(x) str2double(x(end-1:end))==i0,{x.name},'uni',1));
            cd([x(iid).folder '/' x(iid).name]);
            x2=dir('./gs*.mat');
            fprintf(1,'Loading data %s/%s\n',x(iid).name,x2(1).name);
            u=load(x2(1).name);
            out.folder=[x(iid).folder '/' x(iid).name];
            display('Extracting spikes');
            Fs=u.fs;
            t0=1;
            t1=size(u.s,1);
            if not(isempty(tmM))
                t0=max(1,round(tmM(1)*Fs));
                t1=min(t1,round(tmM(2)*Fs));
            end
            ts=(t0:t1)'/Fs;
            % extract peaks
            [b,a] = butter(4, [params_dict.Fc_low/(Fs/2) params_dict.Fc_high/(Fs/2)]);
            filteredData = filtfilt(b,a,double(u.s(t0:t1,1)));
            [allLocs,y_snle,~] = spikes.get_spikes_locationss(filteredData,params_dict);
            spike_locs=allLocs';
            % prepare filtered data for spike profiling
            
            [b,a] = butter(4, [params_dict.Fc_low2/(Fs/2) params_dict.Fc_high2/(Fs/2)]);
            filteredData2 = filtfilt(b,a,double(u.s(t0:t1,1)));
            
            % get light phases, count spikes and get spikes properties
            display('Counting spikes');
            phases=spikes.get_light_phases(u.s(t0:t1,3),2.5,2*params_dict.T*Fs/1000,(params_dict.T-params_dict.Tpulse)*Fs/1000);
            [t_p,t_m]=spikes.get_transitions(u.s(t0:t1,3),2.5);
            
            Nspikes=spikes.count_spikes(phases(:,1:2),spike_locs);
            %sp=spikes.get_spikes_properties(allLocs',filteredData2,t_p,phases,u.fs/1000);
            display('Registering signals');
            [y,yp,t_mock]=spikes.register_signals(spike_locs,t_p,phases,params_dict.T*Fs/1000);
            
            
            %t_p_phases=spikes.get_phase(t_p,phases);
            out.laser=array2table([t_p,yp],'VariableNames',{'t','phase','id'});
            out.mock=array2table(t_mock,'VariableNames',{'t','phase','id'});
            out.spikes=array2table([spike_locs,y,filteredData2(spike_locs)],'VariableNames',{'t','phase','id','delay','light','ampl'});
            deltas=diff(spike_locs)/Fs*1000;
            t2p=zeros(length(spike_locs),2);
            t2p(2:end,1)=deltas;
            t2p(1:end-1,2)=deltas;
            t2p(1,1)=nan;
            t2p(end,2)=nan;
            out.spikes=cat(2,out.spikes,array2table(t2p,'VariableNames',{'t2p','t2n'}));
            
            out.data=u.s(t0:t1,:);
            out.filtered_data=filteredData2;
            out.Nspikes=Nspikes;
            %out.spikes=sp;
            out.phases=phases;
            out.Fs=Fs;
            out.lim=[t0,t1];
            
            if not(isempty(data_out))
                out_fname=['./' data_out];
                fprintf(1,'Saving data into : %s\n',data_out);
                save(out_fname,'-struct','out');
            end
            % CRUDE: plot all firing rates
            display('Plotting raw data');
            figure, plot(ts,[filteredData,u.s(t0:t1,3)/5]);
            hold all, scatter((allLocs+t0-1)/u.fs,filteredData2(allLocs,1),'Marker','o','SizeData',20,'MarkerEdgeColor','k');
            hold all, scatter((out.laser.t+t0-1)/u.fs,out.data(out.laser.t,3)/5,'Marker','s','SizeData',20,'MarkerEdgeColor','k','MarkerFaceColor','r');
            hold all, scatter((out.mock.t+t0-1)/u.fs,ones(length(out.mock.t),1),'Marker','s','SizeData',20,'MarkerEdgeColor','k','MarkerFaceColor','g');
            plot((mean(phases(:,1:2),2)+t0-1)/u.fs,Nspikes(:,1)./Nspikes(:,2)*u.fs,'Color',ccols{3},'MarkerFaceColor',ccols{3},'Marker','o')
            
            
        end
        
        
        
        
        
        function analyze_batch(T,data_root,reextract,p) %p are the constant params
            % batch analysis of multiple recording. Recording are listed a table T which is typically loaded from an excel file describing all the recordings
            ccols={ [0    0.4470    0.7410],...
                [0.8500    0.3250    0.0980],...
                [0.9290    0.6940    0.1250],...
                [0.4940    0.1840    0.5560],...
                [0.4660    0.6740    0.1880],...
                [0.3010    0.7450    0.9330],...
                [0.6350    0.0780    0.1840]};
            if nargin<4
                p={'Fc_high',9999,'Fc_low',300,...
                    'Fc_high2',9999,'Fc_low2',100,'T',50,'Tpulse',10,'name','juxta',...
                    't2use',[-1,1],'w',2,'reject_thresh',3,'ampl_reject_thresh',3};
            end
            
            if nargin<3
                reextract=struct('spikes',1,'clusters',1,'profiles',1,'stats',1);
            end
            names=spikes.get_name(T);
            %Tout=cell2table(names,'VariableNames','name');
            for i=1:height(T)
                data_date=T.date{i};
                data_num=T.id(i);
                
                %                 data_desc=T(i).desc;
                tmM=eval(T.lim{i}); %[0,199.8];
                
                params=cat(2,{'thresh',T.thresh(i),'dir',T.dir(i),'snle_windowSize',10,'snle_period',3},p);
                
                params_dict=cell2struct(params(2:2:end),params(1:2:end),2);
                
                dataname=[names{i}];
                if not(isempty(params_dict.name))
                    dataname=[params_dict.name '_' names{i}];
                end
                
                datafile=spikes.find_data(data_root,data_date,data_num);
                
                
                if reextract.spikes
                    out=spikes.extract_spikes(datafile,tmM,params,0);
                    out.dataname=dataname;
                    save(fullfile(out.folder,[dataname '_spikes.mat']),'-struct','out');
                else
                    out=spikes.load_data(data_root,T(i,:),params_dict.name,'spikes.mat');
                end
                
                if reextract.clusters
                    disp('Clustering spikes');
                    %T.na(i)
                    %size(out.filtered_data(out.spikes.t,1))
                    ampls=out.filtered_data(out.spikes.t,1);
                    ampls_med=median(ampls);
                    ampls_mad=mad(ampls);
                    i2keep=abs(ampls-ampls_med)<params_dict.ampl_reject_thresh*ampls_mad;
                    %i2keep_ix=find(i2keep);
                    %i2rm=find(not(i2keep))
                    gps=zeros(length(ampls),1);
                    ds=zeros(length(ampls),1);
                    [agps,C]=kmeans(ampls(i2keep),T.na(i));
                    %                 kidx = kmeans(inputimage(:), k);
                    % clusermeans = accumarray(kidx, inputimage(kidx),[], @mean);
                    [~, sortidx] = sort(C,1,'descend');
                    agps = sortidx(agps);
                    %clustered = reshape(kidxmapped, size(inputImage));
                    
                    ds2=zeros(length(agps),1);
                    gps1=agps;
                    f_profs(T.na(i)*T.n(i),1)=fcs();
                    
                    for jj=1:T.na(i)
                        cix=find(agps==jj);
                        
                        [cgps,cf_profs,d]=spikes.cluster_spikes(out.filtered_data,out.spikes.t(cix),params_dict.w,out.Fs,T.n(i),...
                            params_dict.t2use,params_dict.reject_thresh);
                        ds2(cix)=d;
                        
                        
                        
                        gps1(cix)=(jj-1)*T.n(i)+cgps;
                        f_profs((jj-1)*T.n(i)+1:jj*T.n(i))=cf_profs;
                    end
                    gps(i2keep)=gps1;
                    d(i2keep)=ds2;
                    save(fullfile(out.folder,[dataname '_profiles.mat']),'gps','f_profs','d');
                else
                    if reextract.profiles
                        disp('Using saved clustering data')
                        u=spikes.load_data(data_root,T((i),:),'juxta','profiles.mat');
                        disp('Reanalyzing profiles')
                        gps=u.gps;
                        ugps=unique(gps(gps>0));
                        f_profs(length(ugps),1)=fcs();
                        for jj=1:length(ugps)
                            f_profs(jj,1).tau=u.f_profs(jj,1).tau;
                            f_profs(jj,1).n=u.f_profs(jj,1).n;
                            f_profs(jj,1).g2=u.f_profs(jj,1).g2;
                        end
                        
                        
                        save(fullfile(out.folder,[dataname '_profiles.mat']),'gps','f_profs');
                    else
                        disp('Using saved clustering AND profiles data')
                        u=spikes.load_data(data_root,T((i),:),'juxta','profiles.mat');
                        gps=u.gps;
                        f_profs=u.f_profs;
                        
                    end
                    
                end
                f_profs.subrnd(30).plot_linear();
                f0=gcf;
                cpos=get(gca,'Position');
                set(gca,'Units','inches');
                set(gca,'Position',[cpos(1:2),1.5,1.5]);
                set(gca,'Units','normalized');
                
                disp('Plotting data');
                
                f1=figure;
                histogram(out.filtered_data(out.spikes.t(gps>0),1),...
                    linspace(min(out.filtered_data(out.spikes.t(gps>0),1)),max(out.filtered_data(out.spikes.t(gps>0),1)),...
                    50));
                xlabel('Spike ampl');
                ylabel('%');
                ts=(1:size(out.filtered_data,1))'/out.Fs;
                cpos=get(gca,'Position');
                set(gca,'Units','inches');
                set(gca,'Position',[cpos(1:2),1.5,1.5]);
                set(gca,'Units','normalized');
                
                f2=figure;
                plot(ts,[out.filtered_data],'Color',[0.6,0.6,0.6]);
                
                hold all
                plot(ts,out.data(:,3)/5,'Color',ccols{6});
                
                
                plot((mean(out.phases(:,1:2),2))/out.Fs,out.Nspikes(:,1)./out.Nspikes(:,2)*out.Fs/6,'Color','k','MarkerFaceColor','k','Marker','s')
                
                for j=1:(T.n(i)*T.na(i))
                    clean_spikes=out.spikes.t(gps==j);
                    scatter((clean_spikes)/out.Fs,out.filtered_data(clean_spikes,1),'Marker','o','SizeData',20,'MarkerEdgeColor',ccols{j});
                    
                    cNspikes=spikes.count_spikes(out.phases,clean_spikes);
                    plot((mean(out.phases(:,1:2),2))/out.Fs,cNspikes(:,1)./cNspikes(:,2)*out.Fs/6,'Color',ccols{j},'MarkerFaceColor',ccols{j},'Marker','o','MarkerEdgeColor','k')
                    
                end
                xlabel('Time [s]');
                cpos=get(gca,'Position');
                set(gca,'Units','inches');
                set(gca,'Position',[cpos(1:2),7,1.5]);
                set(gca,'Units','normalized');
                
                f3=figure;
                for j=1:(T.n(i)*T.na(i))
                    clean_spikes=out.spikes.t(gps==j);
                    scatter((clean_spikes)/out.Fs,out.filtered_data(clean_spikes,1),'Marker','o','SizeData',20,'MarkerEdgeColor',ccols{j});
                    hold all
                end
                xlabel('Time [s]');
                ylabel('Spike amplitude');
                
                set(gca,'Units','inches');
                set(gca,'Position',[cpos(1:2),7,1.5]);
                set(gca,'Units','normalized');
                
                f4=figure;
                for j=1:(T.n(i)*T.na(i))
                    clean_spikes=out.spikes.t(gps==j);
                    scatter((clean_spikes)/out.Fs,d(gps==j),'Marker','o','SizeData',20,'MarkerEdgeColor',ccols{j});
                    hold all
                end
                xlabel('Time [s]');
                ylabel('Spike distance to cluster median');
                set(gca,'Units','inches');
                set(gca,'Position',[cpos(1:2),7,1.5]);
                set(gca,'Units','normalized');
                coolfig.compose({f0,f1,[];f2,f3,f4}',[-1,-1],1,'normalized',1,[0,0,0,0]);
                
                %                 figure;
                %                  for j=1:T.n(i)
                %                     clean_spikes=out.spikes.t(gps==j);
                %                 scatter((clean_spikes)/out.Fs,out.filtered_data(clean_spikes,1),'Marker','o','SizeData',20,'MarkerEdgeColor',ccols{j});
                %                 hold all
                %                  end
                
                %hold all, scatter((out.laser.t+t0-1)/u.fs,out.data(out.laser.t,3)/5,'Marker','s','SizeData',20,'MarkerEdgeColor','k','MarkerFaceColor','r');
                %hold all, scatter((out.mock.t+t0-1)/u.fs,ones(length(out.mock.t),1),'Marker','s','SizeData',20,'MarkerEdgeColor','k','MarkerFaceColor','g');
                %plot((mean(phases(:,1:2),2)+t0-1)/u.fs,Nspikes(:,1)./Nspikes(:,2)*u.fs,'Color',ccols{3},'MarkerFaceColor',ccols{3},'Marker','o')
                
                
                %                 disp('Computing stats');
                %                 nboot=1000;
                %                 [stats0,stats1]=spikes.summarize_statistics(out,gps,nboot);
                %                 save(fullfile(out.folder,[dataname '_stats.mat']),'stats0','stats1');
            end
            
        end
        
        
        %___ HIGH LEVEL FUNCTIONS TO CLUSTER THE SPIKES ONCE IDENTIFIED  ___
        
        
        function [gps_01,f_profs,d]=cluster_spikes(data,ts,w,Fs,n,t2use,reject_thresh) %w in ms
            % data is the output of the spike identification
            w2=w*Fs/1000;
            t_profile=[-w2:1:w2]'/Fs*1000;
            profs=spikes.get_spikes_profiles(data,ts,w2);
            profs=bsxfun(@times,profs,1./data(ts)); %normalize spikes
            
            % first pass, classify independently of light status
            mad_reject=reject_thresh;
            iter=1;
            ix2use=t_profile>t2use(1) & t_profile<t2use(2);
            [gps_01,d,C]=spikes.classify_spikes(profs,ix2use,n,mad_reject,iter);
            
            
            % size(C)
            [~, sortidx] = sort(sum(C,2));
            %sortidx
            outliers=gps_01<0;
            gps_01 = sortidx(abs(gps_01)); %remaping to sort by integral value
            gps_01(outliers)=-gps_01(outliers);
            %clustered = reshape(kidxmapped, size(inputImage));
            
            
            %spikes_clean=out.spikes(gps_01>0 & out.spikes.id>0,:);
            %profs_clean=profs(gps_01>0 & out.spikes.id>0,:);
            
            f_profs(n,1)=fcs;
            for i=1:n
                f_profs(i)=spikes.profiles2fcs(profs(gps_01==i,:),t_profile');
            end
            %f_profs.sub(round((f_profs.numel-1)*rand(30,1))+1).plot_linear([],1);
            %f_profs.plot_linear(gcf,2);
            % set(gca,'XTick',-w:1:w,'XMinorTick','on');
            % xlabel('Time [ms]');
            % ylabel('Amplitude');
            % ff.savefig(gcf,'spike_singles',[0.8,0.6,4]);
            
            
            % f_profs.plot_linear([],1);
            % set(gca,'XTick',-2:1:2,'XMinorTick','on');
            % xlabel('Time [ms]');
            % ylabel('Amplitude');
            %
            % ff.savefig(gcf,'spike_singles',[0.8,0.6,4]);
            
        end
        
        %___ UTILITY FUNCTIONS TO FIND THE RECORDINGS  ___
        
        function [datafile,datafolder]=find_data(rootfolder,data_date,data_num,data_desc)
            if nargin==2
                T=data_date;
                datafile={};
                datafolder={};
                for i=1:height(T)
                    data_date=T.date{i};
                    data_num=T.id(i);
                    [cdatafile,cdatafolder]=spikes.find_data(rootfolder,data_date,data_num,'gs*.mat');
                    datafile=cat(1,datafile,{cdatafile});
                    datafolder=cat(1,datafolder,{cdatafolder});
                end
                
            else
                if nargin<4
                    data_desc='gs*.mat';
                end
                x=dir(fullfile(rootfolder,['*' data_date '*']));
                i0=data_num;
                
                
                iid=find(cellfun(@(x) str2double(x(end-1:end))==i0,{x.name},'uni',1));
                %cd([x(iid).folder '/' x(iid).name]);
                datafolder=fullfile(x(iid).folder,x(iid).name);
                x2=dir(fullfile(datafolder,data_desc));
                datafile=fullfile(datafolder,x2(1).name);
                fprintf(1,'Found data %s\n',datafile);
            end
        end
        
        function names=get_name(data_date,data_num,tmM,phase)
            
            if nargin==1
                names={};
                T=data_date;
                for i=1:height(T)
                    tmM=eval(T.lim{i});
                    phase=T.phase{i};
                    data_date=T.date{i};
                    data_num=T.id(i);
                    
                    dataname=[data_date '_' num2str(data_num)];
                    if not(isempty(tmM))
                        if isempty(phase)
                            dataname=[dataname '_' num2strFixedDigits(tmM(1),4)...
                                'to' num2strFixedDigits(tmM(2),4)];
                            
                        else
                            dataname=[dataname '_' T.phase{i}];
                            
                        end
                    end
                    names=cat(1,names,{dataname});
                end
            else
                if nargin<3
                    tmM=0;
                end
                if nargin<4
                    phase=[];
                end
                dataname=[data_date '_' num2str(data_num)];
                if not(isempty(tmM))
                    if isempty(phase)
                        dataname=[dataname '_' num2strFixedDigits(tmM(1),4)...
                            'to' num2strFixedDigits(tmM(2),4)];
                        
                    else
                        dataname=[dataname '_' phase];
                        
                    end
                end
                names=dataname;
            end
            
        end
        function s2=add_psfix(s,pfix,sfix,sep)
            s2=s;
            if not(isempty(pfix))
                s2=[pfix sep s2];
            end
            if not(isempty(sfix))
                s2=[s2 sep sfix];
            end
        end
        
        function out=load_data(rootf,T,prfx,sfx)
            n=height(T);
            [~,foldername]=spikes.find_data(rootf,T);
            names=spikes.get_name(T);
            for i=n:-1:1
                f2load=fullfile(foldername{i},spikes.add_psfix(names{i},prfx,sfx,'_'));
                fprintf(1,'Loading file %s\n',f2load);
                out{i,1}=load(f2load);
            end
            if n==1
                out=out{1};
            end
            
        end
        %             if nargin<4
        %                 desc='juxta_raw.mat';
        %             end
        %             data_desc=fullfile(rootf,['*' date_str '*']);
        %             x=dir(data_desc);
        %             i0=data_num;
        %
        %
        %             iid=find(cellfun(@(x) str2double(x(end-1:end))==i0,{x.name},'uni',1));
        %             %cd([x(iid).folder '/' x(iid).name]);
        %             x2=dir(fullfile(x(iid).folder,x(iid).name,desc));
        %             fprintf(1,'Loading data %s/%s\n',x2(1).folder,x2(1).name);
        %             out=load(fullfile(x2(1).folder,x2(1).name));
        %end
        
        function nth=get_nth(x,n)
            if length(x)<n
                nth=nan;
            else
                nth=x(n);
            end
        end
        
        
        function spikes_nth=get_nth_spike(s,n)
            
            a=accumarray([s.phase,s.id],(1:length(s.phase))',[],@(x) spikes.get_nth(x,n),nan);
            a=reshape(a',numel(a),1);
            spikes_nth=s(a(not(isnan(a))),:);
        end
        
        function s=get_statistics(spikes,laser,phases_ix)
            %first photon delay
            %phaseid=accumarray([laser.phase,laser.id],1);
            n1=max(laser.phase);
            if nargin<3
                phases_ix=1:n1;
            else
                n1=max(max(phases_ix),n1);
            end
            ntriggers=accumarray(laser.phase,laser.id,[n1,1],@max,0);
            ntriggers_cum=[0;cumsum(ntriggers(phases_ix))];
            n2=max(ntriggers);
            %
            
            Nspikes=accumarray([spikes.phase, spikes.id],1,[max(n1,max(phases_ix)),n2],@sum,0);
            mindelay=accumarray([spikes.phase, spikes.id],spikes.delay,[max(n1,max(phases_ix)),n2],@min,NaN);
            secondDelay=accumarray([spikes.phase, spikes.id],spikes.delay,[max(n1,max(phases_ix)),n2],@(x) x(min(length(x),2)),NaN);
            secondDelay(Nspikes<2)=nan;
            
            
            N=zeros(ntriggers_cum(end),1);
            d=zeros(ntriggers_cum(end),2);
            
            
            for i=1:length(phases_ix)
                N(1+ntriggers_cum(i):ntriggers_cum(i+1))=Nspikes(phases_ix(i),1:ntriggers(phases_ix(i)))';
                d(1+ntriggers_cum(i):ntriggers_cum(i+1),1)=mindelay(phases_ix(i),1:ntriggers(phases_ix(i)))';
                d(1+ntriggers_cum(i):ntriggers_cum(i+1),2)=secondDelay(phases_ix(i),1:ntriggers(phases_ix(i)))';
            end
            s.N=N;
            s.d=d;
        end
        
        function [stats0,stats1]=summarize_statistics(out,gps,nboot)
            if isempty(gps)
                gps=ones(height(out.spikes),1);
            end
            mygps=unique(gps);
            mygps=gps(mygps>0);
            stats0(length(mygps),1)=struct('delay1',[],'delay2',[],'frac',[],'rates',[]);
            stats1(length(mygps),1)=struct('delay1',[],'delay2',[],'frac',[],'rates',[],'drates',[]);
            
            spikes_clean=out.spikes(out.spikes.id>0 & gps>0,:);
            for i=1:length(mygps)
                s1=spikes.get_statistics(out.spikes(out.spikes.light==1 & gps==i & out.spikes.id>0,:),out.laser);
                s0=spikes.get_statistics(out.spikes(out.spikes.light==0 & gps==i & out.spikes.id>0,:),out.mock);
                
                CI=bootci(nboot,@nanmedian,s1.d(:,1));
                stats1(i).delay1=[nanmedian(s1.d(:,1));CI;mad(s1.d(:,1))]'/out.Fs*1000;
                x2=s1.d(s1.d(:,2)>(stats1(i).delay1(1)+stats1(i).delay1(4))*out.Fs/1000,2)
                if length(x2)>3 &  sum(not(isnan(x2)))>3
                    CI=bootci(nboot,@nanmedian,x2);
                    stats1(i).delay2=[nanmedian(x2);CI;mad(x2)]'/out.Fs*1000;
                else
                    stats1(i).delay2=[];
                end
                nn=length(s1.N);
                stats1(i).frac=[sum(s1.N==0),sum(s1.N==1),sum(s1.N==2),sum(s1.N>2)]/nn;
                
                
                CI=bootci(nboot,@nanmedian,s0.d(:,1));
                stats0(i).delay1=[nanmedian(s0.d(:,1));CI;mad(s0.d(:,1))]'/out.Fs*1000;
                x2=s0.d(s0.d(:,2)>(stats0(i).delay1(1)+stats0(i).delay1(4))*out.Fs/1000,2);
                if length(x2)>3 &  sum(not(isnan(x2)))>3
                    
                    CI=bootci(nboot,@nanmedian,x2);
                    stats0(i).delay2=[nanmedian(x2);CI;mad(x2)]'/out.Fs*1000;
                else
                    stats0(i).delay2=[];
                end
                nn=length(s0.N);
                stats0(i).frac=[sum(s0.N==0),sum(s0.N==1),sum(s0.N==2),sum(s0.N>2)]/nn;
                
                Nsp=spikes.count_spikes(out.phases,spikes_clean.t);
                
                r=Nsp(:,1)./Nsp(:,2)*out.Fs;
                r1=r(out.phases(:,3)==1);
                r0=r(out.phases(:,3)==0);
                stats0(i).rates=[mean(r0),std(r0),length(r0)];
                stats1(i).rates=[mean(r1),std(r1),length(r1)];
                
                dr=spikes.getRateChange(out.phases,spikes_clean.t,1,0);
                stats1(i).drates=[mean(dr),std(dr),length(dr)];
                stats1(i).raw=s0;
                stats1(i).dr=dr;
                stats0(i).raw=s0;
                
                
            end
            %save(fullfile(out.folder,'spikes_stats.mat'),'dr','s0','s1','stats0','stats1');
        end
        
        function T=stats2table(stats,nameexp)
            if nargin<2
                nameexp='';
            end
            cdelay1=stats.delay1;
            if isempty(cdelay1)
                cdelay1=nan(1,4);
            end
            cdelay2=stats.delay2;
            if isempty(cdelay2)
                cdelay2=nan(1,4);
            end
            T=cat(2,array2table(stats.rates,'VariableNames',cellfun(@(x) [x nameexp],{'rate_mean','rate_std','rate_n'},'uni',0)),...
                array2table(stats.frac,'VariableNames',cellfun(@(x) [x nameexp],{'f0','f1','f2','fmore'},'uni',0)),...
                array2table(cdelay1,'VariableNames',cellfun(@(x) [x nameexp],{'delay1_med','delay1_CIlow','delay1_CIhigh','delay1_mad'},'uni',0)),...
                array2table(cdelay2,'VariableNames',cellfun(@(x) [x nameexp],{'delay2_med','delay2_CIlow','delay2_CIhigh','delay2_mad'},'uni',0)));
            if isfield(stats,'drates')
                T=cat(2,T,array2table(stats.drates,'VariableNames',cellfun(@(x) [x nameexp],{'drate_mean','drate_std','drate_n'},'uni',0)));
            end
        end
        
        function [T0,T1]=load_stats(data_root,T,dataname,fname)
            T0=table();
            T1=table();
            for i=1:height(T)
                cstats=spikes.load_data(data_root,T(i,:),dataname,fname);
                %                 cstats0=cstats.stats0(T.gp2plot(i))
                %                 cstats1=cstats.stats1(T.gp2plot(i))
                T0=cat(1,T0,spikes.stats2table(cstats.stats0(T.gp2plot(i)),'_mock'));
                T1=cat(1,T1,spikes.stats2table(cstats.stats1(T.gp2plot(i)),'_light'));
            end
            
        end
        
        function y=trains_correlogram(t1,t2,downsmpl,maxlag) %uses
            %     % if two spike trains, computes the cross correlogram. If only one,
            %     % compute with respect to the laser pulse. This is gonna split the
            %     % train using the light phases
            %
            
            maxlag_downsmpl=int32(ceil(maxlag/downsmpl));
            if isempty(t2)
                tmM=[t1(1),t1(end)]; %that's the tmM
                train1=spikes.train2vec(t1,tmM,downsmpl);
                [y,~]=xcorr(train1,train1,maxlag_downsmpl,'coeff');
                
                
            else
                tmM=[min(t1(1),t2(1)) max(t1(end),t2(end))]; %that's the tmM
                train1=spikes.train2vec(t1,tmM,downsmpl);
                train2=spikes.train2vec(t2,tmM,downsmpl);
                [y,~]=xcorr(train1,train2,maxlag_downsmpl,'coeff');
            end
            
        end
        
        function [y,lag]=correlogram(t1,t2,phases1,phases2,downsmpl,maxlag)
            %first relabel phases so that go from 1 to xxx
            maxlag_downsmpl=int32(ceil(maxlag/downsmpl));
            lag=-double(maxlag_downsmpl):double(maxlag_downsmpl);
            nlags=length(lag);
            if isempty(t2)
                [u,~,phases_remapped]=unique(phases1); %remap phases so that they go from 1 to n
                nn=size(u,1);
                if not(issorted(phases_remapped))
                    warning('Phases not sorted, may produce erronous output');
                end
                t1_byphase=accumarray(phases_remapped,t1,[nn,1],@(x) {x});
                y_cell=cellfun(@(x) spikes.trains_correlogram(x,[],downsmpl,maxlag),t1_byphase,'uni',0);
                y=horzcat(y_cell{:})';
            else
                phases_all=unique(cat(1,unique(phases1),unique(phases2))); % can only look
                y=zeros(length(phases_all),nlags);
                for i=1:length(phases_all)
                    i1=phases1==phases_all(i);
                    i2=phases2==phases_all(i);
                    if sum(i1) & sum(i2)
                        y(i,:)=spikes.trains_correlogram(t1(i1),t2(i2),downsmpl,maxlag);
                    end
                end
                
            end
            
            
        end
        
        function [ix,t,y]=get_data(data,tmiddle,d,Fs,center)
            i0=max(1,(tmiddle-d(1))*Fs);
            i1max=inf;
            if not(isempty(data))
                i1max=size(data,1);
            end
            i1=min((tmiddle+d(2))*Fs,i1max);
            ix=[i0,i1];
            t=[];
            y=[];
            if isfinite(i1)
                t=(i0:i1)'/Fs-center*tmiddle;
            end
            if not(isempty(data)) & isfinite(i1)
                y=data(i0:i1,:);
            end
            
        end
        
        %         function [ixrel,val]=get_triggers(trigTimes,trigVals,tminmax,Fs) %index of the trigger in a relative stream
        %          %trigTimes are absolute times for trigger, includes the
        %          %first one whcih is file start
        %          i2keep=trigTimes<=tminmax(2) & trigTimes>=tminmax(1);
        %          trigs2keep=trigTimes(i2keep);
        %          val=trigVals(i2keep);
        %          ixrel=max(1,int32(floor(trigs2keep-tminmax(1))/Fs));
        %         end
        
        function [hsP,hsM]=plot_triggers(ax,trigTimes,trigVals,tmiddle,d,center,pulse_dt,vals)
            if nargin<8
                vals=[0,1]; %y,height
            end
            if isempty(ax)
                ax=gca();
            end
            
            %[ixrel,val]=spikes.get_triggers(trigTimes,trigVals,[tmiddle-d(1),tmiddle+d(2)],Fs);
            tminmax=[tmiddle-d(1),tmiddle+d(2)];
            i2keep=trigTimes<=tminmax(2) & trigTimes>=tminmax(1);
            
            val=trigVals(i2keep);
            
            
            %             [ix,t]=spikes.get_data([],tmiddle,d,Fs,center);
            s2plot=trigTimes(i2keep)-center*tmiddle;
            s2plotP=s2plot(val>0);
            s2plotM=s2plot(val==0);
            hsP=zeros(length(s2plotP),1);
            for i=1:length(s2plotP)
                hsP(i)=rectangle('Position',[s2plotP(i),vals(1),pulse_dt,vals(2)],'FaceColor','b',...
                    'EdgeColor','none');
            end
            hsM=zeros(length(s2plotM),1);
            for i=1:length(s2plotM)
                hsM(i)=rectangle('Position',[s2plotM(i),vals(1),pulse_dt,vals(2)],'FaceColor',[0.3,0.3,0.3],...
                    'EdgeColor','none');
            end
        end
        
        function hs=plot_spikes(ax,s,tmiddle,d,Fs,center,vals)
            if nargin<6
                vals=[0,1];
            end
            if isempty(ax)
                ax=gca();
            end
            [ix,t]=spikes.get_data([],tmiddle,d,Fs,center);
            
            s2plot=s(s>=ix(1) & s<=ix(2))/Fs-center*tmiddle;
            
            hs=zeros(length(s2plot),1);
            for i=1:length(s2plot)
                hs(i)=line(ax,s2plot(i)*[1,1],vals);
                hold all
            end
            
        end
        
        function hs=plot_laser(ax,s,tmiddle,d,Fs,center,pulse_dt,vals)
            if nargin<8
                vals=[0,1]; %y,height
            end
            if isempty(ax)
                ax=gca();
            end
            [ix,t]=spikes.get_data([],tmiddle,d,Fs,center);
            s2plot=s(s>=ix(1) & s<=ix(2))/Fs-center*tmiddle;
            
            hs=zeros(length(s2plot),1);
            for i=1:length(s2plot)
                hs(i)=rectangle('Position',[s2plot(i),vals(1),pulse_dt,vals(2)],'FaceColor','b',...
                    'EdgeColor','none');
            end
        end
        
        function hs=plot_phases(ax,phases,tmiddle,d,Fs,center,vals)
            if nargin<7
                vals=[0,1]; %y,height
            end
            if isempty(ax)
                ax=gca();
            end
            
            
            ix=spikes.get_data([],tmiddle,d,Fs,center)
            p1=find(phases(:,1)<=ix(1) & phases(:,2)>=ix(1),1,'first')
            p2=find(phases(:,1)<=ix(2) & phases(:,2)>=ix(2),1,'last')
            if not(isempty(p1))
                phases2=phases;
                if not(isempty(p2))
                    phases2=phases2(p1:p2,:);
                    phases2(1,1)=ix(1);
                    phases2(end,2)=ix(2);
                else
                    phases2=phases(p1:end,:);
                    phases2(1,1)=ix(1);
                end
                
                
                phases2p=phases2(phases2(:,3)==1,1:2)
                
                
                hs=zeros(size(phases2p,1),1);
                
                
                for i=1:size(phases2p,1)
                    i
                    hs(i)=rectangle('Position',[phases2p(i,1)/Fs-center*tmiddle,vals(1),(phases2p(i,2)-phases2p(i,1))/Fs,vals(2)],'FaceColor','b',...
                        'EdgeColor','none');
                end
            end
        end
        
        
        
        
        function [hax,hs,hs_l,hs_s]=plot(ax,data,sp,l,lim,Fs,center,pulse_dt,rg,p)
            if isempty(ax)
                figure;
                hax=gca;
            else
                hax=ax;
            end
            
            if length(lim)==2
                tmiddle=lim(1);
                d=[0,lim(2)];
            else
                tmiddle=lim(1);
                d=lim(2:3);
            end
            
            hs=[];
            hs_l=[];
            hs_s=[];
            rg2=rg;
            if not(isempty(data))
                [ix,t,y]=spikes.get_data(data,tmiddle,d,Fs,center);
                %figure;
                hs=zeros(size(y,2),1);
                for j=1:size(y,2)
                    hs(j)=line(gca,t,y(:,j),'Color','k');
                    hold all
                end
                if isempty(rg)
                    rg=get(gca,'YLim');
                    
                end
            end
            drg=diff(rg);
            rg2=[rg(1)-sum(p)*drg,rg(2)];
            ylim(rg2);
            
            
            if not(isempty(l))
                if size(l,2)==3 %the we are looking at phases!
                    hs_l=spikes.plot_phases(gca,l,tmiddle,d,Fs,center,[rg2(1),p(1)*drg]);
                else
                    hs_l=spikes.plot_laser(gca,l,tmiddle,d,Fs,center,pulse_dt,[rg2(1),p(1)*drg]);
                end
            end
            if not(isempty(sp))
                hs_s=spikes.plot_spikes(gca,sp,tmiddle,d,Fs,center,[rg2(1)+(p(1)+p(2))*drg,rg2(1)+(p(1)+p(2)+p(3))*drg]);
            end
        end
        
        
    end
    
    
end
