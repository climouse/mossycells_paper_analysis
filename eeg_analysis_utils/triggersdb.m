classdef triggersdb < handle
    properties
        db=eegdb(); %linked eeg database
        sz=[]; % linked sz database
        map=[]; %coordinates of triggers onto sz database = [ix,t2beg, bhflag]
        data={}; % these data are linked to an eeg database
    end
    
    
    methods
        function obj=triggersdb(db)
            if nargin>0
            obj.db=db;
            end
            
        end
        
        
         function makedb(obj) % make the database of triggers, %trig numbers in minutes!
            n=size(obj.db.data,1);
            if obj.db.simplefiles
                obj.data=[];
                for i=1:n
                u=load(fullfile(obj.db.data.pathstr{i},obj.db.data.name{i}),'trdata');
                
                cstamps=u.trdata(1).timestamp;
                if length(cstamps)>2
                ctriggers=[i*ones(length(cstamps)-2,1),(cstamps(2:end-1)-cstamps(1))*(24*60),u.trdata(1).leds',cstamps(2:end-1)];
                [~,srtixs]=sort(ctriggers(:,2));
                obj.data=cat(1,obj.data,ctriggers(srtixs,:));
                %{[fileid,t[mn],led,t0]}
                end
                end
                obj.data={obj.data};
            
                
                
            else
            obj.data=repmat({[]},1,16);
            
            
            for i=1:n
                try
                u=load(fullfile(obj.db.data.pathstr{i},obj.db.data.name{i}),'trdata');
               % nnn=length(u.trdata)
                for j=1:length(u.trdata)
                cstamps=u.trdata(j).timestamp;
                if length(cstamps)>2
                ctriggers=[i*ones(length(cstamps)-2,1),(cstamps(2:end-1)-cstamps(1))*(24*60),u.trdata(j).leds',cstamps(2:end-1)];
                [~,srtixs]=sort(ctriggers(:,2));
                obj.data{j}=cat(1,obj.data{j},ctriggers(srtixs,:));
                %{[fileid,t[mn],led,t0]}
                end
                end
                catch e
                    fprintf(1,'Skipping file :%\n',fullfile(obj.db.data.pathstr{i},obj.db.data.name{i}));
                end
            end
            end
            
        end
        
        
        function [ixs,deltas]=query_closest(obj,t,chan,sgn,db_ix) %find the closest trigger to the list of trigger, and compute the difference
            %db_ix, restricts search to the the database ix indicated (for
            %example if we know what file this is from!)
            ixs=zeros(length(t),1);
            deltas=zeros(length(t),1);
            if nargin>4
                trigselect=obj.data{chan}(:,1)==db_ix;
                trigs=obj.data{chan}(trigselect,4); %/(24*60)+obj.data.t0(trigselect);
            else
                trigs=obj.data{chan}(:,4); %/(24*60)+obj.data.t0;
            end
            if sgn>0 %closest before
                for i=1:length(t)
                    [dmin,imin]=min(t(i)-trigs(trigs<=t(i)));
                    ixs(i)=imin;
                    deltas(i)=dmin;
                end
            else %sign =-1 closest after
                for i=1:length(t)
                    [dmin,imin]=min(trigs(trigs>=t(i))-t(i));
                    ixs(i)=imin;
                    deltas(i)=-dmin;
                end
            end
                    
        end
        
        function ixs=query(obj,t,chan,db_ix) %find the closest trigger to the list of trigger, and compute the difference
            %db_ix, restricts search to the the database ix indicated (for
            %example if we know what file this is from!)
            ixs=zeros(length(t),1);
            if nargin>3
                trigselect=obj.data{chan}(:,1)==db_ix;
                trigs=obj.data{chan}(trigselect,4); %/(24*60)+obj.data.t0(trigselect);
            else
                trigs=obj.data{chan}(:,4); %/(24*60)+obj.data.t0;
            end
                for i=1:length(t)
                    
                    ix=find(trigs==t(i));
                    if not(ismepty(ix))
                    ixs(i)=ix(1); % there should be no duplicate in triggers;
                    end
                end            
        end
        
        function vals=query_interval(obj,t,T,chan,db_ix) % find all the triggers between t and t+T
            %nchan=length(chan);
            %for i=1:length(t)
            if nargin>4
                trigselect=find(obj.data{chan}(:,1)==db_ix);
                trigs=obj.data{chan}(trigselect,:); %/(24*60)+obj.data.t0(trigselect);
                 ix=find(trigs(:,4)>=t & trigs(:,4)<=(t+T/(60*60*24)));
                 vals=cat(2,ix,trigs(ix,:));
                 vals(:,1)=trigselect(ix);
            else
                trigs=obj.data{chan}; %/(24*60)+obj.data.t0;
                ix=find(trigs(:,4)>=t & trigs(:,4)<=(t+T/(60*60*24)));
                 vals=cat(2,ix,trigs(ix,:));
            end
                %trigs=obj.data{chan}; %chan(max(i,nchan))};
                %t0=obj.db.data.t0(trigs(:,1))+trigs(:,2);
               
                %if not(isempty(ix))
                    
                %end
                
                    
            %end
        end
        
        function T=toTable(obj,chan)
            %{[fileid,t[mn],led,t0]}
            T=array2table(obj.data{chan}(:,[1,3,4]),'VariableNames',{'fileid','led','t0'});
            n=size(T,1);
            T=cat(2,table((1:n)','VariableNames',{'trigid'}),T);
        end
%         

% 
%          function link_szdb(obj,sz)
%              % note : the sz database need to be already linked to the eeg
%              % db! %sz database should also contain the 
%              obj.sz=sz;
%             n=sz.length();
%             obj.map=cellfun(@(x) zeros(length(x),1),1,8),obj.data,'uni',0); %zeros(n,3) %sz #, time from beg, isbh
%             for i=1:n
%                 
%                 sz.map
%                 sz
%                 
%                 
%              end
% % %         end
%         
    end
end