classdef fcs < handle
    % This class is a data structure to hold an ensemble of spike waveforms. g2 matrix where each row is a waveform profile (the columns represent the time points), sampled at time points tau. tau is a row vector, all the waveforms should be sampled at the same time points. This code was initally written for a completely different project and to look at completely different type of data and was adapted and simplified to look at spike waveforms.
    properties
        n=0;
        g2=[]; %spike amplitude
        tau=[]; %time
        
    end
    
    
    methods
        function obj=fcs(traces,varargin)
            if nargin==0
                obj;
            else % hard copy
                if nargin==1 && strcmp(class(traces),'fcs')
                    no=length(traces);
                    for i=no:-1:1
                        obj(i,1).n=traces(i,1).n;
                        obj(i,1).g2=traces(i,1).g2;
                        obj(i,1).tau=traces(i,1).tau;
                        
                    end
                else
                end
            end
        end
        
        function n=numel(obj)
            no=length(obj);
            n=zeros(no,1);
            for i=1:no
                n(i)=size(obj(i).g2,1);
            end
        end
        
        function [cf,lh,lh2]=plot_linear(obj,figh,mode,cols,usemed,shading_f)
            if nargin<5
                usemed=0;
            end
            if nargin<4
                cols=[];
            end
            if nargin<3
                mode=1;
            end
            if nargin<2
                figh=[];
            end
            lh2={};
            if nargin<6
                shadingFactor=0.5;
                bandShadingFactor=0.5;
            else
                shadingFactor=shading_f;
                bandShadingFactor=shading_f;
            end
            if isa(mode,'fcs')
                if usemed
                    
                    [cf,lh]=obj.median.plotErrorBand_linear(figh,cols,[],usemed,shadingFactor,mode,bandShadingFactor);
                else
                    [cf,lh]=obj.average.plotErrorBand_linear(figh,cols,[],usemed,shadingFactor,mode,bandShadingFactor);
                end
            else
                switch mode
                    case 0 % only plots the avg/med
                        
                        [cf,lh,lh2]=obj.plotShaded_linear(figh,cols,[],usemed,0);
                    case 1  % plot all curves
                        [cf,lh,lh2]=obj.plotShaded_linear(figh,cols,[],usemed,shadingFactor);
                        
                    case 2  % plot error bars = std
                        eb=obj.std();
                        
                        [cf,lh,lh2]=obj.plot_linear(figh,eb,cols,usemed);
                    case 3 % plot error bars = average se
                        [~,eb]=obj.average;
                        [cf,lh,lh2]=obj.plot_linear(figh,eb,cols,usemed);
                        
                    case 4 % 95% CI
                        alpha=0.05;
                        weightsOn=0;
                        eb=obj.ttest(alpha,weightsOn);
                        [cf,lh,lh2]=obj.plot_linear(figh,eb,cols,usemed);
                        
                    case 5 % 95% CI v2
                        [~,eb]=obj.average;
                        eb.applyfun(@(x) 1.96*x);
                        [cf,lh,lh2]=obj.plot_linear(figh,eb,cols,usemed);
                        
                end
            end
            
            
        end
        
        
        
        function [cf,lh,lh2]=plotShaded_linear(obj,h,cols,lgon,usemed,shadingFactor)
            lh=[];
            lh2={};
            if nargin<6
                shadingFactor=0.5;
            end
            if nargin<5
                usemed=0;
            end
            if nargin<4
                lgon=[];
            end
            if nargin<2 || isempty(h)
                cf=figure();
            else
                if not(h==-1)
                    figure(h);
                    cf=h;
                    %axes(h);
                    hold all
                end
            end
            no=length(obj);
            
            
            
            
            if nargin<3 || isempty(cols)
                %cols0=getColor([1:no]');
                %cols=cellfun(@(x) x(1),cols0,'UniformOutput',0);
                cols=coolcolors.getDefaultColors(no);
                %coolcolors.testColors(cols);
            end
            
            for hh=1:length(cols)
                if ischar(cols{hh})
                    cols{hh}=rem(floor((strfind('kbgcrmy', cols{hh}) - 1) * [0.25 0.5 1]), 2);
                end
            end
            %obja=[];
            if usemed
                obja=obj.median;
            else
                obja=obj.average;
            end
            for i=1:no
                clh2=[];
                if size(obj(i).g2,1)>0
                    
                    ctau=obj(i).tau;
                    
                    if not(isempty(cols{i})) %% otherwise we don't plot
                        ccol=cols{i};
                        
                        if(sum(ccol)==0)
                            
                            ccolFaded=(shadingFactor)*[1,1,1];
                        else
                            ccolFaded=hsv2rgb(rgb2hsv(ccol).*[1,shadingFactor,1]);
                        end
                        
                        if shadingFactor>0 % indicates representation of individual lines
                            
                            for j=1:size(obj(i).g2,1)%j=1:obj(i).n
                                
                                
                                %                         cline=plot(ctau,obj(i).g2(j,:)*obj(i).scale(j),'Color',defLineColorOrder(mod(i-1,nColors)+1,:),'LineStyle',...
                                %                             defLineStyleOrder(mod(floor((i-1)/nColors),nStyles)+1,:));
                                
                                
                                cline=plot(ctau,obj(i).g2(j,:),'Color',ccolFaded);
                                %cline=plot(ctau,obj(i).g2(j,:)*obj(i).scale(j),cols{i});
                                
                                hold all
                                %                             if j==1
                                %                                 set(cline,'DisplayName',obj(i).tag);
                                
                                %else
                                % set(cline,'DisplayName',sprintf('fcs#%g',j));
                                % set(get(get(cline,'Annotation'),'LegendInformation'),...
                                %     'IconDisplayStyle','off');
                                if usemed<0 & j==1
                                    %    set(cline,'DisplayName',obj(i).tag);
                                    %    set(get(get(cline,'Annotation'),'LegendInformation'),...
                                    %        'IconDisplayStyle','on');
                                    lh=cat(1,lh,cline);
                                end
                                clh2=cat(1,clh2,cline);
                                
                                %end
                                
                                
                            end
                        end
                        if usemed>=0 % if -1, then don't represent the mean/median
                            cline=plot(ctau,obja(i).g2,'Color',ccol,'LineWidth',2);
                            lh=cat(1,lh,cline);
                            
                            % set(cline,'DisplayName',obj(i).tag);
                        end
                        hold all;
                    end
                end
                lh2=cat(1,lh2,{clh2});
            end
            
            
            
            
            
            %xlim([1e-6,1e-1]);
            
            if not(isempty(lgon))
                for i=1:length(lgon)
                    set(lh(i),'DisplayName',lgon{i});
                end
                legend SHOW
            end
            %legend(obj.tag);
        end
        
        
        
        
        function [cf,lh]=plotErrorBand_linear(obj,h,cols,lgon,usemed,shadingFactor,eb,bandShadingFactor)
            lh=[];
            if nargin<6
                shadingFactor=0.5;
            end
            if nargin<5
                usemed=0;
            end
            if nargin<4
                lgon=[];
            end
            cf=[];
            if nargin<2 || isempty(h)
                display('Empty figure, creating new');
                cf=figure();
            else
                if not(h==-1)
                    figure(h);
                    cf=h;
                    %axes(h);
                    hold all
                end
            end
            no=length(obj);
            
            if nargin<3 || isempty(cols)
                cols=coolcolors.getDefaultColors(no);
            end
            
            for hh=1:length(cols)
                if ischar(cols{hh})
                    cols{hh}=rem(floor((strfind('kbgcrmy', cols{hh}) - 1) * [0.25 0.5 1]), 2);
                end
            end
            
            if abs(usemed)==1
                obja=obj.median;
            else
                obja=obj.average;
            end
            
            %% first loop across data to plot the patches
            for i=1:no
                if not(isempty(eb)) & size(eb(i).g2,1)>0
                    
                    ctau=obj(i).tau;
                    
                    if not(isempty(cols{i})) %% otherwise we don't plot
                        ccol=cols{i};
                        
                        if(sum(ccol)==0)
                            
                            ccolFaded=(bandShadingFactor)*[1,1,1];
                        else
                            ccolFaded=hsv2rgb(rgb2hsv(ccol).*[1,bandShadingFactor,1]);
                            
                            
                        end
                        edgeColor=ccol+(1-ccol)*0.55;
                        patchSaturation=0.15;
                        patchColor=ccol+(1-ccol)*(1-patchSaturation);
                        
                        if bandShadingFactor>0 %% negative shading factor factor doesn't plot
                            for j=1:size(obj(i).g2,1)
                                
                                uE=obja(i).g2+eb(i).g2;
                                lE=obja(i).g2-eb(i).g2;
                                
                                %Make the cordinats for the patch
                                yP=[lE,fliplr(uE)];
                                xP=[obja(i).tau,fliplr(obja(i).tau)];
                                
                                %remove any nans otherwise patch won't work
                                xP(isnan(yP))=[];
                                yP(isnan(yP))=[];
                                
                                H(i).edgelE=plot(obja(i).tau,lE,'-','color',edgeColor);
                                hold all
                                H(i).edgeuE=plot(obja(i).tau,uE,'-','color',edgeColor);
                                
                                
                                H(i).patch=patch(xP,yP,1,'facecolor',patchColor,...
                                    'edgecolor','none');
                                
                                
                                
                                set(get(get(H(i).edgelE,'Annotation'),'LegendInformation'),...
                                    'IconDisplayStyle','off');
                                set(get(get(H(i).edgeuE,'Annotation'),'LegendInformation'),...
                                    'IconDisplayStyle','off');
                                
                                set(get(get(H(i).patch,'Annotation'),'LegendInformation'),...
                                    'IconDisplayStyle','off');
                                
                                
                                
                                
                            end
                        end
                        
                    end
                end
            end
            
            %%second loop to plot the main lines
            for i=1:no
                if size(obj(i).g2,1)>0
                    
                    ctau=obj(i).tau;
                    
                    if not(isempty(cols{i})) %% otherwise we don't plot
                        ccol=cols{i};
                        
                        if(sum(ccol)==0)
                            
                            ccolFaded=(shadingFactor)*[1,1,1];
                        else
                            ccolFaded=hsv2rgb(rgb2hsv(ccol).*[1,shadingFactor,1]);
                        end
                        
                        if shadingFactor>0
                            for j=1:size(obj(i).g2,1)
                                
                                cline=plot(ctau,obj(i).g2(j,:),'Color',ccolFaded);
                                hold all
                                set(cline,'DisplayName',sprintf('fcs#%g',j));
                                set(get(get(cline,'Annotation'),'LegendInformation'),...
                                    'IconDisplayStyle','off');
                                if usemed<0 & j==1
                                    
                                    set(get(get(cline,'Annotation'),'LegendInformation'),...
                                        'IconDisplayStyle','on');
                                    lh=cat(1,lh,cline);
                                end
                                
                                
                                if not(isempty(obj(i).lims));
                                    
                                    set(cline,'UserData',[i,j,obj(i).lims(j,:)]);
                                else
                                    
                                end
                                
                            end
                        end
                        if usemed>=0
                            cline=plot(ctau,obja(i).g2,'Color',ccol,'LineWidth',1);
                            lh=cat(1,lh,cline);
                            
                            
                        end
                        hold all;
                    end
                end
            end
            
            
            
            
            if not(isempty(lgon))
                for i=1:length(lgon)
                    set(lh(i),'DisplayName',lgon{i});
                end
                legend SHOW
            end
        end
        
        
        function f_trimmed=trim(obj,tauminmax);
            no=length(obj);
            f_trimmed(no,1)=fcs();
            for i=1:no
                ctau=obj(i).tau;
                idxs2k=ctau>=tauminmax(1) & ctau<tauminmax(2);
                f_trimmed(i).tau=ctau(idxs2k);
                
                f_trimmed(i).g2=obj(i).g2(:,idxs2k);
                
                
                
                f_trimmed(i).n=obj(i).n;
            end
        end
        
        
        
        
        
        function [afcs,afcsSE]=average(obj)
            no=length(obj);
            
            afcs(no,1)=fcs();
            afcsSE(no,1)=fcs();
            for i=1:no
                cn=size(obj(i).g2,1);
                if cn>=1
                    
                    
                    afcs(i).g2=mean(obj(i).g2,1);
                    afcsSE(i).g2=std(obj(i).g2,0,1)/sqrt(cn);
                    
                    
                    afcs(i).n=1;
                    afcsSE(i).n=1;
                    
                    
                    afcs(i).tau=obj(i).tau;
                    afcsSE(i).tau=obj(i).tau;
                    
                    
                end
                
            end
            
        end
        
        
        
        function ci=ttest(obj,alpha)
            
            if nargin<2
                alpha=0.05;
            end
            no=length(obj);
            callm=obj.average;
            %h=zeros(no,length(obj(1).tau));
            %p=zeros(no,length(obj(1).tau));
            for i=no:-1:1
                ci(i,1)=obj(i).sub(1);
                
                ntau=length(obj(i).tau);
                for j=1:ntau
                    cm=callm(i).g2(:,j); %mean(obj(i).g2(:,j));
                    [ch,cp,cci]=ttest(obj(i).g2(:,j),cm,'Alpha',alpha);
                    %        h(i,j)=ch;
                    
                    %       p(i,j)=cp;
                    ci(i).g2(j)=cci(2)-cm;
                    
                end
            end
            
            
            
        end
        
        function [cim,cip]=ttest2(obj,obj2,alpha) %two samples t-test (welch test), assumes unequal variances
            if nargin<2
                alpha=0.05;
            end
            no=length(obj);
            if length(obj2)==1
                obj2=repmat(obj2,no,1);
            end
            
            
            %h=zeros(no,length(obj(1).tau));
            %p=zeros(no,length(obj(1).tau));
            for i=no:-1:1
                cim(i,1)=obj(i).sub(1);
                cip(i,1)=obj(i).sub(1);
                ntau=length(obj(i).tau);
                for j=1:ntau
                    cm=mean(obj(i).g2(:,j));
                    cm2=mean(obj2(i).g2(:,j));
                    [ch,cp,cci]=ttest2(obj(i).g2(:,j)-cm,obj2(i).g2(:,j)-cm2,'Alpha',alpha,...
                        'Vartype','unequal');
                    %        h(i,j)=ch;
                    
                    %       p(i,j)=cp;
                    cip(i).g2(j)=cci(2);
                    cim(i).g2(j)=cci(1);
                end
            end
            
            
            
        end
        
        function [f,dist2m,idxs2keep]=removeOutliers(obj,nsigmas,tauminmax,nrep)
            if nargin<2
                nsigmas=5;
            end
            if nargin<3
                tauminmax=[1e-6,1e-3];
            end
            
            if nargin<4
                nrep=1;
            end
            f=obj;
            %idxs2keep0=cellfun(@(x) (1:x)',num2cell(f.numel),'uni',0);
            cn2remove=zeros(length(f),1);
            i=1;
            goon=1;
            while i<=nrep & goon
                
                fref=f.median;
                dist2m=obj.l2dist(fref,tauminmax);%distance 2 media
                sig=cellfun(@(x) 1.4826*mad(x,1),dist2m,'uni',0);
                idxs2keep=cellfun(@(x,y) x<nsigmas*y,dist2m,sig,'uni',0);
                
                n2remove=obj.numel-cellfun(@(x) sum(x),idxs2keep,'uni',1);
                if not(all(n2remove==cn2remove))
                    cn2remove=n2remove;
                    goon=1;
                    i=i+1;
                else
                    goon=0;
                end
                %cellfun(@sum,idxs2keep)
                %dist2m0=obj.l2dist(fref,tauminmax);
                %cidxs2keep0=cellfun(@(x,y) x(y),idxs2keep0,idxs2keep,'uni',0);
                %idxs2keep0=cidxs2keep0;
                
                
                cf=obj.sub(idxs2keep);
                f=cf;
            end
        end
        
        function [f,dist2m,idxs2keep]=ro(obj,nsigmas,tauminmax,nrep)
            if nargin<2
                nsigmas=5;
            end
            if nargin<3
                tauminmax=[1e-6,1e-3];
            end
            
            if nargin<4
                nrep=1;
            end
            [f,dist2m,idxs2keep]=obj.removeOutliers(nsigmas,tauminmax,nrep);
        end
        
        
        
        
        function f2=recombine(obj,idxs)
            n=length(idxs);
            f2(n,1)=fcs();
            for i=1:n
                cidxs=idxs{i};
                f2(i,1)=obj(cidxs).gather();
            end
        end
        
        function afcs=median(obj)
            no=length(obj);
            
            
            afcs(no,1)=fcs();
            for i=1:no
                cn=size(obj(i).g2,1);
                if cn>=1
                    
                    
                    afcs(i).g2=median(obj(i).g2,1);
                    
                    %afcs(i).
                    afcs(i).n=1;
                    
                    
                    afcs(i).tau=obj(i).tau;
                    
                    
                end
                
            end
            
        end
        
        
        function afcs=std(obj,weightsOn,normalize)
            no=length(obj);
            if nargin <2
                weightsOn=0;
            end
            if nargin <3
                normalize=0;
            end
            
            afcs(no,1)=fcs();
            for i=1:no
                cn=size(obj(i).g2,1);
                if cn>=1
                    
                    
                    afcs(i).g2=std(obj(i).g2,0,1);
                    if normalize
                        afcs(i).g2=afcs(i).g2/sqrt(cn);
                    end
                    
                    %afcs(i).
                    afcs(i).n=1;
                    %afcs(i).nTraces=obj(i).n;
                    
                    afcs(i).tau=obj(i).tau;
                    
                    
                    
                end
                
            end
            
        end
        
        function afcs=applyfun(obj,funh,obj2)
            no=length(obj);
            
            
            afcs(no,1)=fcs();
            for i=1:no
                
                if nargin<3 % this should be a point by point application for each time scale
                    
                    cg2=obj(i).g2;
                    
                    
                    for j=1:size(cg2,2)
                        cg2(:,j)=funh(cg2(:,j));
                    end
                    
                    afcs(i).g2=cg2;
                    
                else
                    if strcmp(class(obj2),'fcs')
                        n2=length(obj2);
                        afcs(i).g2=funh(obj(i).g2,obj2(min(i,n2)).g2);
                        
                    elseif strcmp(class(obj2),'cell')
                        % display('here');
                        n2=length(obj2);
                        afcs(i).g2=funh(obj(i).g2,obj2{min(i,n2)});
                        
                    else %obj2 is either a scalar or a vector of scalar
                        n2=length(obj2);
                        afcs(i).g2=funh(obj(i).g2,obj2(min(i,n2)));
                        
                    end
                end
                
                %afcs(i).
                afcs(i).n=size(afcs(i).g2,1);
                
            end
        end
        
        
        function fout=getPlusMinus(obj,fref)
            
            foutM=obj.applyfun(@(x,y) x-y,fref);
            foutP=obj.applyfun(@(x,y) x+y,fref);
            foutPM=cat(1,foutM,foutP);
            fout=foutPM.gather();
            
        end
        
        
        
        
        
        
        function scalesf=normalize(obj,t_tideMin,t_tideMax,reffcs)
            no=length(obj);
            
            if nargin<4
                reffcs=obj(1).g2(1,:);
            else
                if isscalar(reffcs)
                    reffcs=reffcs*ones(size(obj(1).tau));
                end
                
            end
            
            scalesf=repmat({},no,1);
            for i=1:no
                if t_tideMin>=0
                    t_index1=find(obj(i).tau>t_tideMin,1,'first');
                else
                    t_index1=1;
                end
                if t_tideMax>=0
                    t_index2=find(obj(i).tau<t_tideMax,1,'last');
                else
                    t_index2=length(obj(i).tau);
                end
                scalesf{i}=zeros(size(obj(i).g2,1),1);
                
                for j=1:obj(i).n
                    g2ScaleFactor=scale(obj(i).g2(j,t_index1:t_index2),reffcs(t_index1:t_index2));
                    g2Norm=obj(i).g2(j,:)*g2ScaleFactor;
                    obj(i).g2(j,:)=g2Norm;
                    %obj(i).scale(j)=g2ScaleFactor;
                    scalesf{i}(j)=g2ScaleFactor;
                    
                end
                
                
            end
        end
        
        function [fout,scalesf]=normalize2(obj,tm,tM,reffcs,newfcs)
            no=length(obj);
            if nargin<5
                newfcs=1;
            end
            fout=obj;
            if newfcs
                fout=fcs(obj);
            else
                %fout=obj;
            end
            
            if nargin<4
                reffcs=obj.average;
            end
            if not(isa(reffcs,'fcs'))
                %reffcs=obj(1).create_flat_fcs(reffcs);
                display('ref should be of fcs class')
            end
            if length(reffcs)==1 & no>1
                reffcs=repmat(reffcs,no,1);
            end
            
            
            scalesf=repmat({},no,1);
            for i=no:-1:1
                if tm>=0
                    t_index1=find(obj(i).tau>tm,1,'first');
                else
                    t_index1=1;
                end
                if tM>=0
                    t_index2=find(obj(i).tau<tM,1,'last');
                else
                    t_index2=length(obj(i).tau);
                end
                %scalesf{i}=zeros(size(obj(i).g2,1),1);
                
                ws=obj(i).tau(t_index1:t_index2);
                ws=ws/sum(ws);
                
                cscale=zeros(obj(i).n,1);
                y=reffcs(i).g2(1,t_index1:t_index2);
                xs=obj(i).g2(:,t_index1:t_index2);
                xs2=zeros(size(obj(i).g2));
                for j=1:obj(i).n
                    
                    x=xs(j,:);
                    
                    g2ScaleFactor=((ws.*x)*y')/((ws.*x)*x');
                    xs2(j,:)=g2ScaleFactor*obj(i).g2(j,:);
                    
                    %obj(i).scale(j)=g2ScaleFactor;
                    cscale(j)=g2ScaleFactor;
                    
                end
                fout(i).g2=xs2;
                scalesf{i,1}=cscale;
                
                
            end
            
            
            
            
        end
        
        
        function fbsNorm=normalizeBootstrapedFCS(obj,t_tideMin,t_tideMax,refidx)
            fbsNorm=fcs(obj);
            no=length(obj);
            n1=size(obj(1).g2,1);
            for i=1:n1
                cfcs=obj.sub(repmat({i},no,1));
                cfcs.normalize(t_tideMin,t_tideMax,cfcs(refidx).g2);
                for j=1:no
                    fbsNorm(j).g2(i,:)=cfcs(j).g2;
                end
            end
            
        end
        
        function [fbsNorm,shiftLags]=normalizeTauBootstrapedFCS(obj,t_tideMin,t_tideMax,refidx,tlagbnds)
            fbsNorm=fcs(obj);
            
            no=length(obj);
            shiftLags=repmat({zeros(size(obj(1).g2,1),1)},no,1);
            n1=size(obj(1).g2,1);
            cref=obj(refidx).average;
            for i=1:n1
                cfcs=obj.sub(repmat({i},no,1));
                
                %[cshiftedG,cshiftLags]=cfcs.normalizeTau(t_tideMin,t_tideMax,cfcs(refidx),tlagbnds);
                [cshiftedG,cshiftLags]=cfcs.normalizeTau(t_tideMin,t_tideMax,cref,tlagbnds);
                for j=1:no
                    fbsNorm(j).g2(i,:)=cshiftedG(j).g2;
                    shiftLags{j}(i)=cshiftLags{j};
                end
            end
            
        end
        
        
        
        
        
        
        function val=getSnapshot(obj,t_tideMin,t_tideMax)
            no=length(obj);
            
            for i=no:-1:1
                if t_tideMin>=0
                    t_index1=find(obj(i).tau>t_tideMin,1,'first');
                else
                    t_index1=1;
                end
                if t_tideMax>=0
                    t_index2=find(obj(i).tau<t_tideMax,1,'last');
                else
                    t_index2=length(obj(i).tau);
                end
                
                
                
                val{i,1}=mean(obj(i).g2(:,t_index1:t_index2),2);
            end
        end
        
        
        
        function ts=getCrossingTime(obj,val,val2)
            nf=length(obj);
            vals=val;
            ns=num2cell(obj.numel);
            
            if not(iscell(val))
                ntimes=size(val,2);
                if size(val,1)==1
                    val=repmat(num2cell(val),nf,1);
                    
                else
                    val=num2cell(val);
                end
                
                vals=cellfun(@(x,y) repmat(y,length(x),1),ns,val,'uni',0);
            else
                ntimes=size(vals,2);
                
            end
            
            if nargin>2
                vals2=val2;
                
                
                if not(iscell(val2))
                    %ntimes=size(val2,2);
                    if size(val2,1)==1
                        val2=repmat(num2cell(val2),nf,1);
                        
                    else
                        val2=num2cell(val2);
                    end
                    
                    vals2=cellfun(@(x,y) repmat(y,length(x),1),ns,val2,'uni',0);
                else
                    
                    
                end
            end
            
            if nargin<3
                for i=nf:-1:1
                    cvals=vals(i,:);
                    cg2=obj(i).g2;
                    ctaus=obj(i).tau;
                    ctc=nan(size(cg2,1),ntimes);
                    for k=1:ntimes
                        cvals0=cvals{k};
                        for j=1:size(cg2,1)
                            
                            
                            idxs=find(cg2(j,:)>cvals0(j),1,'first');
                            if not(isempty(idxs))
                                ctc(j,k)=ctaus(idxs);
                            end
                            
                        end
                    end
                    ts{i,1}=ctc;
                end
            else
                for i=nf:-1:1
                    cvals=vals(i,:);
                    cvals2=vals2(i,:);
                    cg2=obj(i).g2;
                    ctaus=obj(i).tau;
                    ctc=nan(size(cg2,1),ntimes);
                    for k=1:ntimes
                        cvals0=cvals{k};
                        cvals02=cvals2{k};
                        for j=1:size(cg2,1)
                            
                            
                            idxs=find(cg2(j,:)<cvals0(j) & cg2(j,:)>cvals02(j));
                            if not(isempty(idxs))
                                ctc(j,k)=median(ctaus(idxs));
                            end
                            
                        end
                    end
                    ts{i,1}=ctc;
                end
            end
            
            
        end
        
        function [M,Mtime]=max(obj)
            no=length(obj);
            for i=no:-1:1
                [cM,cMtime]=max(obj(i).g2,[],2);
                cMtime=obj(i).tau(cMtime);
                M{i,1}=cM;
                Mtime{i,1}=cMtime;
                
            end
            %lemonde.fr
            
        end
        
        function [M,Mtime]=min(obj)
            no=length(obj);
            for i=no:-1:1
                [cM,cMtime]=min(obj(i).g2,[],2);
                cMtime=obj(i).tau(cMtime);
                M{i,1}=cM;
                Mtime{i,1}=cMtime;
                
            end
            
            
        end
        
        function p=get_properties(obj,params)
            if nargin<2
                params=struct('baseline_tmM',[-1.5,-1],'raise_thresh',[0.1,0.2],'use_baseline',1);
                %raise time
            end
            objL=obj.trim([-Inf,0]);
            objR=obj.trim([0,Inf]);
            baseline=obj.getSnapshot(params.baseline_tmM(1),params.baseline_tmM(2));
            if params.use_baseline
                
                tc=objL.getCrossingTime(cellfun(@(x) params.raise_thresh(1)*(1-x),baseline,'uni',0),...
                    cellfun(@(x) params.raise_thresh(2)*(1-x),baseline,'uni',0));
            else
                tc=objL.getCrossingTime(params.raise_thresh(1),params.raise_thresh(2));
            end
            
            %[M,Mtime]=obj.max();
            [m,mtime]=objR.min();
            no=length(obj);
            p(no,1)=struct('t_raise',[],'t_dip',[],'A_dip',[],'baseline',[]);
            for j=1:no
                p(j,1).t_raise=-tc{j};
                p(j,1).t_dip=mtime{j}';
                p(j,1).A_dip=m{j};
                p(j,1).baseline=baseline{j};
            end
        end
        
        
        
        function subf=subv(obj,idxs)
            subf=obj.sub(idxs);
        end
        
        function subf=sub(obj,idxs)
            
            no=length(obj);
            for i=no:-1:1
                subf(i,1)=fcs();
            end
            if no==1 & not(iscell(idxs))
                idxs={idxs};
            end
            if not(iscell(idxs)) | not(length(idxs)==no)
                error('No good here');
            end
            for i=1:no
                %cnTraces=obj(i).nTraces;
                %subf(i).nTraces=cnTraces(idxs{i},:);
                if length(idxs{i})==1 & idxs{i}==-1
                    idxs{i}=true(size(obj(i).g2,1),1);
                end
                
                if islogical(idxs{i})
                    nidxs=sum(idxs{i});
                else
                    nidxs=length(idxs{i});
                end
                
                
                subf(i).n=nidxs;
                
                cg2=obj(i).g2;
                %cg2std=obj(i).g2std;
                
                subf(i).g2=cg2(idxs{i},:);
                %subf(i).g2std=[];
                subf(i).tau=obj(i).tau;
                %cscale=obj(i).scale;
                %subf(i).scale=cscale(idxs{i},:);
                
                
                
                
                
            end
            
        end
        
        function f=subrnd(obj,n)
            fn=obj.numel();
            no=length(obj);
            for i=no:-1:1
                if fn(i)<=n
                    ixs{i}=1:fn(i);
                else
                    x=randperm(fn(i));
                    ixs{i}=x(1:n);
                end
            end
            f=obj.sub(ixs);
        end
        
        function efcs=expand(obj,ds)
            nsets=length(ds);
            efcs=[];
            
            for i=1:nsets
                cfcs=obj.sub(ds(i).idxs);
                
                
                
                
                efcs=cat(1,efcs,cfcs);
            end
        end
        
        function gfcs=gather(obj)
            
            
            no=length(obj);
            gfcs=fcs();
            
            cg2=[];
            %cg2std=[];
            ctau=obj(1).tau;
            
            %cScale=[];
            cn=0;
            
            for i=1:no
                cn=cn+obj(i).n;
                cg2=cat(1,cg2,obj(i).g2);
                %cScale=cat(1,cScale,obj(i).scale);
                %cbck=cat(1,cbck,obj(i).bckgrnd);
                
                
            end
            gfcs.n=cn;
            gfcs.tau=ctau;
            gfcs.g2=cg2;
            
        end
        
        
        
        
        
        
        
        
        function g=getG(obj)
            g={obj.g2}';
        end
        
        
        
        
        
        
        function y=integrate(obj,tauminmax)
            no=length(obj);
            t_tideMin=tauminmax(1);
            t_tideMax=tauminmax(2);
            for i=no:-1:1
                if t_tideMin>=0
                    t_index1=find(obj(i).tau>t_tideMin,1,'first');
                else
                    t_index1=1;
                end
                if t_tideMax>=0
                    t_index2=find(obj(i).tau<t_tideMax,1,'last');
                else
                    t_index2=length(obj(i).tau);
                end
                
                
                cx=obj(i).g2(:,t_index1:t_index2);
                cx=cx(:,1:end-1);
                cy=diff(log(obj(i).tau(t_index1:t_index2)));
                
                if numel(cy)==0
                    y{i}=zeros(size(cx,1),1);
                else
                    
                    
                    y{i}=cx*cy';
                end
                
            end
        end
        
        
        
        function d=l2dist(obj,fref,tauminmax)
            
            no=length(obj);
            if length(fref)==1
                fref=repmat(fref,no,1);
            end
            t_tideMin=tauminmax(1);
            t_tideMax=tauminmax(2);
            for i=no:-1:1
                if t_tideMin>=0
                    t_index1=find(obj(i).tau>t_tideMin,1,'first');
                else
                    t_index1=1;
                end
                if t_tideMax>=0
                    t_index2=find(obj(i).tau<t_tideMax,1,'last');
                else
                    t_index2=length(obj(i).tau);
                end
                
                
                cx=obj(i).g2(:,t_index1:t_index2);
                %cx=cx(:,1:end-1);
                cx2=fref(i).g2(:,t_index1:t_index2);
                %cx2=cx2(:,1:end-1);
                for j=1:size(cx,1)
                    cx(j,:)=cx(j,:)-cx2;
                end
                ws=obj(i).tau(t_index1:t_index2);
                ws=ws/sum(ws);
                d{i,1}=(cx.^2)*ws';
            end
        end
        
        
        function gps=getGroup(obj,frefs,tauminmax)
            nrefs=length(frefs);
            
            no=length(obj);
            for i=no:-1:1
                cds=zeros(obj(i).numel,nrefs);
                for j=1:nrefs
                    d=obj(i).l2dist(frefs(j),tauminmax);
                    cds(:,j)=d{1};
                end
                [~,infidx]=min(cds,[],2);
                gps{i,1}=infidx;
            end
        end
        
        
        
        
        
        
    end
    
    
end