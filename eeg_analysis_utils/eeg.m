classdef eeg < handle
    
    properties
        t=[];
        y=[];
        file='';
        ts=0;
        t0=0;
        flag=[];
        ix=[];
        trigs=[];
        tmM=[];
        szs=[];
        xoffset=0;
        ll=[];
        
        
    end
    
    methods
        function obj=eeg(x)
            if nargin>0
                no=length(x);
                obj(no,1)=eeg();
                for i=1:no
                    obj(i).t=x(i).t;
                    obj(i).y=x(i).y;
                    obj(i).file=x(i).file;
                    obj(i).ts=x(i).ts;
                    obj(i).t0=x(i).t0;
                    obj(i).flag=x(i).flag;
                    obj(i).ix=x(i).ix;
                    obj(i).trigs=x(i).trigs;
                    obj(i).tmM=x(i).tmM;
                    obj(i).szs=x(i).szs;
                    obj(i).ll=x(i).ll;
                end
            end
        end
        
        
        function plot(obj)
            for i=1:length(obj)
                figure;
                plot(obj(i).t,obj(i).y);
                title(sprintf('@%s',datestr(obj(i).t0)));
                xlim([obj(i).t(1),obj(i).t(end)]);
            end
        end
        
        function plotall(obj,delta,plottrigs,plotsz,addon)
            if nargin<3
                plottrigs=1;
            end
            if nargin<4
                plotsz=1;
            end
            if nargin<5
                addon={};
            end
            figure;
            ccol={ [0    0.4470    0.7410],...
                [0.8500    0.3250    0.0980],...
                [0.9290    0.6940    0.1250],...
                [0.4940    0.1840    0.5560],...
                [0.4660    0.6740    0.1880],...
                [0.3010    0.7450    0.9330],...
                [0.6350    0.0780    0.1840]};
            for i=1:length(obj)
                
                
                if not(plotsz) | isempty(obj(i).szs)
                    plot(obj(i).t+obj(i).xoffset,obj(i).y+(i-1)*delta,'Color',ccol{mod(i-1,7)+1});
                    hold all
                    if not(isempty(obj(i).ll))
                        plot(obj(i).t+obj(i).xoffset,obj(i).ll+(i-1)*delta-delta/3-median(obj(i).ll),'Color','k');
                    end
                    
                    if not(isempty(addon))
                        cy=addon{i};
                        cym=median(cy);
                        plot(obj(i).t+obj(i).xoffset,cy+(i-1)*delta+delta/3-cym,'Color','k');
                    end
                    
                else
                    ixsfocal=obj(i).t>=obj(i).szs(1,1) & obj(i).t<=obj(i).szs(1,2);
                    ixsbh=obj(i).t>obj(i).szs(1,2) & obj(i).t<=obj(i).szs(1,3);
                    ixspre=obj(i).t<obj(i).szs(1,1);
                    ixspost=obj(i).t>obj(i).szs(1,3);
                    
                    plot(obj(i).t(ixspre)+obj(i).xoffset,obj(i).y(ixspre)+(i-1)*delta,'Color','k');
                    hold all
                    plot(obj(i).t(ixspost)+obj(i).xoffset,obj(i).y(ixspost)+(i-1)*delta,'Color','k');
                    plot(obj(i).t(ixsfocal)+obj(i).xoffset,obj(i).y(ixsfocal)+(i-1)*delta,'Color',ccol{1});
                    plot(obj(i).t(ixsbh)+obj(i).xoffset,obj(i).y(ixsbh)+(i-1)*delta,'Color',ccol{2});
                    
                    
                    if not(isempty(obj(i).ll))
                        mm=median(obj(i).ll);
                        plot(obj(i).t(ixspre)+obj(i).xoffset,obj(i).ll(ixspre)+(i-1)*delta-delta/3-mm,'Color','k');
                        hold all
                        plot(obj(i).t(ixspost)+obj(i).xoffset,obj(i).ll(ixspost)+(i-1)*delta-delta/3-mm,'Color','k');
                        plot(obj(i).t(ixsfocal)+obj(i).xoffset,obj(i).ll(ixsfocal)+(i-1)*delta-delta/3-mm,'Color',ccol{4});
                        plot(obj(i).t(ixsbh)+obj(i).xoffset,obj(i).ll(ixsbh)+(i-1)*delta-delta/3-mm,'Color',ccol{5});
                        
                    end
                    
                    if not(isempty(addon))
                        mm=median(addon{i});
                        cy=addon{i};
                        plot(obj(i).t(ixspre)+obj(i).xoffset,cy(ixspre)+(i-1)*delta+delta/3-mm,'Color','k');
                        hold all
                        plot(obj(i).t(ixspost)+obj(i).xoffset,cy(ixspost)+(i-1)*delta+delta/3-mm,'Color','k');
                        plot(obj(i).t(ixsfocal)+obj(i).xoffset,cy(ixsfocal)+(i-1)*delta+delta/3-mm,'Color',ccol{4});
                        plot(obj(i).t(ixsbh)+obj(i).xoffset,cy(ixsbh)+(i-1)*delta+delta/3-mm,'Color',ccol{5});
                    end
                end
                
                
                
                %                 title(sprintf('@%s',datestr(obj(i).t0)));
                %                 xlim([obj(i).t(1),obj(i).t(end)]);
                
                
                
                if plottrigs
                    for j=1:size(obj(i).trigs,1)
                        cline=plot([obj(i).trigs(j,1) obj(i).trigs(j,1)]+obj(i).xoffset,[median(obj(i).y+(i-1)*delta)-delta/2,median(obj(i).y+(i-1)*delta)+delta/2],...
                            'Color','m','LineWidth',2);
                        if obj(i).trigs(j,3)>0
                            set(cline,'LineStyle',':');
                        end
                        if obj(i).trigs(j,2)>0
                            set(cline,'Color','r');
                            
                        end
                    end
                end
                
            end
        end
        
        
        function fh=plot2(obj,plottrigs,plotsz,addon,lp,trigscols)
            if nargin<2
                plottrigs=1;
            end
            if nargin<3
                plotsz=1;
            end
            if nargin<4
                addon={};
            end
            if nargin<5
                lp=[];
            end
            ccol={ [0    0.4470    0.7410],...
                [0.8500    0.3250    0.0980],...
                [0.9290    0.6940    0.1250],...
                [0.4940    0.1840    0.5560],...
                [0.4660    0.6740    0.1880],...
                [0.3010    0.7450    0.9330],...
                [0.6350    0.0780    0.1840]};
            if nargin<6
                trigscols=[ccol{1};[0.1,0.1,0.1]];
            end
            
            
            
            fh=repmat({[]},length(obj),1);
            for i=1:length(obj)
                if not(plotsz) | isempty(obj(i).szs)
                    cfig=figure;
                    xx=plot(obj(i).t+obj(i).xoffset,obj(i).y,'Color','k');
                    hold all
                    clim=get(gca,'XLim');
                    fh{i,1}=cfig;
                    
                    
                    if plottrigs
                        if plottrigs==1
                            for j=1:size(obj(i).trigs,1)
                                cline=plot([obj(i).trigs(j,1) obj(i).trigs(j,1)]+obj(i).xoffset,get(gca,'YLim'),...
                                    'Color','m','LineWidth',2);
                                if obj(i).trigs(j,3)>0
                                    set(cline,'LineStyle',':');
                                end
                                if obj(i).trigs(j,2)>0
                                    set(cline,'Color','r');
                                    
                                end
                                
                                
                            end
                        else
                            %myorange= [0.8706    0.4902    0];
                            %myred
                            myt=obj(i).trigs;
                            myt(:,1)=myt(:,1)+obj(i).xoffset;
                            %eeg.myplottrigs(gca,myt,[ccol{1};[0.2,0.2,0.2]]);
                            eeg.myplottrigs(gca,myt,trigscols);
                            delete(xx);
                            plot(obj(i).t+obj(i).xoffset,obj(i).y,'Color','k');
                        end
                    end
                    
                    
                    %hold all
                    %                 if not(isempty(obj(i).ll))
                    %                     cfig=figure;
                    %                     fh{i,1}=cat(1,fh{i},cfig);
                    %                     plot(obj(i).t+obj(i).xoffset,obj(i).ll,'Color','k');
                    %                     xlim(clim);
                    %                 end
                    
                    if not(isempty(addon))
                        
                        nn=size(addon,2);
                        mygreen=[ 0.4660    0.6740    0.1880];
                        for j=1:nn
                            cy=addon{i};
                            cym=median(cy);
                            cfig=figure;
                            fh{i,1}=cat(1,fh{i},cfig);
                            plot(obj(i).t+obj(i).xoffset,cy,'Color',mygreen,'LineWidth',1.5);
                            xlim(clim);
                        end
                    end
                    
                else
                    ixsfocal=obj(i).t>=obj(i).szs(1,1) & obj(i).t<=obj(i).szs(1,2);
                    ixsbh=obj(i).t>obj(i).szs(1,2) & obj(i).t<=obj(i).szs(1,3);
                    ixspre=obj(i).t<obj(i).szs(1,1);
                    ixspost=obj(i).t>obj(i).szs(1,3);
                    
                    cfig=figure;
                    plot(obj(i).t(ixspre)+obj(i).xoffset,obj(i).y(ixspre),'Color','k');
                    hold all
                    plot(obj(i).t(ixspost)+obj(i).xoffset,obj(i).y(ixspost),'Color','k');
                    plot(obj(i).t(ixsfocal)+obj(i).xoffset,obj(i).y(ixsfocal),'Color',ccol{1});
                    plot(obj(i).t(ixsbh)+obj(i).xoffset,obj(i).y(ixsbh),'Color',ccol{2});
                    fh{i,1}=cfig;
                    
                    clim=get(gca,'XLim');
                    if plottrigs
                        for j=1:size(obj(i).trigs,1)
                            cline=plot([obj(i).trigs(j,1) obj(i).trigs(j,1)]+obj(i).xoffset,get(gca,'YLim'),...
                                'Color','m','LineWidth',2);
                            if obj(i).trigs(j,3)>0
                                set(cline,'LineStyle',':');
                            end
                            if obj(i).trigs(j,2)>0
                                set(cline,'Color','r');
                                
                            end
                        end
                    end
                    
                    %                      if not(isempty(obj(i).ll))
                    %                          mm=0 ; %median(obj(i).ll);
                    %                           cfig=figure;
                    %
                    %                     plot(obj(i).t(ixspre)+obj(i).xoffset,obj(i).ll(ixspre)-mm,'Color','k');
                    %                     hold all
                    %                     plot(obj(i).t(ixspost)+obj(i).xoffset,obj(i).ll(ixspost)-mm,'Color','k');
                    %                     plot(obj(i).t(ixsfocal)+obj(i).xoffset,obj(i).ll(ixsfocal)-mm,'Color',ccol{4});
                    %                     plot(obj(i).t(ixsbh)+obj(i).xoffset,obj(i).ll(ixsbh)-mm,'Color',ccol{5});
                    %                     xlim(clim);
                    %                     fh{i,1}=cat(1,fh{i},cfig);
                    %                      end
                    
                    if not(isempty(addon))
                        
                        nn=size(addon,2);
                        
                        for j=1:nn
                            cy=addon{i,j};
                            mm=0; %median(addon{i,j});
                            cfig=figure;
                            
                            
                            if not(isempty(lp)) & lp(j)==1
                                semilogy(obj(i).t(ixspre)+obj(i).xoffset,cy(ixspre)-mm,'Color','k');
                                hold all
                                semilogy(obj(i).t(ixspost)+obj(i).xoffset,cy(ixspost)-mm,'Color','k');
                                semilogy(obj(i).t(ixsfocal)+obj(i).xoffset,cy(ixsfocal)-mm,'Color',ccol{4});
                                semilogy(obj(i).t(ixsbh)+obj(i).xoffset,cy(ixsbh)-mm,'Color',ccol{5});
                                xlim(clim);
                                fh{i,1}=cat(1,fh{i},cfig);
                            else
                                plot(obj(i).t(ixspre)+obj(i).xoffset,cy(ixspre)-mm,'Color','k');
                                hold all
                                plot(obj(i).t(ixspost)+obj(i).xoffset,cy(ixspost)-mm,'Color','k');
                                plot(obj(i).t(ixsfocal)+obj(i).xoffset,cy(ixsfocal)-mm,'Color',ccol{4});
                                plot(obj(i).t(ixsbh)+obj(i).xoffset,cy(ixsbh)-mm,'Color',ccol{5});
                                xlim(clim);
                                fh{i,1}=cat(1,fh{i},cfig);
                            end
                        end
                    end
                end
                
                
                
                %                 title(sprintf('@%s',datestr(obj(i).t0)));
                %                 xlim([obj(i).t(1),obj(i).t(end)]);
                
                
                
                
                
            end
        end
        
        function set_offset(obj,offs)
            n=length(offs);
            for i=1:length(obj)
                obj(i).xoffset=offs(min(i,n));
            end
        end
        
        
        
        function varargout=bandpass(obj,lowf,highf,sampl_f)
            
            no=length(obj);
            if nargout>0
                obj_new=eeg(obj);
            else
                obj_new=obj;
            end
            for i=1:no
                if nargin<4
                    sampl_f=1/obj(i).ts*1000;
                end
                
                
                
                if highf>0
                    [bhpf,ahpf]=butter(2,highf*2/sampl_f,'high');
                    
                    xx= filtfilt(bhpf,ahpf,obj_new(i).y);
                    obj_new(i).y=xx;
                end
                if lowf>0
                    [blpf,alpf]=butter(6,lowf*2/sampl_f);
                    xx = filtfilt (blpf,alpf,obj_new(i).y);
                    obj_new(i).y=xx;
                end
            end
            if nargout>0
                varargout={obj_new};
            end
        end
        
        
        function varargout=waveden(obj,TPTR,SORH,SCAL,N,wname)
            
            no=length(obj);
            if nargout>0
                obj_new=eeg(obj);
            else
                obj_new=obj;
            end
            for i=1:no
                
                obj_new(i).y=wden(obj(i).y,TPTR,SORH,SCAL,N,wname);
            end
            if nargout>0
                varargout={obj_new};
            end
        end
        
        function l=linelength(obj,dt)
            for i=length(obj):-1:1
                N=dt*1000/obj(i).ts; %dt in seconds
                ddeg=(abs(diff(obj(i).y)));
                
                b = (1/N)*ones(1,N);
                a = 1;
                xx=filter(b,a,ddeg);
                xx(1:N)=xx(N);
                l{i,1}=[xx(1);xx];
                obj(i).ll=l{i,1};
            end
            
        end
        
        
        function l=slidevar(obj,dt)
            for i=length(obj):-1:1
                N=dt*1000/obj(i).ts; %dt in seconds
                if mod(N,2)==0
                    N=N+1;
                end
                l{i,1}=movvar(obj(i).y,N,'Endpoints','shrink');
                
            end
            
        end
        
        function varargout=downsample(obj,n)
            no=length(obj);
            if nargout>0
                obj_new=eeg(obj);
            else
                obj_new=obj;
            end
            for i=1:no
                
                obj_new(i).y= downsample(obj_new(i).y,n);
                obj_new(i).t=downsample(obj_new(i).t,n);
                
            end
            if nargout>0
                varargout={obj_new};
            end
        end
        
        
        function varargout=select(obj,tmM)
            no=length(obj);
            if nargout>0
                obj_new=eeg(obj);
            else
                obj_new=obj;
            end
            for i=1:no
                ixm=find(obj(i).t>tmM(1),1,'first');
                ixM=find(obj(i).t>tmM(2),1,'first');
                if isempty(ixM)
                    ixM=length(obj(i).t);
                end
                if isempty(ixm)
                    ixm=1;
                end
                obj_new(i).y=obj_new(i).y(ixm:ixM);
                obj_new(i).t=obj_new(i).t(ixm:ixM);
                
            end
            if nargout>0
                varargout={obj_new};
            end
        end
        
        
        
        function f=corr(obj,mlag,ixs,norm,norm2)
            no=length(obj);
            if nargin<4
                norm='unbiased';
            end
            if nargin<5
                norm2=1;
            end
            if not(iscell(ixs))
                ixs={ixs};
            end
            nix=length(ixs);
            f(nix,1)=fcs;
            
            for j=1:nix
                cixs=ixs{j};
                f(j).tau=0:mlag;
                f(j).g2=zeros(no,mlag+1);
                
                for i=1:no
                    if length(cixs)==1 & cixs==-1;
                        [acorr,lag]=xcorr(obj(i).y,mlag,norm);
                        if norm2
                            acorr=acorr/(mean(obj(i).y)^2)-1;
                        end
                    else
                        [acorr,lag]=xcorr(obj(i).y(cixs),mlag,norm);
                        if norm2
                            acorr=acorr/(mean(obj(i).y)^2)-1;
                        end
                    end
                    f(j).g2(i,:)=acorr(mlag+1:end)';
                    
                end
                
            end
            
        end
        
        function f=corr2(obj,mlag,ixs,norm)
            no=length(obj);
            if nargin<4
                norm='unbiased';
            end
            if nargin<5
                norm2=1;
            end
            if not(iscell(ixs))
                ixs={ixs};
            end
            nix=length(ixs);
            f(nix,1)=fcs;
            
            for j=1:nix
                cixs=ixs{j};
                f(j).tau=0:mlag;
                f(j).g2=zeros(no,mlag+1);
                
                for i=1:no
                    m=median(obj(i).y);
                    if length(cixs)==1 & cixs==-1;
                        [acorr,lag]=xcorr(obj(i).y-m,mlag,norm);
                        M=mean(obj(i).y-m)^2;
                    else
                        [acorr,lag]=xcorr(obj(i).y(cixs)-m,mlag,norm);
                        M=mean(obj(i).y(cixs)-m)^2;
                    end
                    f(j).g2(i,:)=acorr(mlag+1:end)'-M;
                    
                end
                
            end
            
        end
        
        function [datav,trigsv,tsv,t0v]=vectorize(obj)
            datav=cellfun(@(x,y) cat(2,x,y),{obj(:).t},{obj(:).y},'uni',0);
            trigsv={obj(:).trigs};
            tsv=[obj(:).ts];
            t0v=[obj(:).t0];
            
        end
        function save(obj,filename,vct)
            if nargin<2
                filename='eegdata.mat';
            end
            eegdata=obj;
            if not(vct)
                save(filename,'eegdata');
                fprintf(1,'EEG data saved in file %s \n',filename);
            else
                [data,trigs,ts,t0]=vectorize(eegdata);
                save(filename,'data','trigs','ts','t0');
            end
        end
        
        
        
        
    end
    methods(Static)
        function myplottrigs(ax,trigs,ccol)
            axis(ax);
            hold all
            for j=1:size(trigs,1)
                
                
                if trigs(j,2)>0
                    edgeColor=ccol(1,:); %+(1-ccol)*0.55;
                    patchSaturation=0.2;
                    patchColor=ccol(1,:)+(1-ccol(1,:))*(1-patchSaturation);
                else
                    
                    edgeColor=ccol(2,:); %+(1-ccol)*0.55;
                    patchSaturation=0.2;
                    patchColor=ccol(2,:)+(1-ccol(2,:))*(1-patchSaturation);
                end
                
                %Make the cordinats for the patch
                ylim=get(ax,'YLim');
                %yP=[ylim(1),ylim(1),ylim(2),ylim(2)]*1/2;
                yP=[-1.5,-1.5,1.5,1.5];
                xP=[trigs(j,1),trigs(j,1)+15,trigs(j,1)+15,trigs(j,1)]; %;fliplr(1:n)];
                
                
                cpatch=patch(xP,yP,1,'facecolor',patchColor,...
                    'edgecolor','none');
                cline=plot([xP,xP(1)],[yP,yP(1)],'Color',edgeColor,'LineWidth',1.5);
                if trigs(j,3)>0
                    set(cline,'LineStyle',':');
                end
                %plot(1:n,cyl','Color',ccol);
                %plot(1:n,cyh','Color',ccol);
                
                
            end
        end
    end
    
    
    
    
    
end