## Description
This repository contains the core part of the code used to analyze the electrophysilogy data in the paper from Bui. et al. : "Dentate gyrus mossy cells control spontaneous convulsive seizures and spatial memory"

The functions used for signal processing are in the folder *eeg_analysis_utils*. In particular, spikes.m allows automated analysis of juxtacellular recordings data. It provides routines for spike detection, clustering, registration with pulsed laser train, delay analysis, visualization, and more.

The *juxtacellular* folder contains the scripts used to run the analysis (using methods implemented in eeg_analysis_utils) on the raw juxtacellular recording data, and to plot the figures corresponding to the juxtacellular recording experiments. 

## Dependencies
Some of the scripts used for plotting require the matlab_plot_utils toolbox, which is available at https://gitlab.com/climouse/matlab_plot_utils
